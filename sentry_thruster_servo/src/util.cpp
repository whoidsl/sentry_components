#include "util.h"
#include <sentry_thruster_servo/State.h>

#include <ros/console.h>

namespace sentry_thruster_servo
{
auto thermistor_kohms(std::float_t voltage) -> std::float_t
{
  if (voltage >= 255.0)
  {
    return std::numeric_limits<std::float_t>::max();  
  }
  else
  {
    return 24.3 * voltage / (255.0 - voltage);
  }
}

bool updateState(const std::string& response, const std::string& address, ::sentry_thruster_servo::State& state,
                 const ros::Time& timestamp) noexcept
{
  const auto last_etx_indx = response.rfind('\x03');
  if (last_etx_indx == std::string::npos)
  {
    ROS_WARN_STREAM("No trailing ETX character found within incoming data");
    return false;
  }
  const auto last_hash_indx = response.rfind('#', last_etx_indx);
  if (last_hash_indx == std::string::npos)
  {
    ROS_WARN_STREAM("No '#' character found preceeding terminating ETX character");
    return false;
  }

  // minimum message possible: #$ADDRESS\r\n:\x03

  if (last_etx_indx - last_hash_indx < 5 + address.size())
  {
    ROS_WARN_STREAM("message too short");
    return false;
  }

  if (response.compare(last_hash_indx + 1, address.size(), address) != 0)
  {
    ROS_WARN_STREAM("address mismatch");
    return false;
  }

  const auto payload_start = last_hash_indx + 1 + address.size();
  const auto payload_size = last_etx_indx - payload_start + 1;

  // location readback?
  if (response.compare(payload_start, QUERY_LOCATION_STRLEN, QUERY_LOCATION_STR) == 0)
  {
    // expected payload message: "?LHHHH\r\n*\x03"
    constexpr auto expected_size = QUERY_LOCATION_STRLEN + 4 + 4;

    if (payload_size != expected_size)
    {
      ROS_WARN_STREAM("message too short: expected " << expected_size << " characters, read " << payload_size << " in "
                                                     << response.substr(payload_start, payload_size));
      return false;
    }

    if (sscanf(response.c_str() + payload_start + QUERY_LOCATION_STRLEN, "%hx", &state.angle_raw) != 1)
    {
      ROS_WARN_STREAM("unable to parse hex integer from payload: " << response.substr(payload_start, payload_size));
      return false;
    }
    state.angle_radians = state.angle_raw / COUNTS_PER_RADIAN;
    state.update = sentry_thruster_servo::State::UPDATE_ANGLE;
  }
  else if (response.compare(payload_start, SET_LOCATION_STRLEN, SET_LOCATION_STR) == 0)
  {
    // expected payload message: "!LHHHH\r\r\n*\x03"
    constexpr auto expected_size = SET_LOCATION_STRLEN + 4 + 1 + 4;

    if (payload_size != expected_size)
    {
      ROS_WARN_STREAM("message too short: expected " << expected_size << " characters, read " << payload_size << " in "
                                                     << response.substr(payload_start, payload_size));
      return false;
    }

    if (sscanf(response.c_str() + payload_start + QUERY_LOCATION_STRLEN, "%hx", &state.set_angle_raw) != 1)
    {
      ROS_WARN_STREAM("unable to parse hex integer from payload: " << response.substr(payload_start, payload_size));
      return false;
    }
    state.set_angle_radians = state.set_angle_raw / COUNTS_PER_RADIAN;
    state.update = sentry_thruster_servo::State::UPDATE_SET_ANGLE;
  }

  // Speed readback?
  else if (response.compare(payload_start, QUERY_SPEED_STRLEN, QUERY_SPEED_STR) == 0)
  {
    // expected payload message: "?M77 1\r\r\n0077 XX\r\n*\x03"

    constexpr auto expected_size = QUERY_SPEED_STRLEN + QUERY_SPEED_RESPONSE_STRLEN;
    if (payload_size != expected_size)
    {
      ROS_WARN_STREAM("message too short: expected " << expected_size << " characters, read " << payload_size << " in "
                                                     << response.substr(payload_start, payload_size));
      return false;
    }

    if (sscanf(response.c_str() + payload_start + QUERY_SPEED_STRLEN, "\r\n %*X %hhX", &state.speed_raw) != 1)
    {
      ROS_WARN_STREAM("unable to parse hex byte from payload: " << response.substr(payload_start, payload_size));
      return false;
    }
    state.update = sentry_thruster_servo::State::UPDATE_SPEED;
  }
  // Switch readback?
  else if (response.compare(payload_start, QUERY_SWITCH_STRLEN, QUERY_SWITCH_STR) == 0)
  {
    // expected payload message: "?M0 1\r\r\nXX\r\n*\x03"
    constexpr auto expected_size = QUERY_SWITCH_STRLEN + QUERY_SWITCH_RESPONSE_STRLEN;
    if (payload_size != expected_size)
    {
      ROS_WARN_STREAM("message too short: expected " << expected_size << " characters, read " << payload_size << " in "
                                                     << response.substr(payload_start, payload_size));
      return false;
    }

    if (sscanf(response.c_str() + payload_start + QUERY_SWITCH_STRLEN, "\r\n %*X %hhX", &state.switch_state) != 1)
    {
      ROS_WARN_STREAM("unable to parse hex byte from payload: " << response.substr(payload_start, payload_size));
      return false;
    }
    state.update = sentry_thruster_servo::State::UPDATE_SWITCH;
  }
  // Index crossing
  else if (response.compare(payload_start, QUERY_INDEX_CROSSING_STRLEN, QUERY_INDEX_CROSSING_STR) == 0)
  {
    // expected payload message: "?IHHHH\r\n*\x03"
    constexpr auto expected_size = QUERY_INDEX_CROSSING_STRLEN + 4 + 4;
    if (payload_size != expected_size)
    {
      ROS_WARN_STREAM("message too short: expected " << expected_size << " characters, read " << payload_size << " in "
                                                     << response.substr(payload_start, payload_size));
      return false;
    }

    if (sscanf(response.c_str() + payload_start + QUERY_INDEX_CROSSING_STRLEN, "%hx", &state.crossing_angle_raw) != 1)
    {
      ROS_WARN_STREAM("unable to parse hex byte from payload: " << response.substr(payload_start, payload_size));
      return false;
    }
    state.update = sentry_thruster_servo::State::UPDATE_CROSSING;
  }
  else if (response.compare(payload_start, QUERY_VOLTAGES_STRLEN, QUERY_VOLTAGES_STR) == 0)
  {
    constexpr auto expected_size = QUERY_VOLTAGES_STRLEN + QUERY_VOLTAGES_RESPONSE_STRLEN;
    if (payload_size != expected_size)
    {
      ROS_WARN_STREAM("message too short: expected " << expected_size << " characters, read " << payload_size << " in "
                                                     << response.substr(payload_start, payload_size));
      return false;
    }

    if (sscanf(response.c_str() + payload_start + QUERY_VOLTAGES_STRLEN, " %hhX %hhX %hhX %hhX", &state.raw_voltages[0],
               &state.raw_voltages[1], &state.raw_voltages[2], &state.raw_voltages[3]) != 4)
    {
      ROS_WARN_STREAM("unable to parse hex byte from payload: " << response.substr(payload_start, payload_size));
      return false;
    }
    // bus voltage uses a 1M, 80.6k voltage divider
    state.bus_voltage_v =
        static_cast<float>(state.raw_voltages[::sentry_thruster_servo::State::RAW_VOLTAGE_INDEX_BUS_VOLTAGE]) *
        BUS_VOLTAGE_DIVIDER_RATIO;
    // bias voltage uses a 402k, 100k voltage divider
    state.bias_voltage_v =
        static_cast<float>(state.raw_voltages[::sentry_thruster_servo::State::RAW_VOLTAGE_INDEX_12_5V_VOLTAGE]) *
        BIAS_VOLTAGE_DIVIDER_RATIO;
    // 6.5V uses a 49.9k, 100k voltage divider
    state.osc_voltage_v =
        static_cast<float>(state.raw_voltages[::sentry_thruster_servo::State::RAW_VOLTAGE_INDEX_6_5V_VOLTAGE]) *
        OSC_VOLTAGE_DIVIDER_RATIO;

    // Thermistor slope is unknown, so we report value of resistance
    state.temperature_kohms = thermistor_kohms(
        static_cast<float>(state.raw_voltages[::sentry_thruster_servo::State::RAW_VOLTAGE_INDEX_TEMPERATURE]));
    state.update = sentry_thruster_servo::State::UPDATE_VOLTAGES;
  }
  // Don't care about this response, return false
  else
  {
    return false;
  }

  // update index state
  auto index_state = ::sentry_thruster_servo::State::INDEX_STATE_UNKNOWN;
  switch (response[last_etx_indx - 1])
  {
    case ':':
      index_state = ::sentry_thruster_servo::State::INDEX_STATE_INDEXED;
      break;
    case '*':
      index_state = ::sentry_thruster_servo::State::INDEX_STATE_UNINDEXED;
      break;
    case 'c':
      index_state = ::sentry_thruster_servo::State::INDEX_STATE_INDEXING;
      break;
    default:
      index_state = ::sentry_thruster_servo::State::INDEX_STATE_UNKNOWN;
      break;
  }

  if (state.index_state != index_state)
  {
    state.index_state = index_state;
    state.update |= ::sentry_thruster_servo::State::UPDATE_INDEX_STATE;
  }

  // update the timestamp
  if (!timestamp.isZero())
  {
    state.header.stamp = timestamp;
    state.ds_header.io_time = timestamp;
  }
  return true;
}

std::string queryCommand(const std::string& address, PollCommands command)
{
  auto os = std::ostringstream{};

  switch (command)
  {
    case PollCommands::QUERY_SWITCH:
      os << "#" << address << QUERY_SWITCH_STR;
      break;
    case PollCommands::QUERY_LOCATION:
      os << "#" << address << QUERY_LOCATION_STR;
      break;
    case PollCommands::QUERY_SPEED:
      os << "#" << address << QUERY_SPEED_STR;
      break;
    case PollCommands::QUERY_INDEX_CROSSING:
      os << "#" << address << QUERY_INDEX_CROSSING_STR;
      break;
    case PollCommands::QUERY_VOLTAGES:
      os << "#" << address << QUERY_VOLTAGES_STR;
      break;
    case PollCommands::SET_LOCATION:
      ROS_ERROR_STREAM("use setLocationCommand to create a set location command");
      break;
    default:
      ROS_WARN_STREAM("Invalid PollCommand enum value: " << static_cast<int>(command));
      break;
  }
  return os.str();
}

std::string setLocationCommand(const std::string& address, float radians)
{
  auto os = std::ostringstream{};

  const auto counts = static_cast<std::int16_t>(radians * COUNTS_PER_RADIAN);
  os << "#" << address << SET_LOCATION_STR << std::uppercase << std::setw(4) << std::setfill('0') << std::hex << counts
     << "\r";
  return os.str();
}

std::string setSpeedAndLocationCommand(const std::string& address, std::uint8_t speed, float radians)
{
  auto os = std::ostringstream{};

  const auto counts = static_cast<std::int16_t>(radians * COUNTS_PER_RADIAN);
  os << "#" << address << SET_SPEED_AND_LOCATION_STR << std::uppercase << std::hex << std::setw(2) << std::setfill('0')
     << static_cast<int>(speed) << "." << std::setw(4) << counts << "\r";
  return os.str();
}

std::string servoCommand(const std::string& address, ServoCommand command)
{
  auto os = std::ostringstream{};
  os << "#" << address;
  switch (command)
  {
    case ServoCommand::INDEX:
      os << "I";
      break;
    case ServoCommand::BRAKE:
      os << "B";
      break;
    case ServoCommand::COAST:
      os << "C";
      break;
    case ServoCommand::SLEEP:
      os << "Z";
      break;
  }
  return os.str();
}

}  // namespace sentry_thruster_servo
