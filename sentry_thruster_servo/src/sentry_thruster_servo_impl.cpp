#include "sentry_thruster_servo_impl.h"
#include "util.h"

#include <ros/console.h>
#include <ros/assert.h>

#include <ostream>

namespace sentry_thruster_servo
{
SentryThrusterServoImpl::SentryThrusterServoImpl() = default;
SentryThrusterServoImpl::~SentryThrusterServoImpl() = default;

std::string SentryThrusterServoImpl::nextPeriodicCommand() noexcept
{
  // Queue not empty, pop next message and use it.
  if (!m_command_queue.empty())
  {
    const auto command = m_command_queue.front();
    m_command_queue.pop();
    ROS_DEBUG_STREAM("Popped command '" << command << "' off the queue.  " << m_command_queue.size()
                                        << " queued commands remain.");
    return command;
  }

  // advance index now
  const auto index = m_poll_index;
  m_poll_index = (m_poll_index + 1) % static_cast<int>(PollCommands::N_POLL_COMMANDS);

  // queue is empty and it is time to update the location.
  // We only send a new location upate if we're enabled, indexed, and have a pending location to send
  if (index == static_cast<int>(PollCommands::SET_LOCATION))
  {
    if (enabled() && indexed() && locationPending())
    {
      m_has_pending_location = false;
      if (state().speed_raw != speed())
      {
        return setSpeedAndLocationCommand(address(), speed(), m_pending_location_radians);
      }
      else
      {
        return setLocationCommand(address(), m_pending_location_radians);
      }
    }
    else
    {
      const auto command = static_cast<PollCommands>(m_poll_index);
      m_poll_index = (m_poll_index + 1) % static_cast<int>(PollCommands::N_POLL_COMMANDS);
      return queryCommand(m_address, command);
    }
  }
  else
  {
    // Send next command from the polling loop.
    const auto command = static_cast<PollCommands>(index);
    return queryCommand(m_address, command);
  }
}

void SentryThrusterServoImpl::sendNextPeriodicCommand() noexcept
{
  // check if response still pending
  if (!m_pending_command.empty())
  {
    ROS_WARN_STREAM("timed out waiting for response to command '" << m_pending_command << "'");
    m_pending_command.clear();
  }

  const auto command = nextPeriodicCommand();

  sendString(command);
}

bool SentryThrusterServoImpl::setLocation(float radians) noexcept
{
  if (enabled() && indexed())
  {
    ROS_DEBUG_STREAM("set pending location: " << radians);
    m_pending_location_radians = radians;
    m_has_pending_location = true;
    return true;
  }
  ROS_WARN_STREAM("unable to set pending location. enabled:  " << std::boolalpha << enabled()
                                                               << " indexed: " << indexed());
  return false;
}

bool SentryThrusterServoImpl::handleResponse(const std::string& response, const ros::Time& timestamp)
{
  ROS_DEBUG_STREAM("rx: " << response);

  const auto result = m_local_echo ? updateState(m_pending_command + response, address(), m_state, timestamp) :
                                     updateState(response, address(), m_state, timestamp);
  if (result)
  {
    m_pending_command.clear();
  }
  return result;
}

bool SentryThrusterServoImpl::sendServoCommand(ServoCommand command)
{
  if (!enabled())
  {
    return false;
  }

  const auto command_string = servoCommand(address(), command);
  m_command_queue.push(command_string);
  return true;
}

void SentryThrusterServoImpl::setSpeedDirect(std::uint8_t speed)
{
  auto os = std::ostringstream{};
  os << "#" << address() << "!M77 1 " << std::hex << std::setw(2) << std::setfill('0') << std::uppercase
     << static_cast<int>(speed) << "\r";
}
}  // namespace sentry_thruster_servo
