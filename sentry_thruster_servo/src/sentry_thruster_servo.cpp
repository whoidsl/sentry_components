#include "sentry_thruster_servo.h"
#include "sentry_thruster_servo_impl.h"
#include "util.h"

#include <ds_actuator_msgs/ServoCmd.h>
#include <ds_actuator_msgs/ServoState.h>
#include <ds_core_msgs/RawData.h>
#include <ds_core_msgs/Abort.h>
#include <sensor_msgs/JointState.h>
#include <sentry_msgs/SailServoCmd.h>

#include <ros/publisher.h>
#include <ros/service_server.h>

#include <memory>

namespace sentry_thruster_servo
{
constexpr double SentryThrusterServo::DEFAULT_POLL_RATE_HZ;
constexpr std::uint8_t SentryThrusterServo::DEFAULT_SPEED;

constexpr char SentryThrusterServo::PARAM_POLL_RATE_HZ[];
constexpr char SentryThrusterServo::PARAM_URDF_JOINT_NAME[];
constexpr char SentryThrusterServo::PARAM_ABORT_TOPIC[];
constexpr char SentryThrusterServo::PARAM_SAIL_ADDRESS[];
constexpr char SentryThrusterServo::PARAM_SPEED[];

constexpr char SentryThrusterServo::TOPIC_JOINT_STATE[];
constexpr char SentryThrusterServo::TOPIC_SERVO_STATE[];
constexpr char SentryThrusterServo::TOPIC_SENTRY_SERVO_STATE[];
constexpr char SentryThrusterServo::TOPIC_SET_LOCATION[];

constexpr char SentryThrusterServo::SERVICE_SERVO_COMMAND[];

class SentryThrusterServoPrivate : public SentryThrusterServoImpl
{
public:
  SentryThrusterServoPrivate()
  {
    m_joint_state.name.resize(1);
    m_joint_state.position.resize(1);
  }

  void setPollingEnabled(bool enabled) noexcept override
  {
    enabled ? m_timer.start() : m_timer.stop();
    SentryThrusterServoImpl::setPollingEnabled(enabled);
  }

  void sendString(const std::string& s) override
  {
    SentryThrusterServoImpl::sendString(s);
    m_instrument_connection->send(s);
  }

  void setEnabled(bool enabled) noexcept override
  {
    const auto old_state = SentryThrusterServoImpl::enabled();
    SentryThrusterServoImpl::setEnabled(enabled);
    if (old_state != enabled)
    {
      m_servo_state_pub.publish(state());
    }
  }

  void handleIncomingBytes(ds_core_msgs::RawData incoming)
  {
    // Successful parsing, publish topics.
    const auto response = std::string{ std::begin(incoming.data), std::end(incoming.data) };
    if (SentryThrusterServoImpl::handleResponse(response, incoming.ds_header.io_time))
    {
      const auto state_ = state();
      // sentry_thruster_servo::State
      m_sentry_servo_state_pub.publish(state_);

      // servo state
      m_servo_state.header.stamp = incoming.header.stamp;
      m_servo_state.ds_header.io_time = incoming.header.stamp;

      m_servo_state.actual_counts = state_.angle_raw;
      m_servo_state.actual_radians = state_.angle_radians;

      m_servo_state.cmd_counts = state_.set_angle_raw;
      m_servo_state.cmd_radians = state_.set_angle_radians;

      m_servo_state.enable = enabled();
      m_servo_state.servo_name = m_joint_state.name.at(0);
      m_servo_state_pub.publish(m_servo_state);

      // joint state
      m_joint_state.header.stamp = incoming.header.stamp;
      m_joint_state.position[0] = state().angle_radians;
      m_joint_state_pub.publish(m_joint_state);
    }
  }

  ros::Timer m_timer;
  boost::shared_ptr<ds_asio::DsConnection> m_instrument_connection;
  sensor_msgs::JointState m_joint_state;
  ds_actuator_msgs::ServoState m_servo_state;
  ros::Publisher m_joint_state_pub;
  ros::Publisher m_servo_state_pub;
  ros::Publisher m_sentry_servo_state_pub;
  ros::ServiceServer m_servo_command_service;
  ros::Subscriber m_abort_topic_sub;
  ros::Subscriber m_location_topic_sub;
};

SentryThrusterServo::SentryThrusterServo()
  : ds_base::DsProcess(), d_ptr_(std::unique_ptr<SentryThrusterServoPrivate>(new SentryThrusterServoPrivate))
{
}

SentryThrusterServo::SentryThrusterServo(int argc, char* argv[], const std::string& name)
  : ds_base::DsProcess(argc, argv, name)
  , d_ptr_(std::unique_ptr<SentryThrusterServoPrivate>(new SentryThrusterServoPrivate))
{
}

SentryThrusterServo::~SentryThrusterServo() = default;

void SentryThrusterServo::setupConnections()
{
  ds_base::DsProcess::setupConnections();
  DS_D(SentryThrusterServo);
  const auto instrument = addConnection(
      "instrument", std::bind(&SentryThrusterServoPrivate::handleIncomingBytes, d, std::placeholders::_1));

  if (!instrument)
  {
    ROS_FATAL("no connection parameter named 'instrument'");
    ROS_BREAK();
  }

  d->m_instrument_connection = instrument;
}

void SentryThrusterServo::setupParameters()
{
  ds_base::DsProcess::setupParameters();

  DS_D(SentryThrusterServo);

  auto param_name = nodeHandle("~").resolveName(&PARAM_SAIL_ADDRESS[1]);
  auto address = std::string{};
  ROS_ASSERT_MSG(ros::param::get(param_name, address), "Missing servo sail address parameter: '%s'",
                 param_name.c_str());
  d->setAddress(std::move(address));
  ROS_INFO_STREAM("Using sail address: " << d->address());

  param_name = nodeHandle("~").resolveName(&PARAM_URDF_JOINT_NAME[1]);
  ROS_ASSERT_MSG(ros::param::get(param_name, d->m_joint_state.name[0]), "Missing servo joint name parameter: '%s'",
                 param_name.c_str());
  ROS_INFO_STREAM("Using URDF joint name: " << d->m_joint_state.name.at(0));

  const auto speed_ = static_cast<std::uint8_t>(ros::param::param<int>(PARAM_SPEED, DEFAULT_SPEED));
  d->setSpeed(speed_);
  ROS_INFO_STREAM("Servo speed set to: 0x" << std::setw(2) << std::hex << std::uppercase << std::setfill('0')
                                           << static_cast<int>(speed_));
}

void SentryThrusterServo::setupTimers()
{
  ds_base::DsProcess::setupTimers();
  auto nh = nodeHandle();
  DS_D(SentryThrusterServo);
  const auto rate = ros::param::param<double>(PARAM_POLL_RATE_HZ, DEFAULT_POLL_RATE_HZ);
  ROS_INFO_STREAM("Polling servo with rate: " << rate << " Hz.");

  d->m_timer = nh.createTimer(ros::Rate(rate).expectedCycleTime(),
                              std::bind(&SentryThrusterServoPrivate::sendNextPeriodicCommand, d), false, false);
  d->setPollingEnabled(true);
}

void SentryThrusterServo::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();
  DS_D(SentryThrusterServo);
  d->m_joint_state_pub = nodeHandle().advertise<sensor_msgs::JointState>(TOPIC_JOINT_STATE, 10, false);
  d->m_servo_state_pub = nodeHandle("~").advertise<ds_actuator_msgs::ServoState>(&TOPIC_SERVO_STATE[1], 10, false);
  d->m_sentry_servo_state_pub =
      nodeHandle("~").advertise<::sentry_thruster_servo::State>(&TOPIC_SENTRY_SERVO_STATE[1], 10, false);
}

void SentryThrusterServo::setupSubscriptions()
{
  ds_base::DsProcess::setupSubscriptions();
  // private namespace nodehandle
  auto p_nh = nodeHandle("~");

  const auto abort_topic_path = p_nh.resolveName(&PARAM_ABORT_TOPIC[1]);
  auto abort_topic = std::string{};
  ROS_ASSERT_MSG(ros::param::get(abort_topic_path, abort_topic), "Missing abort topic parameter: '%s'",
                 abort_topic_path.c_str());
  ROS_INFO_STREAM("Using abort topic: " << abort_topic);

  DS_D(SentryThrusterServo);
  d->m_abort_topic_sub = p_nh.subscribe<ds_core_msgs::Abort>(
      abort_topic, 10, [&](const ds_core_msgs::AbortConstPtr& msg) { setEnabled(msg->enable); });
  d->m_location_topic_sub = p_nh.subscribe<ds_actuator_msgs::ServoCmd>(
      &TOPIC_SET_LOCATION[1], 10, [&](const ds_actuator_msgs::ServoCmdConstPtr msg) { setLocation(*msg); });
}

void SentryThrusterServo::checkProcessStatus(const ros::TimerEvent& event)
{
  DsProcess::checkProcessStatus(event);
  DS_D(SentryThrusterServo);
  auto status_message = statusMessage();

  const auto last_message_age = event.current_real - d->state().header.stamp;

  // Last data from servo is older than 5 seconds - probably a comms issue
  if (last_message_age > ros::Duration(5.0))
  {
    status_message.status = ds_core_msgs::Status::STATUS_ERROR;
  }
  // Servo is enabled and indexed - good normal state
  else if (d->enabled() && d->state().index_state == ::sentry_thruster_servo::State::INDEX_STATE_INDEXED)
  {
    status_message.status = ds_core_msgs::Status::STATUS_GOOD;
  }
  // everything else is a warning
  else
  {
    status_message.status = ds_core_msgs::Status::STATUS_WARN;
  }
  publishStatus(status_message);
}

void SentryThrusterServo::setupServices()
{
  DsProcess::setupServices();
  DS_D(SentryThrusterServo);

  d->m_servo_command_service =
      nodeHandle("~").advertiseService<sentry_msgs::SailServoCmd::Request, sentry_msgs::SailServoCmd::Response>(
          &SERVICE_SERVO_COMMAND[1],
          std::bind(static_cast<bool (SentryThrusterServo::*)(sentry_msgs::SailServoCmd::Request&,
                                                              sentry_msgs::SailServoCmd::Response&)>(
                        &SentryThrusterServo::sendServoCommand),
                    this, std::placeholders::_1, std::placeholders::_2));
}

bool SentryThrusterServo::sendServoCommand(sentry_msgs::SailServoCmd::Request& request,
                                           sentry_msgs::SailServoCmd::Response& /*response*/)
{
  auto command = ServoCommand::INDEX;
  switch (request.command)
  {
    case sentry_msgs::SailServoCmd::Request::SERVO_CMD_INDEX:
      command = ServoCommand::INDEX;
      break;
    case sentry_msgs::SailServoCmd::Request::SERVO_CMD_BRAKE:
      command = ServoCommand::BRAKE;
      break;
    case sentry_msgs::SailServoCmd::Request::SERVO_CMD_SLEEP:
      command = ServoCommand::SLEEP;
      break;
    case sentry_msgs::SailServoCmd::Request::SERVO_CMD_COAST:
      command = ServoCommand::COAST;
      break;
    default:
      ROS_WARN_STREAM("Invalid servo command enum value: " << static_cast<int>(request.command));
      return false;
  }

  return sendServoCommand(command);
}

bool SentryThrusterServo::sendServoCommand(ServoCommand command)
{
  DS_D(SentryThrusterServo);
  return d->sendServoCommand(command);
}

void SentryThrusterServo::setAddress(const std::string& address) noexcept
{
  DS_D(SentryThrusterServo);
  d->setAddress(address);
}

const std::string& SentryThrusterServo::address() const noexcept
{
  const DS_D(SentryThrusterServo);
  return d->address();
}

void SentryThrusterServo::setEnabled(bool enabled) noexcept
{
  DS_D(SentryThrusterServo);
  d->setEnabled(enabled);
}

bool SentryThrusterServo::enabled() const noexcept
{
  const DS_D(SentryThrusterServo);
  return d->enabled();
}

bool SentryThrusterServo::setLocation(float radians) noexcept
{
  DS_D(SentryThrusterServo);
  return d->setLocation(radians);
}

bool SentryThrusterServo::setLocation(const ds_actuator_msgs::ServoCmd& msg)
{
  return setLocation(msg.cmd_radians);
}
}  // namespace sentry_thruster_servo
