#include "sentry_thruster_servo.h"

using namespace sentry_thruster_servo;
int main(int argc, char* argv[])
{
  SentryThrusterServo node(argc, argv, "sentry_thruster_servo");
  node.run();
  return 0;
}