#ifndef SENTRY_THRUSTER_SERVO_UTIL_H
#define SENTRY_THRUSTER_SERVO_UTIL_H

#include <sentry_thruster_servo/State.h>

#include <cstring>

namespace sentry_thruster_servo
{
constexpr auto QUERY_SWITCH_STR = "?M0 1\r";
constexpr auto QUERY_SWITCH_STRLEN = std::strlen(QUERY_SWITCH_STR);
constexpr auto QUERY_SWITCH_RESPONSE_STRLEN = std::strlen("\r\nXXXX XX\r\n*\x03");

constexpr auto QUERY_LOCATION_STR = "?L";
constexpr auto QUERY_LOCATION_STRLEN = std::strlen(QUERY_LOCATION_STR);

constexpr auto SET_LOCATION_STR = "!L";
constexpr auto SET_LOCATION_STRLEN = std::strlen(SET_LOCATION_STR);

constexpr auto SET_SPEED_AND_LOCATION_STR = "!P";
constexpr auto SET_SPEED_AND_LOCATION_STRLEN = std::strlen(SET_SPEED_AND_LOCATION_STR);

constexpr auto QUERY_INDEX_CROSSING_STR = "?I";
constexpr auto QUERY_INDEX_CROSSING_STRLEN = std::strlen(QUERY_INDEX_CROSSING_STR);

constexpr auto QUERY_SPEED_STR = "?M77 1\r";
constexpr auto QUERY_SPEED_STRLEN = std::strlen(QUERY_SPEED_STR);
constexpr auto QUERY_SPEED_RESPONSE_STRLEN = std::strlen("\r\nXXXX XX\r\n*\x03");

constexpr auto QUERY_VOLTAGES_STR = "?V";
constexpr auto QUERY_VOLTAGES_STRLEN = std::strlen(QUERY_VOLTAGES_STR);
constexpr auto QUERY_VOLTAGES_RESPONSE_STRLEN = std::strlen(" XX XX XX XX\r\n*\x03");

// bus voltage uses a 1M, 80.6k voltage divider
constexpr auto BUS_VOLTAGE_DIVIDER_RATIO = 5.0 / 255.0 * 1080.6 / 80.6;
// bias voltage uses a 402k, 100k voltage divider
constexpr auto BIAS_VOLTAGE_DIVIDER_RATIO = 5.0 / 255.0 * 502.0 / 100.0;
// 6.5V uses a 49.9k, 100k voltage divider
constexpr auto OSC_VOLTAGE_DIVIDER_RATIO = 5.0 / 255.0 * 49.9 / 149.9;

auto thermistor_kohms(std::float_t voltage) -> std::float_t;

enum class PollCommands
{
  QUERY_SPEED = 0,
  QUERY_LOCATION,
  QUERY_INDEX_CROSSING,
  QUERY_VOLTAGES,
  QUERY_SWITCH,
  SET_LOCATION,
  N_POLL_COMMANDS,
};

enum class ServoCommand
{
  INDEX,
  BRAKE,
  COAST,
  SLEEP,
};

constexpr float COUNTS_PER_RADIAN = 1163.7f;

[[nodiscard]] bool updateState(const std::string& response, const std::string& address,
                               ::sentry_thruster_servo::State& state,
                               const ros::Time& timestamp = ros::Time::ZERO) noexcept;

/// Build a servo position command string
///
/// \param address: Servo address, without the '#' (e.g. S4)
/// \param radians: Commanded position
///
/// \return command string
///
/// The thruster servo position is given as a hex-encoded two's compliment signed 16bit integer
std::string setLocationCommand(const std::string& address, float radians);
std::string setSpeedAndLocationCommand(const std::string& address, std::uint8_t speed, float radians);
std::string queryCommand(const std::string& address, PollCommands command);
std::string servoCommand(const std::string& address, ServoCommand command);
}  // namespace sentry_thruster_servo
#endif
