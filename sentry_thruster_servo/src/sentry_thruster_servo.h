#ifndef SENTRY_THRUSTER_SERVO_H
#define SENTRY_THRUSTER_SERVO_H

#include "util.h"

#include <ds_base/ds_process.h>
#include <ds_base/ds_global.h>
#include <sentry_msgs/SailServoCmd.h>

#include <ros/timer.h>
#include <ros/rate.h>

namespace ds_actuator_msgs
{
ROS_DECLARE_MESSAGE(ServoCmd);
}

namespace sentry_thruster_servo
{
class SentryThrusterServoPrivate;

class SentryThrusterServo : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(SentryThrusterServo);

public:
  static constexpr char PARAM_POLL_RATE_HZ[] = "~poll_rate";
  static constexpr double DEFAULT_POLL_RATE_HZ = 5.0;

  static constexpr char PARAM_URDF_JOINT_NAME[] = "~urdf_joint_name";
  static constexpr char PARAM_ABORT_TOPIC[] = "~abort_topic";

  static constexpr std::uint8_t DEFAULT_SPEED = 0x90;
  static constexpr char PARAM_SPEED[] = "~speed";

  static constexpr char PARAM_SAIL_ADDRESS[] = "~sail_address";

  static constexpr char TOPIC_JOINT_STATE[] = "joint_states";
  static constexpr char TOPIC_SERVO_STATE[] = "~state";
  static constexpr char TOPIC_SENTRY_SERVO_STATE[] = "~sentry_servo_state";
  static constexpr char TOPIC_SET_LOCATION[] = "~cmd";

  static constexpr char SERVICE_SERVO_COMMAND[] = "~ctrl_cmd";

  SentryThrusterServo();
  SentryThrusterServo(int argc, char* argv[], const std::string& name);
  ~SentryThrusterServo() override;

  void setAddress(const std::string& address) noexcept;
  const std::string& address() const noexcept;

  void setEnabled(bool enabled) noexcept;
  bool enabled() const noexcept;

  bool setLocation(float radians) noexcept;
  bool setLocation(const ds_actuator_msgs::ServoCmd& msg);

  bool sendServoCommand(ServoCommand command);
  bool sendServoCommand(sentry_msgs::SailServoCmd::Request& request, sentry_msgs::SailServoCmd::Response& response);

protected:
  void setupConnections() override;
  void setupParameters() override;
  void setupTimers() override;
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupServices() override;
  void checkProcessStatus(const ros::TimerEvent& event) override;

private:
  std::unique_ptr<SentryThrusterServoPrivate> d_ptr_;
};

}  // namespace sentry_thruster_servo
#endif
