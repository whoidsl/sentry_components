#ifndef SENTRY_THRUSTER_SERVO_IMPL_H
#define SENTRY_THRUSTER_SERVO_IMPL_H

#include <sentry_thruster_servo/State.h>
#include "util.h"

#include <queue>
#include <memory>

namespace sentry_thruster_servo
{
class SentryThrusterServoImpl
{
public:
  SentryThrusterServoImpl();
  ~SentryThrusterServoImpl();

  virtual void setEnabled(bool enable) noexcept
  {
    // set to coast before disabling
    if (!enable && m_state.enabled)
    {
      sendServoCommand(ServoCommand::COAST);
      m_has_pending_location = false;
    }
    if (enable != m_state.enabled)
    {
      m_state.update = ::sentry_thruster_servo::State::UPDATE_ENABLED;
    }
    m_state.enabled = enable;
  }

  bool enabled() const noexcept
  {
    return m_state.enabled;
  }

  bool indexed() const noexcept
  {
    return state().index_state == ::sentry_thruster_servo::State::INDEX_STATE_INDEXED;
  }

  bool setLocation(float radians) noexcept;

  bool locationPending(float* radians = nullptr) const noexcept
  {
    if (m_has_pending_location && radians)
    {
      *radians = m_pending_location_radians;
    }
    return m_has_pending_location;
  }

  void setSpeed(std::uint8_t speed) noexcept
  {
    m_speed = speed;
  }

  std::uint8_t speed() const noexcept
  {
    return m_speed;
  }

  const std::queue<std::string>& pendingCommands() const noexcept
  {
    return m_command_queue;
  }

  bool sendServoCommand(ServoCommand command);

  void setSpeedDirect(std::uint8_t speed);

  virtual void setPollingEnabled(bool enable) noexcept
  {
    m_polling_enabled = enable;
  }

  virtual bool pollingEnabled() const noexcept
  {
    return m_polling_enabled;
  }

  void setAddress(const std::string& address)
  {
    m_address = address;
  }

  const std::string& address() const noexcept
  {
    return m_address;
  }

  std::string nextPeriodicCommand() noexcept;

  void sendNextPeriodicCommand() noexcept;

  const ::sentry_thruster_servo::State& state() const
  {
    return m_state;
  }

  virtual bool handleResponse(const std::string& response, const ros::Time& timestamp);

  virtual void sendString(const std::string& s)
  {
    m_pending_command = s;
  }

private:
  std::string m_address;

  std::queue<std::string> m_command_queue;
  std::string m_pending_command;
  std::size_t m_poll_index = 0;
  bool m_polling_enabled = false;
  bool m_local_echo = true;

  bool m_has_pending_location = false;
  float m_pending_location_radians = 0.0;

  std::uint8_t m_speed = 0xFF;
  ::sentry_thruster_servo::State m_state;
};
}  // namespace sentry_thruster_servo
#endif
