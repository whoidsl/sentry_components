#include "util.h"

#include <ds_base/ds_process.h>
#include <boost/asio.hpp>
#include <boost/asio/handler_invoke_hook.hpp>
#include <boost/asio/serial_port.hpp>

#include <ros/assert.h>
#include <ros/console.h>

#include <algorithm>
#include <math.h>

class FakeServo : public ds_base::DsProcess
{
public:
  // servo speed in rad/s at full-speed.  Estimated by the guess that
  // it takes about 3 seconds for the fins to move 90 degress at "normal" speed, which is
  // 0x90 out of 0xFF.
  //
  // NOTE:  Servo firmware *explicitly* states that the actual rotation speed is not a linear
  // function of the speed parameter, so this guess is really just that - a workable guess
  // for testing.
  static constexpr float SERVO_ROTATION_SPEED_RAD_PER_S = 90.0 / 3.0 * static_cast<float>(0xFF) / 0x90 * M_PI / 180.0;
  enum class ParserState
  {
    IDLE,
    QUERY,
    SET,
    SET_OR_QUERY,
    READ_QUERY_MEMORY,
    READ_SET_LOCATION,
    READ_SET_SPEED,
    READ_ADDRESS,
  };

  FakeServo() : DsProcess()
  {
  }
  FakeServo(int argc, char* argv[], const std::string& name) : DsProcess(argc, argv, name)
  {
  }

protected:
  void setupParameters() override
  {
    ds_base::DsProcess::setupParameters();
    nodeHandle("~").getParam("sail_address", m_sail_address);
    std::transform(std::begin(m_sail_address), std::end(m_sail_address), std::begin(m_sail_address),
                   [](unsigned char c) { return std::toupper(c); });
    ROS_INFO_STREAM("Fake servo using sail address: " << m_sail_address);
  }

  void setupConnections() override
  {
    ds_base::DsProcess::setupConnections();
    auto p_nh = nodeHandle("~");

    const auto port = ros::param::param<std::string>(p_nh.resolveName("instrument/port"), "/dev/ttyUSB0");
    m_serial_port = std::unique_ptr<boost::asio::serial_port>(new boost::asio::serial_port(asio()->io_service));
    try
    {
      m_serial_port->open(port);
    }
    catch (boost::system::system_error e)
    {
      ROS_FATAL_STREAM("Failed to open serial port: " << port);
      ROS_FATAL_STREAM("boost system error code: " << e.code() << " " << e.what());
    }
    ROS_ASSERT_MSG(m_serial_port->is_open(), "Unable to open serial port: %s", port.c_str());

    m_serial_port->async_read_some(
        boost::asio::buffer((void*)&m_incoming, 1),
        std::bind(&FakeServo::m_handler, this, std::placeholders::_1, std::placeholders::_2));
  }

  void setupTimers() override
  {
    m_timer =
        nodeHandle().createTimer(ros::Duration(0.1), std::bind(&FakeServo::m_updateState, this, std::placeholders::_1));
  }

  virtual void sendResponse(const std::string& response)
  {
    const auto buffer = boost::asio::const_buffer(response.data(), response.size());
    m_serial_port->write_some(buffer);
  }

  void parseChar(char c)
  {
    switch (m_state)
    {
      case ParserState::IDLE: {
        if (c == '#')
        {
          m_state = ParserState::READ_ADDRESS;
        }
        break;
      }
      case ParserState::READ_ADDRESS: {
        m_parser_buffer.push_back(c);
        if (m_parser_buffer.size() == m_sail_address.size())
        {
          const auto address = std::string{ std::begin(m_parser_buffer), std::end(m_parser_buffer) };
          if (address == m_sail_address)
          {
            m_state = ParserState::SET_OR_QUERY;
            m_parser_buffer.clear();
          }
          else
          {
            resetState();
          }
        }
        break;
      }
      case ParserState::SET_OR_QUERY: {
        if (c == '!')
        {
          m_state = ParserState::SET;
        }
        else if (c == '?')
        {
          m_state = ParserState::QUERY;
        }
        else if (c == 'C')
        {
          m_enabled = false;
        }
        else if (c == 'I')
        {
          m_desired_location = 0x0000;
          m_enabled = true;
          m_prompt = 'c';
          auto os = std::ostringstream{};
          os << "#" << m_sail_address << "I\r\nc\x03";
          sendResponse(os.str());
        }
        else if (c == 'Z')
        {
          m_desired_location = 0x0000;
          m_enabled = false;
          m_prompt = '*';
          auto os = std::ostringstream{};
          os << "#" << m_sail_address << "Z\r\n*\x03";
          sendResponse(os.str());
        }
        else
        {
          resetState();
        }
        break;
      }
      case ParserState::SET: {
        if (c == 'L')
        {
          m_state = ParserState::READ_SET_LOCATION;
        }
        else if (c == 'P')
        {
          m_state = ParserState::READ_SET_SPEED;
        }
        else
        {
          resetState();
        }
        break;
      }
      case ParserState::QUERY: {
        if (c == 'M')
        {
          m_state = ParserState::READ_QUERY_MEMORY;
        }
        else if (c == 'L')
        {
          auto os = std::ostringstream{};
          os << "#" << m_sail_address << "?L" << std::hex << std::uppercase << std::setw(4) << std::setfill('0')
             << m_location << "\r\n"
             << m_prompt << '\x03';
          sendResponse(os.str());
          resetState();
        }
        else if (c == 'I')
        {
          auto os = std::ostringstream{};
          os << "#" << m_sail_address << "?I"
             << "0000\r\n"
             << m_prompt << '\x03';
          sendResponse(os.str());
          resetState();
        }
        else
        {
          resetState();
        }
        break;
      }
      case ParserState::READ_SET_LOCATION: {
        m_parser_buffer.push_back(c);
        if (m_parser_buffer.size() == 4)
        {
          const auto hex_str = std::string{ std::begin(m_parser_buffer), std::end(m_parser_buffer) };
          try
          {
            m_desired_location = std::stoi(hex_str, 0, 16);
          }
          catch (std::exception)
          {
          }
          resetState();
        }
        break;
      }
      case ParserState::READ_SET_SPEED: {
        m_parser_buffer.push_back(c);
        if (m_parser_buffer.size() == 7)
        {
          std::uint8_t speed;
          std::int16_t location;
          const auto s = std::string{ std::begin(m_parser_buffer), std::end(m_parser_buffer) };
          if (2 == std::sscanf(s.c_str(), "%hhX.%hX", &speed, &location))
          {
            m_speed = speed;
            m_desired_location = location;
          }
          resetState();
        }
        break;
      }
      case ParserState::READ_QUERY_MEMORY: {
        m_parser_buffer.push_back(c);
        if (c == '\r')
        {
          unsigned int address;
          int count;
          const auto msg = std::string{ std::begin(m_parser_buffer), std::end(m_parser_buffer) };
          if (2 == sscanf(msg.c_str(), "%X %d\r", &address, &count))
          {
            auto os = std::ostringstream{};
            if (address == 0x77 && count == 1)
            {
              os << "#" << m_sail_address << "?M77 1\r" << std::hex << std::uppercase << std::setw(2)
                 << std::setfill('0') << static_cast<int>(m_speed) << "\r\n"
                 << m_prompt << '\x03';
            }
            else if (address == 0 && count == 1)
            {
              os << "#" << m_sail_address << "?M0 1\r" << std::hex << std::uppercase << std::setw(2)
                 << std::setfill('0') << static_cast<int>(m_location > 0) << "\r\n"
                 << m_prompt << '\x03';
            }
            const auto response = os.str();
            if (response.size() > 0)
            {
              sendResponse(response);
            }
            resetState();
          }
        }
        break;
      }
    }
  }

private:
  void resetState()
  {
    m_parser_buffer.clear();
    m_state = ParserState::IDLE;
  }
  std::unique_ptr<boost::asio::serial_port> m_serial_port;
  ParserState m_state = ParserState::IDLE;
  std::string m_sail_address = "S1";
  std::vector<std::uint8_t> m_parser_buffer;
  std::uint8_t m_speed = 0xFF;
  std::int16_t m_desired_location = 0x0000;
  std::int16_t m_location = 0x0000;
  char m_prompt = '*';
  char m_incoming;
  ros::Timer m_timer;
  bool m_enabled = false;

  void m_handler(boost::system::error_code ec, std::size_t size)
  {
    if (!ec.failed())
    {
      parseChar(m_incoming);
    }
    m_serial_port->async_read_some(
        boost::asio::buffer((void*)&m_incoming, 1),
        std::bind(&FakeServo::m_handler, this, std::placeholders::_1, std::placeholders::_2));
  }

  void m_updateState(const ros::TimerEvent& event)
  {
    if (!m_enabled)
    {
      return;
    }

    if (m_prompt == ':' && m_desired_location == m_location)
    {
      return;
    }

    const auto dt = event.current_real - event.last_real;
    const auto raw_step = SERVO_ROTATION_SPEED_RAD_PER_S * static_cast<float>(m_speed) / 0xFF * dt.toSec() *
                          sentry_thruster_servo::COUNTS_PER_RADIAN;
    const auto step = std::min(static_cast<int>(raw_step), std::abs(m_desired_location - m_location));
    ROS_INFO_STREAM("dt: " << dt.toSec() << " raw step: " << raw_step << " step: " << step);

    m_location = m_desired_location < m_location ? m_location - step : m_location + step;
    if (m_prompt == 'c' && m_desired_location == m_location)
    {
      m_prompt = ':';
    }
  }
};

int main(int argc, char* argv[])
{
  FakeServo node(argc, argv, "fake_servo");
  node.run();
  return 0;
}
