#include "util.h"

#include <gtest/gtest.h>

#include <tuple>

using namespace sentry_thruster_servo;

using PollCommandTestArgs = std::tuple<std::string, PollCommands, std::string>;

class PollCommandTest : public testing::TestWithParam<PollCommandTestArgs>
{
};

TEST_P(PollCommandTest, toString)
{
  std::string address;
  PollCommands command;
  std::string expected;
  std::tie(address, command, expected) = GetParam();

  const auto result = queryCommand(address, command);
  EXPECT_STREQ(result.c_str(), expected.c_str());
}

const PollCommandTestArgs TestPollCommands[] = {
  PollCommandTestArgs{ "S5", PollCommands::QUERY_INDEX_CROSSING, "#S5?I" },
  PollCommandTestArgs{ "S1", PollCommands::QUERY_SPEED, "#S1?M77 1\r" },
  PollCommandTestArgs{ "S3", PollCommands::QUERY_LOCATION, "#S3?L" },
  PollCommandTestArgs{ "S9", PollCommands::QUERY_VOLTAGES, "#S9?V" },
  PollCommandTestArgs{ "S9", PollCommands::QUERY_SWITCH, "#S9?M0 1\r" },
  PollCommandTestArgs{ "S3", PollCommands::SET_LOCATION, "" },
  PollCommandTestArgs{ "S9", PollCommands::N_POLL_COMMANDS, "" },
};

INSTANTIATE_TEST_SUITE_P(PollCommands, PollCommandTest, testing::ValuesIn(TestPollCommands));

using LocationCommandTestArgs = std::tuple<std::string, float, float, std::string>;

class LocationCommandTest : public testing::TestWithParam<LocationCommandTestArgs>
{
};

TEST_P(LocationCommandTest, toString)
{
  std::string address;
  float radians;
  float resolution;
  std::string expected;
  std::tie(address, radians, resolution, expected) = GetParam();

  const auto result = setLocationCommand(address, radians);
  EXPECT_STREQ(result.c_str(), expected.c_str());
}

const LocationCommandTestArgs LocationCommands[] = {
  LocationCommandTestArgs{ "S1", 0.1, COUNTS_PER_RADIAN, "#S1!L0074\r" },
  LocationCommandTestArgs{ "S1", -0.1, COUNTS_PER_RADIAN, "#S1!LFF8C\r" },  // 2's compliment: 0xFFFF - 0x0074 + 1
  LocationCommandTestArgs{ "S5", -0.0, COUNTS_PER_RADIAN, "#S5!L0000\r" },
};

INSTANTIATE_TEST_SUITE_P(LocationCommands, LocationCommandTest, testing::ValuesIn(LocationCommands));

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
