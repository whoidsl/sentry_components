#include "sentry_thruster_servo_impl.h"
#include "util.h"

#include <gtest/gtest.h>
#include <math.h>

using namespace sentry_thruster_servo;

class Behavior : public SentryThrusterServoImpl, public ::testing::Test
{
protected:
  void SetUp() override
  {
    setAddress("S5");
  }

  void sendString(const std::string& s) override
  {
    SentryThrusterServoImpl::sendString(s);
    m_last_sent = s;
  }

  void setIndexed()
  {
    const auto msg = std::string{ "#S5?L0000\r\n:\x03" };
    handleResponse(msg, ros::Time::ZERO);
  }

  void setUnindexed()
  {
    const auto msg = std::string{ "#S5?L0000\r\n*\x03" };
    handleResponse(msg, ros::Time::ZERO);
  }

  void setIndexing()
  {
    const auto msg = std::string{ "#S5?L0000\r\nc\x03" };
    handleResponse(msg, ros::Time::ZERO);
  }

  std::string m_last_sent;
};

TEST_F(Behavior, set_location_fails_when_disabled)
{
  ASSERT_FALSE(enabled());
  ASSERT_FALSE(setLocation(0.5));
  auto radians = -10.0f;
  ASSERT_FALSE(locationPending(&radians));
  ASSERT_FLOAT_EQ(radians, -10.0);
}

TEST_F(Behavior, set_location_fails_when_not_indexed)
{
  setEnabled(true);
  ASSERT_TRUE(enabled());
  ASSERT_FALSE(indexed());
  ASSERT_FALSE(setLocation(0.5));
  auto radians = -10.0f;
  ASSERT_FALSE(locationPending(&radians));
  ASSERT_FLOAT_EQ(radians, -10.0);
}

TEST_F(Behavior, set_location_passes_when_enabled_and_indexed)
{
  setEnabled(true);
  setIndexed();
  ASSERT_TRUE(enabled());
  ASSERT_TRUE(indexed());
  ASSERT_TRUE(setLocation(0.5));
  auto radians = -10.0f;
  ASSERT_TRUE(locationPending(&radians));
  ASSERT_FLOAT_EQ(radians, 0.5);
}

TEST_F(Behavior, disabling_when_enabled_sets_coast_and_clears_pending_location)
{
  // enable and index servo
  setEnabled(true);
  setIndexed();
  ASSERT_TRUE(enabled());
  ASSERT_TRUE(indexed());
  // set a location pending
  ASSERT_TRUE(setLocation(0.5));
  auto radians = -10.0f;
  ASSERT_TRUE(locationPending(&radians));
  ASSERT_FLOAT_EQ(radians, 0.5);

  // disable servo
  setEnabled(false);
  ASSERT_FALSE(enabled());
  // queue should hold the coast command
  const auto expected_command = servoCommand(address(), ServoCommand::COAST);
  const auto queue_ = pendingCommands();
  ASSERT_EQ(queue_.size(), 1);
  ASSERT_STREQ(queue_.front().c_str(), expected_command.c_str());

  // pending location should be cleared
  radians = -10.0;
  ASSERT_FALSE(locationPending(&radians));
  ASSERT_FLOAT_EQ(radians, -10.0);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
