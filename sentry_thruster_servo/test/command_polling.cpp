#include "sentry_thruster_servo_impl.h"
#include "util.h"

#include <gtest/gtest.h>
#include <math.h>

using namespace sentry_thruster_servo;

class PollingLoop : public SentryThrusterServoImpl, public ::testing::Test
{
protected:
  void SetUp() override
  {
    setAddress("S5");
  }

  void sendString(const std::string& s) override
  {
    SentryThrusterServoImpl::sendString(s);
    m_last_sent = s;
  }

  void setIndexed()
  {
    const auto msg = std::string{ "#S5?L0000\r\n:\x03" };
    handleResponse(msg, ros::Time::ZERO);
  }

  void setServoSpeed(std::uint8_t speed)
  {
    auto os = std::ostringstream{};
    os << "#S5?M77 1\r\r\n0077 " << std::hex << std::uppercase << std::setw(2) << std::setfill('0')
       << static_cast<int>(speed) << "\r\n"
       << (indexed() ? ":" : "*") << "\x03";
    const auto msg = os.str();
    handleResponse(msg, ros::Time::ZERO);
  }

  std::string m_last_sent;
};

TEST_F(PollingLoop, when_disabled)
{
  ASSERT_TRUE(m_last_sent.empty());

  const auto location = M_PI / 180.0 * 45;
  setEnabled(false);
  setLocation(location);

  for (auto index = 0; index < static_cast<int>(PollCommands::N_POLL_COMMANDS); ++index)
  {
    sendNextPeriodicCommand();
    // We shouldn't send a set-location command when disabled
    if (index == static_cast<int>(PollCommands::SET_LOCATION))
    {
      continue;
    }
    const auto expected = queryCommand(address(), static_cast<PollCommands>(index));
    ASSERT_STREQ(expected.c_str(), m_last_sent.c_str()) << "Test failed with PollCommands index: " << index;
  }
}

TEST_F(PollingLoop, when_enabled_but_no_location)
{
  ASSERT_TRUE(m_last_sent.empty());

  setEnabled(true);

  for (auto index = 0; index < static_cast<int>(PollCommands::N_POLL_COMMANDS); ++index)
  {
    sendNextPeriodicCommand();
    // We shouldn't send a set-location command when we haven't provided a location setpoint yet
    if (index == static_cast<int>(PollCommands::SET_LOCATION))
    {
      continue;
    }
    const auto expected = queryCommand(address(), static_cast<PollCommands>(index));
    ASSERT_STREQ(expected.c_str(), m_last_sent.c_str()) << "Test failed with PollCommands index: " << index;
  }
}

TEST_F(PollingLoop, when_enabled_with_location_but_not_indexed)
{
  ASSERT_TRUE(m_last_sent.empty());

  setEnabled(true);
  const float location = M_PI / 180.0 * 45.0;
  setLocation(location);

  for (auto index = 0; index < static_cast<int>(PollCommands::N_POLL_COMMANDS); ++index)
  {
    sendNextPeriodicCommand();
    // We shouldn't send a set-location command when we haven't provided a location setpoint yet
    if (index == static_cast<int>(PollCommands::SET_LOCATION))
    {
      continue;
    }
    const auto expected = queryCommand(address(), static_cast<PollCommands>(index));
    ASSERT_STREQ(expected.c_str(), m_last_sent.c_str()) << "Test failed with PollCommands index: " << index;
  }
}

TEST_F(PollingLoop, enabled_with_location_and_indexed)
{
  ASSERT_TRUE(m_last_sent.empty());

  setEnabled(true);
  setServoSpeed(speed());
  ASSERT_EQ(state().speed_raw, speed());
  setIndexed();
  const float location = M_PI / 180.0 * 45.0;
  setLocation(location);

  for (auto index = 0; index < static_cast<int>(PollCommands::N_POLL_COMMANDS); ++index)
  {
    sendNextPeriodicCommand();
    // We shouldn't send a set-location command when we haven't provided a location setpoint yet
    const auto expected = [&] {
      if (index == static_cast<int>(PollCommands::SET_LOCATION))
      {
        return setLocationCommand(address(), location);
      }
      else
      {
        return queryCommand(address(), static_cast<PollCommands>(index));
      }
    }();
    ASSERT_STREQ(expected.c_str(), m_last_sent.c_str()) << "Test failed with PollCommands index: " << index;
  }
}

TEST_F(PollingLoop, set_speed_if_needed)
{
  ASSERT_TRUE(m_last_sent.empty());

  // Enabled and indexed, but with a speed NOT what we want.
  setEnabled(true);
  const auto desired_speed = std::uint8_t{ 0x90 };
  setSpeed(desired_speed);
  ASSERT_NE(state().speed_raw, desired_speed);
  setIndexed();
  const float location = M_PI / 180.0 * 45.0;
  setLocation(location);

  for (auto index = 0; index < static_cast<int>(PollCommands::N_POLL_COMMANDS); ++index)
  {
    sendNextPeriodicCommand();
    const auto expected = [&] {
      if (index == static_cast<int>(PollCommands::SET_LOCATION))
      {
        return setSpeedAndLocationCommand(address(), desired_speed, location);
      }
      else
      {
        return queryCommand(address(), static_cast<PollCommands>(index));
      }
    }();
    ASSERT_STREQ(expected.c_str(), m_last_sent.c_str()) << "Test failed with PollCommands index: " << index;
  }
}

TEST_F(PollingLoop, sending_location_only_once)
{
  ASSERT_TRUE(m_last_sent.empty());

  setEnabled(true);
  setServoSpeed(speed());
  setIndexed();
  ASSERT_TRUE(state().index_state == ::sentry_thruster_servo::State::INDEX_STATE_INDEXED);

  const float location = M_PI / 180.0 * 45.0;
  setLocation(location);

  for (auto index = 0; index < static_cast<int>(PollCommands::N_POLL_COMMANDS); ++index)
  {
    sendNextPeriodicCommand();
    // We shouldn't send a set-location command when we haven't provided a location setpoint yet
    const auto expected = [&] {
      if (index == static_cast<int>(PollCommands::SET_LOCATION))
      {
        return setLocationCommand(address(), location);
      }
      else
      {
        return queryCommand(address(), static_cast<PollCommands>(index));
      }
    }();
    ASSERT_STREQ(expected.c_str(), m_last_sent.c_str()) << "Test failed with PollCommands index: " << index;
  }

  for (auto index = 0; index < static_cast<int>(PollCommands::N_POLL_COMMANDS); ++index)
  {
    sendNextPeriodicCommand();
    // We shouldn't send a set-location command because we haven't provided a new position since the previous loop.
    if (index == static_cast<int>(PollCommands::SET_LOCATION))
    {
      continue;
    }
    const auto expected = queryCommand(address(), static_cast<PollCommands>(index));
    ASSERT_STREQ(expected.c_str(), m_last_sent.c_str()) << "Test failed with PollCommands index: " << index;
  }
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
