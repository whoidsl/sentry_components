#include <sentry_thruster_servo/State.h>
#include "util.h"

#include <gtest/gtest.h>
#include <math.h>

using namespace sentry_thruster_servo;

TEST(parsing, location_empty)
{
  const auto message = std::string{ "#S1?L0000\r\n*\x03" };
  auto state = ::sentry_thruster_servo::State{};
  state.angle_radians = 0.5;
  state.angle_raw = 0xFFFF;
  state.index_state = State::INDEX_STATE_UNKNOWN;

  const auto expected_radians = 0.0f;
  const std::int16_t expected_raw = 0x0000;
  const auto expected_index_state = State::INDEX_STATE_UNINDEXED;

  ASSERT_TRUE(updateState(message, "S1", state, ros::Time::ZERO));
  EXPECT_FLOAT_EQ(expected_radians, state.angle_radians);
  EXPECT_EQ(expected_raw, state.angle_raw);
  EXPECT_EQ(expected_index_state, state.index_state);
}

TEST(parsing, location_positive)
{
  const auto message = std::string{ "#S1?L000A\r\n:\x03" };
  auto state = ::sentry_thruster_servo::State{};
  state.angle_radians = 0;
  state.angle_raw = 0x0000;
  state.index_state = State::INDEX_STATE_UNKNOWN;

  const auto expected_radians = static_cast<int16_t>(0xA) / ::sentry_thruster_servo::COUNTS_PER_RADIAN;
  const std::int16_t expected_raw = 0x000A;
  const auto expected_index_state = State::INDEX_STATE_INDEXED;

  ASSERT_TRUE(updateState(message, "S1", state, ros::Time::ZERO));
  EXPECT_FLOAT_EQ(expected_radians, state.angle_radians);
  EXPECT_EQ(expected_raw, state.angle_raw);
  EXPECT_EQ(expected_index_state, state.index_state);
}

TEST(parsing, set_location_positive)
{
  const auto message = std::string{ "#S1!L000A\r\r\n:\x03" };
  auto state = ::sentry_thruster_servo::State{};
  state.set_angle_radians = 0;
  state.set_angle_raw = 0x0000;
  state.index_state = State::INDEX_STATE_UNKNOWN;

  const auto expected_radians = static_cast<std::int16_t>(0x0A) / ::sentry_thruster_servo::COUNTS_PER_RADIAN;
  const std::int16_t expected_raw = 0x000A;
  const auto expected_index_state = State::INDEX_STATE_INDEXED;

  ASSERT_TRUE(updateState(message, "S1", state, ros::Time::ZERO));
  EXPECT_FLOAT_EQ(expected_radians, state.set_angle_radians);
  EXPECT_EQ(expected_raw, state.set_angle_raw);
  EXPECT_EQ(expected_index_state, state.index_state);
}

TEST(parsing, location_negative)
{
  const auto message = std::string{ "#S1?LFFF4\r\nc\x03" };
  auto state = ::sentry_thruster_servo::State{};
  state.angle_radians = 0;
  state.angle_raw = 0x0000;
  state.index_state = State::INDEX_STATE_UNKNOWN;

  const auto expected_radians = static_cast<std::int16_t>(0xFFF4) / ::sentry_thruster_servo::COUNTS_PER_RADIAN;
  const std::int16_t expected_raw = 0xFFF4;
  const auto expected_index_state = State::INDEX_STATE_INDEXING;

  ASSERT_TRUE(updateState(message, "S1", state, ros::Time::ZERO));
  EXPECT_FLOAT_EQ(expected_radians, state.angle_radians);
  EXPECT_EQ(expected_raw, state.angle_raw);
  EXPECT_EQ(expected_index_state, state.index_state);
}

TEST(parsing, voltages)
{
  const auto message = std::string{ "#S1?V FF DD 00 05\r\n*\x03" };
  auto state = ::sentry_thruster_servo::State{};
  state.index_state = State::INDEX_STATE_UNINDEXED;

  const auto expected_raw_voltages = std::array<std::uint8_t, 4>{ 0xFF, 0xDD, 0x00, 0x05 };
  const auto expected_thermistor = thermistor_kohms(
      static_cast<float>(expected_raw_voltages[::sentry_thruster_servo::State::RAW_VOLTAGE_INDEX_TEMPERATURE]));
  const auto expected_bus_voltage =
      static_cast<float>(expected_raw_voltages[::sentry_thruster_servo::State::RAW_VOLTAGE_INDEX_BUS_VOLTAGE]) *
      BUS_VOLTAGE_DIVIDER_RATIO;
  const auto expected_bias_voltage =
      static_cast<float>(expected_raw_voltages[::sentry_thruster_servo::State::RAW_VOLTAGE_INDEX_12_5V_VOLTAGE]) *
      BIAS_VOLTAGE_DIVIDER_RATIO;
  const auto expected_osc_voltage =
      static_cast<float>(expected_raw_voltages[::sentry_thruster_servo::State::RAW_VOLTAGE_INDEX_6_5V_VOLTAGE]) *
      OSC_VOLTAGE_DIVIDER_RATIO;

  ASSERT_TRUE(updateState(message, "S1", state, ros::Time::ZERO));
  EXPECT_EQ(state.update, ::sentry_thruster_servo::State::UPDATE_VOLTAGES);
  EXPECT_TRUE(
      std::equal(std::begin(expected_raw_voltages), std::end(expected_raw_voltages), std::begin(state.raw_voltages)));

  EXPECT_FLOAT_EQ(state.temperature_kohms, expected_thermistor);
  EXPECT_FLOAT_EQ(state.bus_voltage_v, expected_bus_voltage);
  EXPECT_FLOAT_EQ(state.bias_voltage_v, expected_bias_voltage);
  EXPECT_FLOAT_EQ(state.osc_voltage_v, expected_osc_voltage);
}

TEST(parsing, no_etx)
{
  const auto message = std::string{ "#S1!L0000\r\n*" };
  auto state = ::sentry_thruster_servo::State{};

  ASSERT_FALSE(updateState(message, "S1", state, ros::Time::ZERO));
}

TEST(parsing, missing_newline_ok)
{
  const auto message = std::string{ "#S1?L0000\r*\x03" };
  auto state = ::sentry_thruster_servo::State{};
  ASSERT_FALSE(updateState(message, "S1", state, ros::Time::ZERO));
}

TEST(parsing, missing_cr)
{
  const auto message = std::string{ "#S1?L0000\n*\x03" };
  auto state = ::sentry_thruster_servo::State{};
  ASSERT_FALSE(updateState(message, "S1", state, ros::Time::ZERO));
}

TEST(parsing, address_mismatch)
{
  const auto message = std::string{ "#S1?L0000\n*\x03" };
  auto state = ::sentry_thruster_servo::State{};
  ASSERT_FALSE(updateState(message, "S5", state, ros::Time::ZERO));
}

TEST(parsing, long_command)
{
  const auto message = std::string{ "#S2?L#S2?I#S2?V#S2?M0 1\r#S2?M77 1\r#S1?L0000\r\n:\x03" };
  auto state = ::sentry_thruster_servo::State{};
  state.angle_radians = 0.5;
  state.angle_raw = 0xFFFF;
  state.index_state = State::INDEX_STATE_UNKNOWN;

  const auto expected_radians = 0.0f;
  const std::int16_t expected_raw = 0x0000;
  const auto expected_index_state = State::INDEX_STATE_INDEXED;

  ASSERT_TRUE(updateState(message, "S1", state, ros::Time::ZERO));
  EXPECT_FLOAT_EQ(expected_radians, state.angle_radians);
  EXPECT_EQ(expected_raw, state.angle_raw);
  EXPECT_EQ(expected_index_state, state.index_state);
}

TEST(parsing, speed)
{
  const auto message = std::string{ "#S2?M77 1\r\r\n0077 90\r\n:\x03" };
  auto state = ::sentry_thruster_servo::State{};
  state.speed_raw = 0x00;

  const auto expected_speed = std::uint8_t{ 0x90 };

  ASSERT_TRUE(updateState(message, "S2", state, ros::Time::ZERO));
  EXPECT_EQ(expected_speed, state.speed_raw);
}
#if 0

TEST_F(sail_servo_test, reject_location_cmd) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1!L0000\r\n*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}


TEST_F(sail_servo_test, reject_prompt) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L0000\r\n\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}


TEST_F(sail_servo_test, reject_payload) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L\r\n*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, reject_junk) {

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?LGM!\r\n*\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}

TEST_F(sail_servo_test, fuzz) {

  ds_core_msgs::RawData bytes = buildRawMsg("DFHGHUIOHA\x03");
  EXPECT_TRUE(!underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadRaw());
  EXPECT_EQ(NODATA, underTest.locationRadCorrected());
  EXPECT_TRUE(!underTest.locationGood());
}


// These tests fail because SailServo actually tries to publish something,
// but the test fixture's constructor didn't set up the publishers, which
// results in a crash that prevents rostest from even tallying the
// successes/failures.
/**
TEST_F(sail_servo_test, good_index) {

  EXPECT_TRUE(!underTest.locationGood());

  ds_core_msgs::RawData bytes = buildRawMsg("#S1?L0000\r\n:\x03");
  EXPECT_TRUE(underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(0, underTest.locationRadRaw());
  EXPECT_EQ((-S_OFFSET * S_SCALE), underTest.locationRadCorrected());
  EXPECT_TRUE(underTest.locationGood());
}

TEST_F(sail_servo_test, long_command) {

  EXPECT_TRUE(!underTest.locationGood());

  ds_core_msgs::RawData bytes = buildRawMsg("#S2?L#S2?I#S2?V#S2?M0 1\r#S2?M77 1\r#S1?L0000\r\n:\x03");
  EXPECT_TRUE(underTest._parseReceivedInner(bytes));
  EXPECT_EQ(NODATA, underTest.indexRadRaw());
  EXPECT_EQ(0, underTest.locationRadRaw());
  EXPECT_EQ((-S_OFFSET * S_SCALE), underTest.locationRadCorrected());
  EXPECT_TRUE(underTest.locationGood());
}
*/
#endif

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
