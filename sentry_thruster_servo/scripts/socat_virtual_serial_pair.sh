#!/usr/bin/env bash

# Usage:  socat_virtual_serial_pair.sh PORT0 PORT1
#
# PORT0:  path to master end of virtual serial pair (default: /tmp/ttyUSB0)
# PORT1:  path to slave end of virtual serial pair (default: /tmp/ttyUSB1)
socat -dd PTY,link=${1:-/tmp/ttyUSB0},echo=1 PTY,link=${2:-/tmp/ttyUSB1},echo=1
