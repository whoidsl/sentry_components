/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_blueview/sentry_blueview.h"

namespace sentry_blueview
{
SentryBlueview::~SentryBlueview() = default;

SentryBlueview::SentryBlueview(int argc, char* argv[], const std::string& name) : DsProcess(argc, argv, name)
{
}

void SentryBlueview::onBlueviewData(ds_core_msgs::RawData bytes)
{
  std::string blvData(bytes.data.begin(), bytes.data.end());

  if (!blvData.find("ROS_AVOID"))
  {
    ds_control_msgs::ExternalBottomFollowAlarm out;
    out.header.stamp = ros::Time::now();
    out.alarm = true;
    out.delta_down = -50.0;
    out.speed_override = 0.05;
    alarm_pub_.publish(out);
  }
  if (!blvData.find("ROS_RESUME"))
  {
    ds_control_msgs::ExternalBottomFollowAlarm out;
    out.header.stamp = ros::Time::now();
    out.alarm = false;
    alarm_pub_.publish(out);
  }
}

void SentryBlueview::onBlueviewStatusData(ds_core_msgs::RawData bytes)
{
  std::string blvData(bytes.data.begin(), bytes.data.end());

  ds_sensor_msgs::ForwardLookingStatus out;
  out.header.stamp = ros::Time::now();

  double time;
  int return_code;
  int status;
  int enable_oa;
  int avoiding;
  status = sscanf(blvData.c_str(), "%lf SONAR_BAD %d", &time, &return_code);
  if (status == 2)
  {
    out.sonar_ok = false;
    out.return_code = return_code;
    status_pub_.publish(out);
    return;
  }
  status = sscanf(blvData.c_str(), "%lf SONAR_OK %d %d %d", &time, &return_code, &enable_oa, &avoiding);
  if (status == 4)
  {
    out.sonar_ok = true;
    out.return_code = return_code;
    out.oa_enabled = enable_oa;
    out.actively_avoiding = avoiding;
    status_pub_.publish(out);
    return;
  }
}

void SentryBlueview::onJtRosMsg(const std_msgs::String::ConstPtr msg)
{
  std::string data = msg->data;
  std::string out;

  const auto equals_idx = data.find_first_of(' ');
  if (std::string::npos != equals_idx)
  {
    out = data.substr(equals_idx + 1);
    ROS_ERROR_STREAM(out);
    blueview_conn_->send(out);
  }
  else
  {
    ROS_ERROR_STREAM(out);
  }
}

void SentryBlueview::setupConnections()
{
  ds_base::DsProcess::setupConnections();

  blueview_conn_ = addConnection("blueview_connection", boost::bind(&SentryBlueview::onBlueviewData, this, _1));

  blueview_status_conn_ =
      addConnection("blueview_status_connection", boost::bind(&SentryBlueview::onBlueviewStatusData, this, _1));
}

void SentryBlueview::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();

  auto nh = nodeHandle();
  std::string alarm_topic = ros::param::param<std::string>("~alarm_topic", "/alarm");
  alarm_pub_ = nodeHandle().advertise<ds_control_msgs::ExternalBottomFollowAlarm>(alarm_topic, 1);

  std::string status_topic = ros::param::param<std::string>("~sonar_status_topic", "/sonar_status");
  status_pub_ = nodeHandle().advertise<ds_sensor_msgs::ForwardLookingStatus>(status_topic, 1);
}

void SentryBlueview::setupSubscriptions()
{
  ds_base::DsProcess::setupSubscriptions();

  std::string jtros_topic = ros::param::param<std::string>("~jtros_topic", "/blueview/jtros");
  jtros_sub_ =
      nodeHandle().subscribe<std_msgs::String>(jtros_topic, 1, boost::bind(&SentryBlueview::onJtRosMsg, this, _1));
}
}
