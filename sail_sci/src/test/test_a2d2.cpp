/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/27/18.
//

#include "../../include/sail_sci/a2d2.h"

#include <gtest/gtest.h>

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class A2D2Test : public ::testing::Test
{
public:
  // This method runs ONCE before a text fixture is run (not once-per-test-case)
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

TEST_F(A2D2Test, AcceptsGood)
{
  std::string testStr("#AD3D +3.9175 +0.0000 +0.0000 +0.0000 \3");
  sail_sci::A2D2 this_A2D2;
  EXPECT_TRUE(this_A2D2.acceptsMessage(testStr));
}

TEST_F(A2D2Test, ParsesGood)
{
  std::string testStr("#AD0D 15");
  sail_sci::A2D2 this_A2D2;
  auto parse = this_A2D2.handleMessage("#AD3D +3.9175 +0.0000 +0.0000 +0.0000 \3");
  EXPECT_TRUE(parse);
  ROS_INFO_STREAM(this_A2D2.getRaw());
  EXPECT_FLOAT_EQ(3.9175, this_A2D2.getRaw());
}

TEST_F(A2D2Test, ParsesNeg)
{
  std::string testStr("#AD0D 15");
  sail_sci::A2D2 this_A2D2;
  auto parse = this_A2D2.handleMessage("#AD3D -3.9175 +0.0000 +0.0000 +0.0000 \3");
  EXPECT_TRUE(parse);
  ROS_INFO_STREAM(this_A2D2.getRaw());
  EXPECT_FLOAT_EQ(-3.9175, this_A2D2.getRaw());
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}