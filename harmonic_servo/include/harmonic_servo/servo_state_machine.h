/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by llindzey on 7/2/19.
//

// separate header for state-machine definition;
// all ROS interfaces handled in servo_driver.h

#ifndef SENTRY_COMPONENTS_HARMONIC_SERVO_STATE_MACHINE_H
#define SENTRY_COMPONENTS_HARMONIC_SERVO_STATE_MACHINE_H

#include <ros/console.h>  // Only for rosconsole logging
#include "ds_asio/ds_asio.h"  // for iosm_ (Maybe it should just be forward-declared as a class?)
#include "ds_core_msgs/RawData.h"  // for iosm interaction

#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>

#include <typeinfo>  // for typeid(foo).name()

#include "harmonic_servo/servo_commands.h"

namespace msmf = boost::msm::front;  // Cleans up the transition table.

class HarmonicServo; // defined in servo_driver.h

namespace harmonic_servo {

  // State names for the outer state machine, used for debug output.
  static char const* const state_names[] = {"Entry", "NoDevice", "Braked",
					    "Nominal", "Coasting",
					    "Configuring"};

  ///////////////////////////////////////////
  // Events
  ///////////////////////////////////////////
  /*
  struct NoCommand {
    NoCommand() {};
  };
  */
  struct DeviceDetected {
    DeviceDetected() {};
  };

  struct NoResponse {
    NoResponse() {}
  };

  struct Enable {
    Enable() {}
  };

  struct Brake {
    Brake() {}
  };

  struct StartCoast {
    StartCoast() {}
  };

  struct StopCoast {
    StopCoast() {}
  };

  struct UpdateCommand {
    UpdateCommand(int cc) : counts(cc) {}
    int counts;
  };

  struct Configure {
  Configure(std::string cc, std::string vv) : code(cc), value(vv) {}
    std::string code;
    std::string value;
  };

  struct ConfiguringSucceeded {
    ConfiguringSucceeded() {}
  };

  struct ConfiguringFailed {
    ConfiguringFailed() {}
  };

  struct StartIndex {
	  StartIndex() {}
  };

  struct DoneIndexing {
    DoneIndexing() {}
  };

  struct AtNegativeStop {
    AtNegativeStop(int cc) : counts(cc) {}
    int counts;
  };

  struct GotCurrentOffset {
    GotCurrentOffset(int cc, int oo) : counts(cc), offset(oo) {}
    int counts;
    int offset;
  };

  struct EncoderValid {
    EncoderValid(int cc) : counts(cc) {}
    int counts;
  };

  struct AtPositiveStop {
    AtPositiveStop(int cc) : counts(cc) {}
    int counts;
  };



  ///////////////////////////////////////////
  // State machine definition
  ///////////////////////////////////////////

  struct ServoFront : public msmf::state_machine_def<ServoFront> {
    // The state machine needs access to the Servo in order to manage the IoSM,
    // some of whose callbacks need to belong to the DsProcess.
    // (e.g. anything parsing/publishing the data)
    // Using a bare pointer here allows the HarmonicServo class and the boost::msm
    // struct that it uses to manage state to have references to each other,
    // while respecting the expected order of destruction.
    HarmonicServo* node_;
    ds_asio::IoSM* iosm_;
	int trajectory_status_id_;

    std::map<std::string, int> status_command_ids_;

    ServoFront(HarmonicServo* node, ds_asio::IoSM* iosm)
      : node_(node), iosm_(iosm) {}

    template <class Event, class Fsm> void on_entry(Event const&, Fsm&) {
      ROS_INFO_STREAM("entering: Servo State machine!");
    }

    template <class Event, class Fsm> void on_exit(Event const&, Fsm&) {
      ROS_INFO_STREAM("leaving: Servo State Machine");
    }

    ///////////////////////////////////////////
    // States (defined within the machine)
    ///////////////////////////////////////////

    /// Initial state in Servo State machine.
    //
    // Populates the iosm with the standard status queries, since those should
    // be running in any state.
    struct Entry : public msmf::state<> {
      template <class Event, class Fsm> void on_entry(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("Entry.on_entry");

	typedef bool (harmonic_servo::HarmonicServo::*CallbackFP)(ds_core_msgs::RawData);
	std::map<std::string, CallbackFP> status_loop_commands = {
          {read_voltage_cmd, &HarmonicServo::inputVoltageCallback},
          {read_current_cmd, &HarmonicServo::motorCurrentCallback},
          {read_halls_cmd, &HarmonicServo::hallStateCallback},
          {read_encoder_status_cmd, &HarmonicServo::encoderStatusCallback},
          {read_trajectory_status_cmd, &HarmonicServo::trajectoryStatusCallback},
          {read_fault_status_cmd, &HarmonicServo::faultStatusCallback},
          {read_event_status_cmd, &HarmonicServo::eventStatusCallback},
	  // Velocity  should always 2nd to last
          {read_velocity_cmd, &HarmonicServo::velocityCallback},
	  // Position should always be last query before commands to minimize
	  // how stale the current position is when updating goal position.
          {read_position_cmd, &HarmonicServo::positionCallback}
	};

	for(const auto& pair : status_loop_commands) {
	  auto command = pair.first;
	  auto callback = boost::bind(pair.second, fsm.node_, _1);
	  float timeout = 0.1;
	  bool force_next = false;
	  auto cmd = ds_asio::IoCommand(command, timeout, force_next, callback);
	  fsm.status_command_ids_[command] = fsm.iosm_->addRegularCommand(cmd);
	}
      }

      template <class Event, class Fsm> void on_exit(Event const&, Fsm&) {
	ROS_INFO_STREAM("Entry.on_exit");
      }
    };

    // State before communications have been confirmed with the servo.
    //
    // This state queries the serial number; once it has been received,
    // pass it to the driver and exit state.
    struct NoDevice : public msmf::state<> {
      int cmd_id_;  // Command id for the serial number command

      template <class Event, class Fsm> void on_entry(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("NoDevice.on_entry");
	fsm.node_->resetSerialNumberCount();

	float timeout = 0.1;
	bool force_next = false;
	auto cb = boost::bind(&HarmonicServo::serialNumberCallback, fsm.node_, _1);
	auto iocmd = ds_asio::IoCommand(read_serial_number_cmd, timeout, force_next, cb);
	cmd_id_ = fsm.iosm_->addRegularCommand(iocmd);
      }

      template <class Event, class Fsm> void on_exit(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("NoDevice.on_exit");
	fsm.iosm_->deleteRegularCommand(cmd_id_);
	// This sets all of the "runtime" config calls, that are either
	// controlled by parameters (rather than the configuration server),
	// or that might have been changed during operation.
	std::list<std::pair<std::string, int>> config = {
	  // * Peak/continuous current depends on chain tension, so is
	  //   a launch param. It is also changed during zeroing.
	  // * Encoder offset is per-servo, so needs to be set by the driver.
	  //   (The config service call is meant to be generic to the hardware.)
	  {"0x22", fsm.node_->config_.continuous_current_limit},
	  {"0x21", fsm.node_->config_.peak_current_limit}
	};
	float timeout = 0.1;
	bool force_next = false;
	for(const auto pair : config) {
	  std::string cmd = getSetRamCmd(pair.first, std::to_string(pair.second));
	  auto cb = boost::bind(&HarmonicServo::commandCallback, fsm.node_, cmd, _1);
	  auto iocmd = ds_asio::IoCommand(cmd, timeout, force_next, cb);
	  fsm.iosm_->addPreemptCommand(iocmd);
	}
      }
    };  // NoDevice

    // State where servo is powered up but not responding to commands.
    // Corresponds to enable=False in /sentry/abort messages.
    struct Braked : public msmf::state<> {
      template <class Event, class Fsm> void on_entry(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("Braked.on_entry");
	float timeout = 0.1;
	bool force_next = false;
	auto cb1 = boost::bind(&HarmonicServo::commandCallback, fsm.node_,
			       set_trajectory_mode_cmd, _1);
	auto iocmd1 = ds_asio::IoCommand(set_trajectory_mode_cmd, timeout,
					 force_next, cb1);
	fsm.iosm_->addPreemptCommand(iocmd1);
	auto cb2 = boost::bind(&HarmonicServo::commandCallback, fsm.node_,
			       cancel_trajectory_cmd, _1);
	auto iocmd2 = ds_asio::IoCommand(cancel_trajectory_cmd, timeout,
					 force_next, cb2);
	fsm.iosm_->addPreemptCommand(iocmd2);
      }

      template <class Event, class Fsm> void on_exit(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("Braked.on_exit");
      }
    };  // Braked

    struct Nominal : public msmf::state<> {
      template <class Event, class Fsm> void on_entry(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("Nominal.on_entry");
	// Reset goal and commanded counts so the receding-horizon check doesn't
	// send a command corresponding to a previously-unfinished goal.
	fsm.node_->resetGoal();
	fsm.node_->publishResetGoal(false);
	

	// This doesn't need to set up anything in the iosm because the only
	// entry point is from Braked, so mode is already set to trajectory
      }

      template <class Event, class Fsm> void on_exit(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("Nominal.on_exit");
	// Cancel any goal that might be running.
	float timeout = 0.1;
	bool force_next = false;
	auto cb = boost::bind(&HarmonicServo::commandCallback, fsm.node_, cancel_trajectory_cmd, _1);
	fsm.iosm_->addPreemptCommand(ds_asio::IoCommand(cancel_trajectory_cmd, timeout, force_next, cb));
      }
    };  // Nominal

    struct Coasting : public msmf::state<> {
      template <class Event, class Fsm> void on_entry(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("Coasting.on_entry");
	float timeout = 0.1;
	bool force_next = false;
	auto cb1 = boost::bind(&HarmonicServo::commandCallback, fsm.node_, set_coast_mode_cmd, _1);
	fsm.iosm_->addPreemptCommand(ds_asio::IoCommand(set_coast_mode_cmd, timeout, force_next, cb1));
      }

      template <class Event, class Fsm> void on_exit(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("Coasting.on_exit");
      }
    };  // Coasting

    struct Configuring : public msmf::state<> {
      template <class Event, class Fsm> void on_entry(Event const& event, Fsm& fsm) {
	ROS_INFO_STREAM("Configuring.on_entry");
	std::string cmd = getSetFlashCmd(event.code, event.value);
	// Some of the configuration commands are long enough that a 0.1 second
	// timeout is insufficient. (In particular, the filter coefficients.)
	float timeout = 0.2;
	bool force_next = false;
	auto cb = boost::bind(&HarmonicServo::configCallback, fsm.node_, event.code, event.value, _1);
	fsm.iosm_->addPreemptCommand(ds_asio::IoCommand(cmd, timeout, force_next, cb));
      }

      template <class Event, class Fsm> void on_exit(Event const&, Fsm& fsm) {
	ROS_INFO_STREAM("Configuring.on_exit");
	// Since we repeatedly enter/exit this state, the actual exit behavior
	// of resetting the servo is handled in the action associated with the
	// ConfiguringSucceeded event.

      }
    };  // Configuring

    /////////////////////////////////////////////////////
    // Define Actions
    /////////////////////////////////////////////////////


    // All of the actions are expected to log a state transition,
    // but some  may be defined to also perform additional checks and/or
    // carry out additional actions.

    // Default action; only log
    // TODO: This should probably publish a custom message for monitoring
    //       servo driver state
    struct LogAction {
      template<class Event, class Fsm, class Source, class Target>
      void operator()(Event const& event, Fsm& fsm, Source& source, Target& target) const {
	ROS_INFO_STREAM("Action: (" << typeid(Event).name() << ") "
			<< typeid(source).name() << " -> " << typeid(target).name());
      }
    };

    struct ConfiguringSucceededAction {
      template<class Event, class Fsm, class Source, class Target>
      void operator()(Event const& event, Fsm& fsm, Source& source, Target& target) const {
	// Output the standard logging information
	LogAction la;
	la(event, fsm, source, target);
	fsm.node_->publishConfigureResult(true);
	// Send the command to reset the servo.
	// (Used to repopulate the RAM with saved flash configuration)
	float timeout = 0.1;
	// This command is expected to time out (the motor controller doesn't
	// respond), so no need to provide a callback.
	fsm.iosm_->addPreemptCommand(ds_asio::IoCommand(reset_cmd, timeout));
      }
    };

    // Could have failed, or been interrupted.
    struct ConfiguringFailedAction {
      template<class Event, class Fsm, class Source, class Target>
      void operator()(Event const& event, Fsm& fsm, Source& source, Target& target) const {
	// Output the standard logging information
	LogAction la;
	la(event, fsm, source, target);
	fsm.node_->publishConfigureResult(false);
      }
    };


    // Update the command that has been sent to the motor controller
    // (This is used during self-transitions in the Nominal state, since
    // updated goals don't trigger a state change.)
    struct UpdateCommandAction {
      template<class Event, class Fsm, class Source, class Target>
	void operator()(Event const& event, Fsm& fsm, Source& source, Target& target) const {
	// Output the standard logging information
	LogAction la;
	la(event, fsm, source, target);
	// Update goal (Corresponding action will only happen if we're in Nominal state)
	std::string cmd = getTrajectoryGoalCmd(event.counts);
	float timeout = 0.1;
	bool force_next = false;
	// In the case of commands arriving faster than they can be forwarded to the controller
	// at 9600 baud, don't add any new ones.
	auto preempt_queue_size = fsm.iosm_->getPreemptQueueSize();
	if (preempt_queue_size < 2) {
	  auto cb1 = boost::bind(&HarmonicServo::commandCallback, fsm.node_, cmd, _1);
	  auto cb2 = boost::bind(&HarmonicServo::commandCallback, fsm.node_, start_trajectory_cmd, _1);
	  fsm.iosm_->addPreemptCommand(ds_asio::IoCommand(cmd, timeout, force_next, cb1));
	  fsm.iosm_->addPreemptCommand(ds_asio::IoCommand(start_trajectory_cmd, timeout, force_next, cb2));
	} else {
	  ROS_ERROR_STREAM("Commands arriving too fast; preempt queue length = " << preempt_queue_size
			   << ". Skipping goal of " << event.counts << " counts");
	}

      }
    };  // UpdateCommandAction

    /////////////////////////////////////////////////////
    // Define Transitions
    /////////////////////////////////////////////////////

    /* 
      Format for updating this table is as follows:
      state you are currently in, Action taken, State transitioned to, log or update, msmf::none
    */
   
    typedef Entry initial_state;

    struct transition_table : boost::mpl::vector<
      // State 0: Entry
      msmf::Row<Entry, msmf::none, NoDevice, LogAction, msmf::none>,

      // State 1: No Device
      msmf::Row<NoDevice, DeviceDetected, Braked, LogAction, msmf::none>,
      // This seems extraneous, but we want to reset the serial number count.
      msmf::Row<NoDevice, NoResponse, NoDevice, LogAction, msmf::none>,
      msmf::Row<NoDevice, UpdateCommand, msmf::none, LogAction, msmf::none>,
      msmf::Row<NoDevice, Enable, msmf::none, LogAction, msmf::none>,
      msmf::Row<NoDevice, StopCoast, msmf::none, LogAction, msmf::none>,
      msmf::Row<NoDevice, StartCoast, msmf::none, LogAction, msmf::none>,
      msmf::Row<NoDevice, Brake, msmf::none, LogAction, msmf::none>,

      // State 2: Braked
      msmf::Row<Braked, NoResponse, NoDevice, LogAction, msmf::none>,
      msmf::Row<Braked, Enable, Nominal, LogAction, msmf::none>,
      msmf::Row<Braked, StartCoast, Coasting, LogAction, msmf::none>,
      msmf::Row<Braked, Configure, Configuring, LogAction, msmf::none>,
      msmf::Row<Braked, StopCoast, Braked, LogAction, msmf::none>,
      msmf::Row<Braked, UpdateCommand, msmf::none, LogAction, msmf::none>,

      // State 3: Nominal
      msmf::Row<Nominal, NoResponse, NoDevice, LogAction, msmf::none>,
      msmf::Row<Nominal, Brake, Braked, LogAction, msmf::none>,
      msmf::Row<Nominal, StartCoast, Coasting, LogAction, msmf::none>,
      msmf::Row<Nominal, UpdateCommand, msmf::none, UpdateCommandAction, msmf::none>,
      msmf::Row<Nominal, Configure, Configuring, LogAction, msmf::none>,
      msmf::Row<Nominal, Enable, Nominal, LogAction, msmf::none>,
      msmf::Row<Nominal, StopCoast, Nominal, LogAction, msmf::none>,

      // State 4: Coasting
      msmf::Row<Coasting, NoResponse, NoDevice, LogAction, msmf::none>,
      msmf::Row<Coasting, StopCoast, Braked, LogAction, msmf::none>,
      msmf::Row<Coasting, Configure, Configuring, LogAction, msmf::none>,
      msmf::Row<Coasting, StartCoast, Coasting, LogAction, msmf::none>,
      msmf::Row<Coasting, Enable, Coasting, LogAction, msmf::none>,
      msmf::Row<Coasting, UpdateCommand, Coasting, LogAction, msmf::none>,
    
      // State 5: Configuring
      msmf::Row<Configuring, NoResponse, NoDevice, ConfiguringFailedAction, msmf::none>,
      // This doesn't currently work because the same event is used for internal
      // transitions within configuring and for transitioning to it in the first place.
      // So, this switches to coast and immediately back to configure, which
      // isn't quite what the user would expect. This is an unimportant transition,
      // so simply disabling it.
      //msmf::Row<Configuring, StartCoast, Coasting, ConfiguringFailedAction, msmf::none>,
      msmf::Row<Configuring, Configure, Configuring, LogAction, msmf::none>,
      msmf::Row<Configuring, ConfiguringSucceeded, NoDevice, ConfiguringSucceededAction, msmf::none>,
      msmf::Row<Configuring, ConfiguringFailed, NoDevice, ConfiguringFailedAction, msmf::none>
	  >{};

  
  };  // end ServoFront

  typedef boost::msm::back::state_machine<ServoFront> ServoBack;

} // namespace harmonic_servo




#endif  // SENTRY_COMPONENTS_HARMONIC_SERVO_STATE_MACHINE_H
