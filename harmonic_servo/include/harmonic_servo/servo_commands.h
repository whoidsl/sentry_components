/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on July 11, 2019.
//
// Constants for the subset of the servo commands the driver uses.

#ifndef SENTRY_COMPONENTS_HARMONIC_SERVO_COMMANDS_H
#define SENTRY_COMPONENTS_HARMONIC_SERVO_COMMANDS_H

namespace harmonic_servo {
  // Commands for querying ram register status
  const std::string read_voltage_cmd = "g r0x1e\r";
  const std::string read_current_cmd = "g r0x38\r";
  const std::string read_halls_cmd = "g r0xa2\r";
  const std::string read_encoder_status_cmd = "g r0x12f\r";
  const std::string read_trajectory_status_cmd = "g r0xc9\r";
  const std::string read_fault_status_cmd = "g r0xa4\r";
  const std::string read_event_status_cmd = "g r0xac\r";
  const std::string read_velocity_cmd = "g r0x18\r";
  const std::string read_position_cmd = "g r0x17\r";
  const std::string read_offset_cmd = "g r0xc6\r";

  // Commands for querying parameters stored in flash
  const std::string read_serial_number_cmd = "g f0x81\r";

  // Commands for setting controller mode
  const std::string set_trajectory_mode_cmd = "s r0x24 21\r";
  const std::string set_coast_mode_cmd = "s r0x24 0\r";

  // Commands for managing homing
  const std::string home_negative_cmd = "s r0xc2 2580\r"; //1044
  const std::string home_positive_cmd = "s r0xc2 2564\r"; //1028->2564

  // Command for setting current position to home
  const std::string home_immediate_cmd = "s r0xc2 2049\r";

  // Commands for managing trajectory mode
  const std::string cancel_trajectory_cmd = "t 0\r";
  const std::string start_trajectory_cmd = "t 1\r";
  const std::string start_homing_cmd = "t 2\r";

  // Command to reset the motor controller
  const std::string reset_cmd = "r\r";

  // Set ram/flash parameters in the controller.
  std::string getSetFlashCmd(std::string code, std::string value) {
    return "s f" + code + " " + value + "\r";
  }
  std::string getSetRamCmd(std::string code, std::string value) {
    return "s r" + code + " " + value + "\r";
  }

  // Set offset position for
  std::string setOffsetCmd(int counts) {
    return "s f0xc6 " + std::to_string(counts) + "\r";
  }

  // Set goal position for trajectory
  std::string getTrajectoryGoalCmd(int counts) {
    return "s r0xca " + std::to_string(counts) + "\r";
  }

  // Set state of digital output pins
  std::string getDigitalOutputCmd(int value) {
    return "s r0xab " + std::to_string(value) + "\r";
  }

}  // namespace harmonic_servo


#endif  // SENTRY_COMPONENTS_HARMONIC_SERVO_COMMANDS_H
