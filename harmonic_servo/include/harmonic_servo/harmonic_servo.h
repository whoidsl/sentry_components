/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on July 2, 2019.

#ifndef SENTRY_COMPONENTS_HARMONIC_SERVO_HARMONIC_SERVO_H
#define SENTRY_COMPONENTS_HARMONIC_SERVO_HARMONIC_SERVO_H

#include <ros/ros.h>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/state_machine_def.hpp>

#include "ds_base/ds_process.h"

#include "ds_actuator_msgs/ServoCmd.h"
#include "ds_actuator_msgs/ServoState.h"
#include "ds_core_msgs/Abort.h"
#include "ds_core_msgs/RawData.h"
#include "ds_core_msgs/KeyBool.h"
#include "sentry_msgs/CopleyEventStatus.h"
#include "sentry_msgs/CopleyFaultStatus.h"
#include "sentry_msgs/CopleyTrajectoryStatus.h"
#include "sentry_msgs/HallState.h"
#include "sentry_msgs/HarmonicServoState.h"
#include "sentry_msgs/ServoCoastCmd.h"
#include "sentry_msgs/ServoIndexResult.h"
#include "sentry_msgs/ServoConfigureCmd.h"
#include "sentry_msgs/ServoConfigureResult.h"
#include "sentry_msgs/ServoParameter.h"
#include "sentry_msgs/ServoHomeCmd.h"
#include "sentry_msgs/ServoIndexCmd.h"
#include "sensor_msgs/JointState.h"

namespace harmonic_servo {
  class ServoFront;
  typedef boost::msm::back::state_machine<ServoFront> ServoBack;
}

namespace harmonic_servo {

// Holds all the feedback that has been received from the servo
// These are in over-the-wire units (counts, counts/sec, 100mV, 0.01A, etc),
// NOT engineering units (volts, amps, etc.). That conversion happens just
// before the messages are published, since all class functions use counts.
struct ServoFeedback {
  int position_counts;
  ros::Time position_time;
  float position_radians;
  int velocity_counts; // counts per second
  float velocity_radians; // radians per second
  int input_voltage; // 100mV
  int motor_current; // 0.01A
  int hall_states; // lowest 3 bits specify define on/off
  u_int32_t encoder_status; // bitmapped status returned by encoder.
  bool encoder_data_valid;
  u_int16_t trajectory_status;
  u_int32_t fault_status;
  u_int32_t event_status;

  // Messages that are populated by data from a single callback
  sentry_msgs::HallState hall_msg;
  sentry_msgs::CopleyEventStatus event_status_msg;
  sentry_msgs::CopleyFaultStatus fault_status_msg;
  sentry_msgs::CopleyTrajectoryStatus trajectory_status_msg;

};


// Hold all the configuration parameters for the servo
struct ServoConfig {
  std::string joint_name;

  // number of counts before encoder wraps
  int encoder_counts;
  // Encoder value when fin is level.
  int encoder_center;
  
  // Whether increasing encoder counts is positive or negative change in angle.
  // Will be multiplicative factor used to convert counts to/from radians.
  // (Should be +/- 1)
  // NOTE: This is controller-to-driver sign; there's a separate configuration
  //       parameter (0x5C) that specifies whether the load encoder is aligned or
  //       anti-aligned with the motor controller's direction.
  int encoder_fin_direction;

  // Min and Max counts (Software hard stop counts)
  int min_goal_counts;
  int max_goal_counts;
  
  int continuous_current_limit;
  // Maximum peak current in normal operation (0.01 A)
  int peak_current_limit;
  // Maximum continuous and peak current used for seeking hard stop (0.01 A)
  int calibration_current_limit;
  // Current threshold used for detecting hard stop (0.01 A)
  int calibration_current_threshold;
  // Hard stop detected when above threshold for this time (ms)
  // Should be longer than the status update period so the stored position
  // will be accurate.
  int calibration_timeout;
  // Velocity to use when seeking hard stop (0.1 counts / sec)
  int calibration_velocity;

  // Minimum and maximum values for the fin, in radians. This should correspond
  // to where the hard stops are, and assumes that they are symmetric about
  // horizontal.
  float max_angle;
  float min_angle;

  // Ratio between output shaft : fin spar
  float gear_ratio;

  // Software limit relative to the absolute min/max range of motion, in counts.
  // Used in an effort to avoid ever hitting the hard stop in operation.
  // (In long moves, with high velocity limits, overshoots of >50 counts
  // have been observed)
  int soft_stop_margin;

  // Sets the the negative stop to be this many counts. This is a bit of a
  // hack to avoid ever having to deal with the encoders wrapping.
  // (Encoder positions has been observed to have +/- 7 counts of noise,
  // so setting zero to the hard stop's position could lead to wrapping)
  int wrap_margin;

  // Computed from the other parameters.
  float counts_per_radian;

};


class HarmonicServo : public ds_base::DsProcess {
  friend class harmonic_servo::ServoFront;

 public:

  explicit HarmonicServo();
  ~HarmonicServo() override;

  DS_DISABLE_COPY(HarmonicServo);

 protected:

  void setupParameters() override;
  void setupConnections() override;
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupServices() override;
  void setupTimers() override;

  void checkProcessStatus(const ros::TimerEvent& event) override;

  // These are used by tests, so can't be private.

  // Put all configuration variables in the same struct...
  ServoConfig config_;
  // Accumulates feedback from the individual callbacks
  ServoFeedback feedback_;

  // How much time can pass since the last received message from the servo before the
  // driver declares status = STATUS_ERROR
  ros::Duration iosm_timeout_;

  // Assorted functions that convert position and velocity from units of
  // encoder counts and counts/sec into radians and radians/sec about the FIN
  // axis. The fin will turn 4/3 radian for every 2 radians at the encoder turns.
  float radiansFromCounts(int counts);
  int countsFromRadians(float radians);
  float velocityFromCounts(int counts);

  // Connected by the state machine; I'm not in love with how entangled the two are.
  // (TODO: Might be better to construct the state machine with command/callback pairs)
  bool positionCallback(ds_core_msgs::RawData msg);
  bool velocityCallback(ds_core_msgs::RawData msg);
  bool inputVoltageCallback(ds_core_msgs::RawData msg);
  bool motorCurrentCallback(ds_core_msgs::RawData msg);
  bool hallStateCallback(ds_core_msgs::RawData msg);
  bool encoderStatusCallback(ds_core_msgs::RawData msg);
  bool faultStatusCallback(ds_core_msgs::RawData msg);
  bool eventStatusCallback(ds_core_msgs::RawData msg);
  bool trajectoryStatusCallback(ds_core_msgs::RawData msg);


 private:

  // This is a pointer so we can initialize it later.
  std::unique_ptr<harmonic_servo::ServoBack> sbp_;
  // Handles communications with the servo
  boost::shared_ptr<ds_asio::IoSM> iosm_;
  // Mutex enforcing serialization of process_event calls.
  std::recursive_mutex event_mutex_;



  // Callbacks providing the driver's ROS interface.

  // The Abort message contains an abort field (for the XRs) and an
  // enable field (for arming the servos/thrusters).
  void handleEnable(const ds_core_msgs::Abort &msg);
  // Receive new goal
  void handleGoal(const ds_actuator_msgs::ServoCmd &msg);
  // Return value indicates whether a transition occurred.
  bool handleCoast(sentry_msgs::ServoCoastCmd::Request& request,
		   sentry_msgs::ServoCoastCmd::Response& response);
  // Start configuration of servo
  bool handleConfig(sentry_msgs::ServoConfigureCmd::Request& request,
		    sentry_msgs::ServoConfigureCmd::Response& response);
  // Go Home when Asked
  bool handleHome(sentry_msgs::ServoHomeCmd::Request& request,
      sentry_msgs::ServoHomeCmd::Response& response);
  // Index to current encoder position
  bool handleIndex(sentry_msgs::ServoIndexCmd::Request& request,
      sentry_msgs::ServoIndexCmd::Response& response);


  // Callback for parsing the motor controller's response to a command.
  // (Anything that should elicit "ok\r" or "e ##\r")
  bool commandCallback(const std::string& cmd, ds_core_msgs::RawData msg);

  // Callback for stepping through configuration list
  bool configCallback(const std::string& code, const std::string& value, ds_core_msgs::RawData msg);

  // Callbacks for handling data from the iosm_;
  // Default callback for every iosm callback; called in addition to the
  // per-command ones. Used to pet watchdog checking on comms with the servo.
  bool iosmCallback(ds_core_msgs::RawData msg);


  // Used to reset the count of received serial numbers on entry to NoDevice state.
  // That state will be exited after receiving two consecutive serial numbers.
  void resetSerialNumberCount();
  int serial_number_count_;
  bool serialNumberCallback(ds_core_msgs::RawData msg);

  // Populate the status message based on current state in state machine
  ds_core_msgs::Status getStatus();

  // Resets goal and commanded counts to clear old goals.
  void resetGoal();

  // Publishes whether goal reset is requested (useful for operator feedback)
  void publishResetGoal(bool goal_reset_requested);

  // Publishes feedback messages on completing service-call triggered indexing action
  void publishIndexResult(bool succeeded);

  // Publishes feedback messages on completing service-call triggered actions
  void publishConfigureResult(bool succeeded);

  // Checks whether we've heard from the servo recently.
  // If not, triggers transition to NoDevice. This check needs to happen more
  // frequently than DsProcess::checkProcessStatus is called by default.
  void checkComms(const ros::TimerEvent& event);
  // Timer that fires to check age of most recent data from the servo
  ros::Timer iosm_timer_;
  // Last time any response was received from the servo.
  ros::Time iosm_time_;

  // Minimum goal that will be sent to the controller (respecting the
  // soft limits on motion).
  int min_goal_counts_;
  // Maximum goal that will be sent to the controller (respecting the
  // soft limits on motion).
  int max_goal_counts_;

  // Check for index requested
  bool index_requested_;
  // Enforce limits on goal positions to keep them from approaching the
  // hard stops.
  int limitGoalRange(int goal_counts);
  // The motor controller isn't configurable to realize that we have hard stops
  // at +/- 2/3 pi, so it'll try to wrap around and take the short way. So,
  // we have to send it intermediate goals, never more than 2/3 pi away.
  int limitGoalStep(int goal_counts, int current_counts);

  void publishFeedback();


  // Used for real-time modeling, includes actual and command position
  ros::Publisher actuator_state_pub_;
  // Same as above, but the type that URDF/TF expects
  ros::Publisher joint_state_pub_;
  // More detailed status information about the servo. Used for diagnostics.
  ros::Publisher servo_state_pub_;
  // Publishes success/failure result for indexing service call
  ros::Publisher index_result_pub_;
  // Publishes success/failure result for configuration service call
  ros::Publisher configure_result_pub_;
  // Publishes whether servo reset goal has been called
  ros::Publisher servo_reset_goal_pub_;

  // enable bit / software kill
  ros::Subscriber abort_sub_;
  // Input servo command from the control chain
  ros::Subscriber command_sub_;
  // Set the servo to coast / re-enable control
  ros::ServiceServer coast_srv_;
  // Tell the servo to configure itself
  ros::ServiceServer configure_srv_;
  // Tell the servo to perform zeroing routine
  ros::ServiceServer zero_srv_;
  // Tell the servo to go home
  ros::ServiceServer home_srv_;
  // Tell the servo to automatically index to encoder center
  ros::ServiceServer index_srv_;

  // Most recent command received from the control chain. Set to current
  // servo position if coasting or disabled.
  int goal_counts_;
  // Most recent goal sent to the servos.
  int commanded_counts_;
  // Maximum allowable step (to deal with motor controller's default
  // wrapping behavior)
  int max_step_;


  // Index into the configuration message (used for stepping through the
  // parameters in response to iosm callbacks).
  int config_index_;
  std::vector<sentry_msgs::ServoParameter> config_params_;

};  // HarmonicServo


}  // namespace harmonic_servo

#endif  // SENTRY_COMPONENTS_HARMONIC_SERVO_HARMONIC_SERVO_H
