/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on 7/2/19.
//
// Utility functions for parsing the data returned by the
// Copley motor controller.

#ifndef SENTRY_COMPONENTS_HARMONIC_SERVO_PARSERS_H
#define SENTRY_COMPONENTS_HARMONIC_SERVO_PARSERS_H

#include <regex>
#include <ros/console.h>  // for rosconsole

namespace harmonic_servo {
  // Possible error codes in response to an ASCII command sent to the Copley
  // motor controller.
  const std::map<int, std::string> copley_error_codes = {
    {1, "Too much data passed with command"},
    {3, "Unknown command code"},
    {4, "Not enough data was supplied with the command"},
    {5, "Too much data was supplied with the command"},
    {9, "Unknown parameter ID"},
    {10, "Data value out of range"},
    {11, "Attempt to modify read-only parameter"},
    {14, "Unknown axis state"},
    {15, "Parameter doesn’t exist on requested page"},
    {16, "Illegal serial port forwarding"},
    {18, "Illegal attempt to start a move while currently moving"},
    {19, "Illegal velocity limit for move"},
    {20, "Illegal acceleration limit for move"},
    {21, "Illegal deceleration limit for move"},
    {22, "Illegal jerk limit for move"},
    {25, "Invalid trajectory mode"},
    {27, "Command is not allowed while CVM is running"},
    {31, "Invalid node ID for serial port forwarding"},
    {32, "CAN Network communications failure"},
    {33, "ASCII command parsing error"},
    {36, "Bad axis letter specified"},
    {46, "Error sending command to encoder"},
    {48, "Unable to calculate filter"}};


  // Parse copley motor controller's response to a query:
  // only handles responses of the form "v ###\r"
  // (uints have not been tested; and "ok" defintely doesn't work)
  // Valid input types:
  // int, short, unsigned long
  template <typename T>
  bool parseResponse(std::vector<uint8_t> raw_data, T *output);
  // This isn't quite right, but seems like the better way to do this
  // (making it explicit that only these two types are supported)
  //bool parseResponse(std::vector<uint8_t> raw_data, int* output);
  //bool parseResponse(std::vector<uint8_t> raw_data, unsigned long* output);


}  // namespace harmonic_servo

#endif  // SENTRY_COMPONENTS_HARMONIC_SERVO_PARSERS_H
