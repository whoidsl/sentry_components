/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

//
// Created by llindzey on 7/2/19.
//

// Enable longer transition table in the boost::msm.
//
// Relevant quote from the documentation:
// https://www.boost.org/doc/libs/1_53_0/libs/msm/doc/HTML/ch03s02.html
/*
  The transition table is actually a MPL vector (or list), which brings the
  limitation that the default maximum size of the table is 20. If you need
  more transitions, overriding this default behavior is necessary, so you
  need to add before any header:

  #define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
  #define BOOST_MPL_LIMIT_VECTOR_SIZE 30 //or whatever you need
  #define BOOST_MPL_LIMIT_MAP_SIZE 30 //or whatever you need

  The other limitation is that the MPL types are defined only up to
  50 entries. For the moment, the only solution to achieve more is to add
  headers to the MPL (luckily, this is not very complicated).
*/
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
#define BOOST_MPL_LIMIT_VECTOR_SIZE 40
#define BOOST_MPL_LIMIT_MAP_SIZE 40

#include "harmonic_servo/harmonic_servo.h"
#include "harmonic_servo/servo_parsers.h"  // utilities for parsing responses from the servo.
#include "harmonic_servo/servo_state_machine.h"

#include <algorithm>  // for std::remove
#include <math.h>     // for M_PI

namespace harmonic_servo
{

HarmonicServo::HarmonicServo() : ds_base::DsProcess()
{
  serial_number_count_ = 0;
  goal_counts_ = -1;
  commanded_counts_ = -1;
}

HarmonicServo::~HarmonicServo() = default;

void HarmonicServo::setupConnections()
{
  ds_base::DsProcess::setupConnections();
  auto cb = boost::bind(&HarmonicServo::iosmCallback, this, _1);
  iosm_ = addIoSM("statemachine", "device", cb);
  sbp_.reset(new harmonic_servo::ServoBack(this, iosm_.get()));
  sbp_->start();
}

void HarmonicServo::setupParameters()
{
  ds_base::DsProcess::setupParameters();
  ros::NodeHandle nh("~");
  if (!nh.getParam("urdf_joint_name", config_.joint_name))
  {
    ROS_FATAL("Unable to find parameter urdf_joint_name");
    ROS_BREAK();
  }

  // How long the driver will wait for an answer from the servo before
  // reporting an error.
  double timeout;
  if (!nh.getParam("iosm_timeout", timeout))
  {
    ROS_FATAL("Unable to find parameter iosm_timeout");
    ROS_BREAK();
  }
  iosm_timeout_ = ros::Duration(timeout);
  if (!nh.getParam("continuous_current_limit", config_.continuous_current_limit))
  {
    ROS_FATAL("Unable to find parameter continuous_current_limit");
    ROS_BREAK();
  }

  if (!nh.getParam("peak_current_limit", config_.peak_current_limit))
  {
    ROS_FATAL("Unable to find parameter peak_current_limit");
    ROS_BREAK();
  }

  if (!nh.getParam("calibration_current_limit", config_.calibration_current_limit))
  {
    ROS_FATAL("Unable to find parameter calibration_current_limit");
    ROS_BREAK();
  }

  if (!nh.getParam("calibration_current_threshold", config_.calibration_current_threshold))
  {
    ROS_FATAL("Unable to find parameter calibration_current_threshold");
    ROS_BREAK();
  }
  if (config_.calibration_current_threshold >= config_.calibration_current_limit)
  {
    ROS_FATAL("calibration current threshold must be lower than absolute limit");
    ROS_BREAK();
  }

  if (!nh.getParam("calibration_timeout", config_.calibration_timeout))
  {
    ROS_FATAL("Unable to find parameter calibration_timeout");
    ROS_BREAK();
  }

  if (!nh.getParam("calibration_velocity", config_.calibration_velocity))
  {
    ROS_FATAL("Unable to find parameter calibration_velocity");
    ROS_BREAK();
  }

  if (!nh.getParam("gear_ratio", config_.gear_ratio))
  {
    ROS_FATAL("Unable to find parameter gear_ratio");
    ROS_BREAK();
  }
  /*
  if (2 * M_PI < config_.gear_ratio * (config_.max_angle - config_.min_angle))
  {
    ROS_FATAL("Input gear ratio and fin motion extrema would lead to encoder wraping");
    ROS_BREAK();
  }
*/
  if (!nh.getParam("soft_stop_margin", config_.soft_stop_margin))
  {
    ROS_FATAL("Unable to find parameter soft_stop_margin");
    ROS_BREAK();
  }
  ROS_ASSERT(config_.soft_stop_margin >= 0);

  if (!nh.getParam("wrap_margin", config_.wrap_margin))
  {
    ROS_FATAL("Unable to find parameter wrap_margin");
    ROS_BREAK();
  }
  ROS_ASSERT(config_.wrap_margin >= 0);

  if (!nh.getParam("encoder_counts", config_.encoder_counts))
  {
    ROS_FATAL("Unable to find parameter encoder_counts");
    ROS_BREAK();
  }

  if (!nh.getParam("min_goal_counts", config_.min_goal_counts))
  {
    ROS_FATAL("Unable to find parameter min_goal_counts");
    ROS_BREAK();
  }

  if (!nh.getParam("max_goal_counts", config_.max_goal_counts))
  {
    ROS_FATAL("Unable to find parameter max_goal_counts");
    ROS_BREAK();
  }
  if (!nh.getParam("encoder_center", config_.encoder_center))
  {
    ROS_FATAL("Unable to find parameter encoder_center");
    ROS_BREAK();
  }
  if (!nh.getParam("encoder_fin_direction", config_.encoder_fin_direction))
  {
    ROS_FATAL("Unable to find parameter encoder_fin_direction");
    ROS_BREAK();
  }
  ROS_ASSERT(1 == abs(config_.encoder_fin_direction));

  // Known based on the servo's design: encoder is 1:1 with servo output,
  // and there's a 24:18 ratio from output gear to fin gear.
  config_.counts_per_radian = config_.encoder_counts * config_.gear_ratio / (2 * M_PI);
  // Assumes that min_angle < 0, and center corresponds to 0 radians.

  /*
  // TODO: This needs to use max_angle if encoder_fin_direction is -1
   if (config_.encoder_fin_direction > 0)
   {
     config_.encoder_center = -1 * config_.encoder_ctr;
   }
    else {
     config_.encoder_center = config_.encoder_ctr;
   }
  */
  index_requested_ = false;
  ROS_WARN_STREAM("Finished configuring servo.");
  ROS_WARN_STREAM("Center at: " << config_.encoder_center << " counts, and " << config_.counts_per_radian
                                << " counts/radian");

  max_step_ = (int)std::round(config_.encoder_counts / 3);
  min_goal_counts_ = config_.min_goal_counts + config_.soft_stop_margin;
  max_goal_counts_ = config_.max_goal_counts - config_.soft_stop_margin;

  if (max_goal_counts_ > config_.encoder_counts - 2 * config_.wrap_margin)
  {
    ROS_FATAL("Configured fin range of motion too high; encoder may wrap.");
    ROS_BREAK();
  }

  ROS_WARN_STREAM("Configured hard stop limits. min: " << min_goal_counts_ << " max: " << max_goal_counts_);
}

void HarmonicServo::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();
  auto nh = nodeHandle("~");

  const int queue_size = 1;
  actuator_state_pub_ = nh.advertise<ds_actuator_msgs::ServoState>("state", queue_size, false);
  joint_state_pub_ = nh.advertise<sensor_msgs::JointState>("joint_states", queue_size, false);
  servo_state_pub_ = nh.advertise<sentry_msgs::HarmonicServoState>("sentry_servo_state", queue_size, false);
  configure_result_pub_ = nh.advertise<sentry_msgs::ServoConfigureResult>("servo_config_result", queue_size, false);
  index_result_pub_ = nh.advertise<sentry_msgs::ServoIndexResult>("servo_index_result", queue_size, false);
  servo_reset_goal_pub_ = nh.advertise<ds_core_msgs::KeyBool>("servo_reset_goal", queue_size, false);
  // Check for empty ROS Publisher
  if (!actuator_state_pub_ || !joint_state_pub_ || !servo_state_pub_ || !configure_result_pub_ || !index_result_pub_ || !servo_reset_goal_pub_ )
  {
    ROS_FATAL("Unable to create publisher. Exiting.");
    ROS_BREAK();
  }
}

void HarmonicServo::setupSubscriptions()
{
  ds_base::DsProcess::setupSubscriptions();
  auto nh = nodeHandle("~");

  // "enable" bit / software kill
  abort_sub_ = nh.subscribe("abort", 10, &HarmonicServo::handleEnable, this);

  // "cmd" for ds_actuator_msgs/ServoCommand
  command_sub_ = nh.subscribe("cmd", 10, &HarmonicServo::handleGoal, this);
}

void HarmonicServo::setupServices()
{
  ds_base::DsProcess::setupServices();
  auto nh = nodeHandle("~");
  auto coast_cb = boost::bind(&HarmonicServo::handleCoast, this, _1, _2);
  coast_srv_ = nh.advertiseService<sentry_msgs::ServoCoastCmd::Request, sentry_msgs::ServoCoastCmd::Response>("coast_"
                                                                                                              "cmd",
                                                                                                              coast_cb);

  auto config_cb = boost::bind(&HarmonicServo::handleConfig, this, _1, _2);
  configure_srv_ =
      nh.advertiseService<sentry_msgs::ServoConfigureCmd::Request, sentry_msgs::ServoConfigureCmd::Response>("config_"
                                                                                                             "cmd",
                                                                                                             config_cb);
  auto index_cb = boost::bind(&HarmonicServo::handleIndex, this, _1, _2);
  index_srv_ = nh.advertiseService<sentry_msgs::ServoIndexCmd::Request, sentry_msgs::ServoIndexCmd::Response>("index_"
                                                                                                              "cmd",
                                                                                                              index_cb);

  auto home_cb = boost::bind(&HarmonicServo::handleHome, this, _1, _2);
  home_srv_ =
      nh.advertiseService<sentry_msgs::ServoHomeCmd::Request, sentry_msgs::ServoHomeCmd::Response>("goto_index_cmd", home_cb);
}

void HarmonicServo::setupTimers()
{
  DsProcess::setupTimers();

  auto nh = nodeHandle("~");
  auto cb = boost::bind(&HarmonicServo::checkComms, this, _1);
  iosm_timer_ = nh.createTimer(iosm_timeout_, cb);
}

void HarmonicServo::checkProcessStatus(const ros::TimerEvent& event)
{
  auto status = getStatus();
  publishStatus(status);
}

ds_core_msgs::Status HarmonicServo::getStatus()
{
  ros::Time now = ros::Time::now();
  ds_core_msgs::Status status;
  status.descriptive_name = descriptiveName();
  status.ds_header.io_time = now;

  int state = sbp_->current_state()[0];
  if (state == 1)
  {
    // Don't currently have comms with the device
    status.status = status.STATUS_ERROR;
  }
  else if (state != 3)
  {
    // Servo is talking, but either it's disabled or coasting.  So status=WARN
    status.status = status.STATUS_WARN;
  }
  else
  {
    // Servo not disabled and is in active control state.
    // Ready to accept new commands.
    status.status = status.STATUS_GOOD;
  }
  return status;
}

void HarmonicServo::handleEnable(const ds_core_msgs::Abort& msg)
{
  std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
  if (msg.enable)
  {
    sbp_->process_event(Enable());
  }
  else
  {
    sbp_->process_event(Brake());
  }
}

bool HarmonicServo::handleCoast(sentry_msgs::ServoCoastCmd::Request& request,
                                sentry_msgs::ServoCoastCmd::Response& response)
{
  std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
  bool transitioned = false;
  if (request.coast)
  {
    transitioned = sbp_->process_event(StartCoast());
  }
  else
  {
    transitioned = sbp_->process_event(StopCoast());
  }
  return transitioned;
}

bool HarmonicServo::handleConfig(sentry_msgs::ServoConfigureCmd::Request& request,
                                 sentry_msgs::ServoConfigureCmd::Response& response)
{
  bool transitioned = false;
  ROS_INFO_STREAM("Received Configure command!");
  // TODO: This SHOULD be published as a message (since services aren't logged),
  //    but perhaps as one, and not several hundred =)
  for (const auto param : request.parameters)
  {
    ROS_INFO_STREAM("..." << param.code << ": " << param.value);
  }

  config_index_ = 0;
  config_params_ = request.parameters;
  if (config_params_.size() > config_index_)
  {
    std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
    auto param = config_params_.at(config_index_);
    transitioned = sbp_->process_event(Configure(param.code, param.value));
    config_index_ += 1;
    return transitioned;
  }
  else
  {
    return true;
  }
}

// Indexing service: handles indexing to current position as ctr point
// Should be run at install time to set encoder ctr
// Can be run at anytime to set new encoder ctr temporarily
bool HarmonicServo::handleIndex(sentry_msgs::ServoIndexCmd::Request& request,
                                sentry_msgs::ServoIndexCmd::Response& response)
{
  std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
  bool transitioned = false;
  if (!feedback_.encoder_data_valid) {
    ROS_ERROR_STREAM("Attempt to set index value while feedback data is invalid");
    return false;
  }

  config_.encoder_center = feedback_.position_counts;
  if (config_.encoder_center - config_.wrap_margin <= feedback_.position_counts && feedback_.position_counts <= config_.encoder_center + config_.wrap_margin) {
    ROS_WARN_STREAM("Already within index bounds, not indexing again");
    return true;
  }

  ROS_WARN_STREAM("New index position set at " << config_.encoder_center << " counts");
  //sbp_->process_event(Enable());
  ROS_INFO_STREAM("Drive is referenced, going to index point: " << config_.encoder_center);
  float counts = config_.encoder_center;
  goal_counts_ = counts;
  commanded_counts_ = limitGoalStep(goal_counts_, feedback_.position_counts);
  ROS_INFO_STREAM("Goal counts is: " << goal_counts_ << " and commanded counts is: " << commanded_counts_);
  sbp_->process_event(UpdateCommand(commanded_counts_));
  return true;
}

// Homing service: handles going to index position once servo is indexed
// Should be run pre-dive to drive servos to index position
bool HarmonicServo::handleHome(sentry_msgs::ServoHomeCmd::Request& request,
                               sentry_msgs::ServoHomeCmd::Response& response)
{
  std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
  if (config_.encoder_center == 0) {
    ROS_ERROR_STREAM("Encoder center set to 0, this doesn't seem right");
  }

  if (feedback_.encoder_data_valid && feedback_.trajectory_status_msg.referenced)
  {
    ROS_WARN_STREAM("Current pos is: "<<feedback_.position_counts<<" | index point is: "<<config_.encoder_center);
    //ROS_INFO_STREAM("Drive is referenced, going to index point: " << config_.encoder_center);
    float counts = config_.encoder_center;
    goal_counts_ = counts;
    commanded_counts_ = limitGoalStep(goal_counts_, feedback_.position_counts);
    ROS_WARN_STREAM("Goal counts is: " << goal_counts_ << " and commanded counts is: " << commanded_counts_);
    sbp_->process_event(UpdateCommand(commanded_counts_));
    if (commanded_counts_ == config_.encoder_center) {
	    index_requested_ = true;
	    return true;
    }
    else {
	ROS_ERROR_STREAM("Failed to go to index position");
	publishIndexResult(false);
	return false;
   	 }
   }
   else
   {
    ROS_ERROR_STREAM("Indexing failed: Encoder data invalid or drive not referenced");
    publishIndexResult(false);
    return false;
   }
}

void HarmonicServo::handleGoal(const ds_actuator_msgs::ServoCmd& msg)
{
  auto cmd_radians = msg.cmd_radians;
  if (config_.encoder_fin_direction > 0) {
    cmd_radians = -1 * msg.cmd_radians;
  }
  else {
    cmd_radians = config_.encoder_fin_direction * msg.cmd_radians;
  }
  
  std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
  ROS_INFO_STREAM("received command message: " << msg.cmd_radians << " radians");
  // This doesn't need to check on the driver's current state because this event
  // will only be handled if it is in a mode that accepts commands.
  float counts = countsFromRadians(cmd_radians);
  goal_counts_ = limitGoalRange(counts);
  commanded_counts_ = limitGoalStep(goal_counts_, feedback_.position_counts);
  sbp_->process_event(UpdateCommand(commanded_counts_));
}

int HarmonicServo::limitGoalRange(int goal_counts) {
  if (goal_counts < min_goal_counts_) {
    ROS_ERROR_STREAM("Received goal position out of safe range: " << std::to_string(goal_counts) << " truncating to " << feedback_.position_counts);
    return feedback_.position_counts;
  } else if(goal_counts > max_goal_counts_) {
    ROS_ERROR_STREAM("Received goal position out of safe range: " << std::to_string(goal_counts) << " truncating to " << feedback_.position_counts);
    return feedback_.position_counts;
  } else {
    return goal_counts;
  }
}

int HarmonicServo::limitGoalStep(int goal_counts, int current_counts)
{
  int limited_counts;
  int dcounts = goal_counts - current_counts;
  if (dcounts < -1 * max_step_)
  {
    limited_counts = current_counts - max_step_;
    ROS_INFO_STREAM("Limiting goal counts: " << goal_counts << " -> " << limited_counts);
  }
  else if (dcounts > max_step_)
  {
    limited_counts = current_counts + max_step_;
    ROS_INFO_STREAM("Limiting goal counts: " << goal_counts << " -> " << limited_counts);
  }
  else
  {
    limited_counts = goal_counts;
  }
  return limited_counts;
}

void HarmonicServo::checkComms(const ros::TimerEvent& event)
{
  ros::Time now = ros::Time::now();
  ros::Duration iosm_age = now - iosm_time_;

  if (iosm_timeout_ > ros::Duration(0) && iosm_age > iosm_timeout_)
  {
    ROS_INFO_STREAM("Have not heard from device! iosm_age: " << iosm_age);
    std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
    sbp_->process_event(NoResponse());
  }
}

bool HarmonicServo::commandCallback(const std::string& command, ds_core_msgs::RawData msg)
{
  // This can't use the existing parseResponse function because it expects "ok"
  // rather than "v #"

  // Strip off final \r
  std::string data = std::string(std::begin(msg.data), std::end(msg.data) - 1);

  // Strip "\r" using erase-remove idiom.
  std::string cmd = command;
  cmd.erase(std::remove(cmd.begin(), cmd.end(), '\r'), cmd.end());

  ROS_INFO_STREAM("Received response to command: " << cmd << " " << data);
  bool msg_valid = true;
  int error_code;
  if (data == "ok")
  {
    ROS_INFO_STREAM("....command successful!");
    if (cmd == "s r0xc2 2049")
    {
      sbp_->process_event(DoneIndexing());
    }
  }
  else if (1 == sscanf(data.c_str(), "e %d", &error_code))
  {
    ROS_ERROR_STREAM("Error response to " << cmd << ": " << copley_error_codes.at(error_code));
  }
  else
  {
    msg_valid = false;
    ROS_ERROR_STREAM("Unrecognized response to " << cmd << ": " << data
                                                 << ". THIS IS PROBABLY A SYNCHRONIZATION ERROR.");
  }
  return msg_valid;
}

bool HarmonicServo::configCallback(const std::string& code, const std::string& value, ds_core_msgs::RawData msg)
{
  std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
  // Strip off final \r
  std::string data = std::string(std::begin(msg.data), std::end(msg.data) - 1);
  int error_code;
  bool msg_valid = true;
  if (data == "ok")
  {
    if (config_params_.size() > config_index_)
    {
      auto param = config_params_.at(config_index_);
      sbp_->process_event(Configure(param.code, param.value));
      config_index_ += 1;
    }
    else
    {
      sbp_->process_event(ConfiguringSucceeded());
    }
  }
  else
  {
    if (1 == sscanf(data.c_str(), "e %d", &error_code))
    {
      ROS_ERROR_STREAM("Error response to " << code << " " << value << ": " << copley_error_codes.at(error_code));
      sbp_->process_event(ConfiguringFailed());
    }
    else
    {
      msg_valid = false;
      ROS_ERROR_STREAM("Unrecognized response to " << code << " " << value << ": " << data
                                                   << ". THIS IS PROBABLY A SYNCHRONIZATION ERROR.");
      sbp_->process_event(ConfiguringFailed());
    }
  }
  return msg_valid;
}

void HarmonicServo::publishIndexResult(bool succeeded)
{
  sentry_msgs::ServoIndexResult result;
  result.header.stamp = ros::Time::now();
  result.succeeded = succeeded;
  index_result_pub_.publish(result);
}

void HarmonicServo::publishConfigureResult(bool succeeded)
{
  sentry_msgs::ServoConfigureResult result;
  result.header.stamp = ros::Time::now();
  result.succeeded = succeeded;
  std::string config_str = "";
  for (auto param : config_params_)
  {
    config_str = config_str + param.code + ":" + param.value + ",";
  }
  result.configuration = config_str;
  configure_result_pub_.publish(result);
}

bool HarmonicServo::iosmCallback(ds_core_msgs::RawData msg)
{
  iosm_time_ = ros::Time::now();
  // The iosm's decision to advance is an AND of this global callback and the
  // command-specific one, so all sanity checking happens in the other ones.
  return true;
}

// TODO(llindzey): Errors in any of the parsing functions are probably a synchronization
// error; figure out how to deal with those!
// According to the parameter list, 0x17 has return type int32
bool HarmonicServo::positionCallback(ds_core_msgs::RawData msg)
{
  bool msg_valid = true;
  int position;
  bool succeeded = parseResponse(msg.data, &position);
  if (succeeded)
  {
    if (position >= 0 && position < config_.encoder_counts)
    {
      feedback_.position_counts = position;
      feedback_.position_radians = radiansFromCounts(position);
      feedback_.position_time = ros::Time::now();
    }
    else
    {
      ROS_ERROR_STREAM("Received invalid servo position: " << position << "(counts)");
      msg_valid = false;
    }
  }
  publishFeedback();

  // Don't need to check what the current state is because:
  // 1) These should only differ when in Nominal, and
  // 2) This event has no effect except in Nominal
  if (goal_counts_ != commanded_counts_)
  {
    std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
    commanded_counts_ = limitGoalStep(goal_counts_, feedback_.position_counts);
    sbp_->process_event(UpdateCommand(commanded_counts_));
  }
  return msg_valid;
}

// According to the parameter list, 0x18 has return type int32
bool HarmonicServo::velocityCallback(ds_core_msgs::RawData msg)
{
  int velocity;
  bool succeeded = parseResponse(msg.data, &velocity);
  if (succeeded)
  {
    // For now, no meaningful way to detect error based on value.
    feedback_.velocity_counts = velocity;
    feedback_.velocity_radians = velocityFromCounts(velocity);
  }
  return succeeded;
}

// According to the parameter list, 0x1E has return type int16 and units 100mV
bool HarmonicServo::inputVoltageCallback(ds_core_msgs::RawData msg)
{
  int voltage;
  bool succeeded = parseResponse(msg.data, &voltage);
  if (succeeded)
  {
    float voltage_volts = voltage / 10.;
    if (voltage_volts < 20 || voltage_volts > 60)
    {
      // In bench testing, I've seen input voltage spike > 60 V
      // with a power supply set at 50 V, due to back EMF.
      ROS_ERROR_STREAM("Servo's input voltage out of bounds: " << std::to_string(voltage_volts));
    }
    feedback_.input_voltage = voltage;
  }
  // The validity checks above aren't absolute enough to use to indicate
  // that the iosm is at fault, so just check that it parsed.
  return succeeded;
}

// According to the parameter list, 0x38 has return type int16 and units 0.01A
bool HarmonicServo::motorCurrentCallback(ds_core_msgs::RawData msg)
{
  int motor_current;
  bool msg_valid = false;
  bool succeeded = parseResponse(msg.data, &motor_current);
  if (succeeded)
  {
    float current_amps = 1.0 * motor_current / 100;
    if (abs(current_amps) < config_.peak_current_limit)
    {
      msg_valid = true;
    }
    // Turns out that reported motor current varies more widely than
    // originally expected, so it's not a great candidate for determining
    // synchronization.
    /**
    else {
      ROS_ERROR_STREAM("Servo's motor current out of bounds: "
           << std::to_string(current_amps)
           << "Probably a synchronization error");
    }
    **/
    feedback_.motor_current = motor_current;
  }
  // TODO: Once I have more confidence that the above bounds are
  //       absolutely true, switch this to msg_valid.
  return succeeded;
}

// According to the parameter list, 0xA2 has return type int16
bool HarmonicServo::hallStateCallback(ds_core_msgs::RawData msg)
{
  int halls;
  bool msg_valid = false;
  bool succeeded = parseResponse(msg.data, &halls);
  if (succeeded)
  {
    if (halls >= 0 && halls < 8)
    {
      feedback_.hall_states = halls;
      feedback_.hall_msg.ds_header.io_time = ros::Time::now();
      feedback_.hall_msg.status = halls;
      // The manual states that this parameter gives the value after corrections
      // specified by 0x52 have been applied, so I'm assuming that u,v,w are
      // bits 0,1,2.
      // TODO: The resulting logs haven't yet been analyzed for utility
      //       (sufficient data rate) or correctness (of hall ordering)
      feedback_.hall_msg.u = (0 != (halls & 1));
      feedback_.hall_msg.v = (0 != (halls & 2));
      feedback_.hall_msg.w = (0 != (halls & 4));
      msg_valid = true;
    }
    else
    {
      ROS_ERROR_STREAM("Received invalid hall state: " << halls);
    }
  }
  return msg_valid;
}

// 0x12F U32
//  I still haven't confirmed that this does what I think it does.
//  * I sent an email to the Copley guy asking about how 11 bits of data
//    will be  mapped onto 7 bits of status. I suspect that the 4 MSB are
//    lost, including the one status field that the encoder actually provides.
//  * I haven't yet confirmed that the "invalid data" flag propagates as
//    intended -- waiting to talk with Justin/Sean about safe ways to do so.
bool HarmonicServo::encoderStatusCallback(ds_core_msgs::RawData msg)
{
  unsigned long status;
  bool succeeded = parseResponse(msg.data, &status);
  if (succeeded)
  {
    // This is an implicit conversion from unsigned long -> uint32_t, but that's fine.
    feedback_.encoder_status = status;
  }
  // bits 0-6 are "fault flags returned by encoder"
  // bit 15 is "encoder data invalid"
  feedback_.encoder_data_valid = succeeded && (0 == (status & 1 << 15));
  return succeeded;
}

// 0xA4 U32
bool HarmonicServo::faultStatusCallback(ds_core_msgs::RawData msg)
{
  unsigned long status;
  bool succeeded = parseResponse(msg.data, &status);
  if (succeeded)
  {
    // Since this is interpreted as a bit field, there's no way to sanity-check
    feedback_.fault_status = status;
    feedback_.fault_status_msg.ds_header.io_time = ros::Time::now();

    feedback_.fault_status_msg.status = status;

    feedback_.fault_status_msg.crc_failure = (0 != (status & 1));
    feedback_.fault_status_msg.internal_error = (0 != (status & 1 << 1));
    feedback_.fault_status_msg.short_circuit = (0 != (status & 1 << 2));
    feedback_.fault_status_msg.drive_over_temperature = (0 != (status & 1 << 3));
    feedback_.fault_status_msg.motor_over_temperature = (0 != (status & 1 << 4));
    feedback_.fault_status_msg.over_voltage = (0 != (status & 1 << 5));
    feedback_.fault_status_msg.under_voltage = (0 != (status & 1 << 6));
    feedback_.fault_status_msg.feedback_fault = (0 != (status & 1 << 7));

    feedback_.fault_status_msg.phasing_error = (0 != (status & 1 << 8));
    feedback_.fault_status_msg.tracking_error = (0 != (status & 1 << 9));
    feedback_.fault_status_msg.current_limited = (0 != (status & 1 << 10));
    feedback_.fault_status_msg.fpga_error1 = (0 != (status & 1 << 11));
    feedback_.fault_status_msg.command_input_lost = (0 != (status & 1 << 12));
    feedback_.fault_status_msg.fpga_error2 = (0 != (status & 1 << 13));
    feedback_.fault_status_msg.unable_to_control_current = (0 != (status & 1 << 14));
    feedback_.fault_status_msg.wiring_disconnected = (0 != (status & 1 << 15));
  }
  return succeeded;
}

// 0xAC U32 is the sticky event status; it's automatically cleared when read
bool HarmonicServo::eventStatusCallback(ds_core_msgs::RawData msg)
{
  unsigned long status;
  bool succeeded = parseResponse(msg.data, &status);
  if (succeeded)
  {
    feedback_.event_status = status;
    feedback_.event_status_msg.ds_header.io_time = ros::Time::now();

    feedback_.event_status_msg.status = status;

    feedback_.event_status_msg.short_circuit = (0 != (status & 1));
    feedback_.event_status_msg.over_temperature = (0 != (status & 1 << 1));
    feedback_.event_status_msg.over_voltage = (0 != (status & 1 << 2));
    feedback_.event_status_msg.under_voltage = (0 != (status & 1 << 3));
    feedback_.event_status_msg.temperature_sensor_active = (0 != (status & 1 << 4));
    feedback_.event_status_msg.feedback_error = (0 != (status & 1 << 5));
    feedback_.event_status_msg.phasing_error = (0 != (status & 1 << 6));
    feedback_.event_status_msg.current_output_limited = (0 != (status & 1 << 7));

    feedback_.event_status_msg.voltage_output_limited = (0 != (status & 1 << 8));
    feedback_.event_status_msg.positive_limit_switch = (0 != (status & 1 << 9));
    feedback_.event_status_msg.negative_limit_switch = (0 != (status & 1 << 10));
    feedback_.event_status_msg.enable_not_active = (0 != (status & 1 << 11));
    feedback_.event_status_msg.software_disable = (0 != (status & 1 << 12));
    feedback_.event_status_msg.stopping_motor = (0 != (status & 1 << 13));
    feedback_.event_status_msg.brake_activated = (0 != (status & 1 << 14));
    feedback_.event_status_msg.pwm_output_disabled = (0 != (status & 1 << 15));

    feedback_.event_status_msg.positive_software_limit = (0 != (status & 1 << 16));
    feedback_.event_status_msg.negative_software_limit = (0 != (status & 1 << 17));
    feedback_.event_status_msg.tracking_error = (0 != (status & 1 << 18));
    feedback_.event_status_msg.tracking_warning = (0 != (status & 1 << 19));
    feedback_.event_status_msg.drive_in_reset = (0 != (status & 1 << 20));
    feedback_.event_status_msg.position_wrapped = (0 != (status & 1 << 21));
    feedback_.event_status_msg.drive_fault = (0 != (status & 1 << 22));
    feedback_.event_status_msg.velocity_limit = (0 != (status & 1 << 23));

    feedback_.event_status_msg.acceleration_limit = (0 != (status & 1 << 24));
    feedback_.event_status_msg.tracking_window = (0 != (status & 1 << 25));
    feedback_.event_status_msg.home_switch_active = (0 != (status & 1 << 26));
    feedback_.event_status_msg.in_motion = (0 != (status & 1 << 27));
    feedback_.event_status_msg.velocity_window = (0 != (status & 1 << 28));
    feedback_.event_status_msg.phase_uninitialized = (0 != (status & 1 << 29));
    feedback_.event_status_msg.command_fault = (0 != (status & 1 << 30));
  }
  return succeeded;
}

// 0xC9 is an int16, and it WILL sometimes be reported as a negative int in ascii
// TODO: The way this was implemented when tested on the vehicle on 10/7/2019
// led to the error "expected non-negative input. got -24320". This makes
// sense in the case that the in-motion flag was set and the type was
// a int16. sscanf(...%hi...)
bool HarmonicServo::trajectoryStatusCallback(ds_core_msgs::RawData msg)
{
  short status;
  bool succeeded = parseResponse(msg.data, &status);

  if (succeeded)
  {
    // NOTE: This is is OK because it's casting signed to unsigned, so the
    //       bit representation will not change.
    feedback_.trajectory_status = status;
    feedback_.trajectory_status_msg.ds_header.io_time = ros::Time::now();

    feedback_.trajectory_status_msg.status = status;

    // bits 0-8 and 10 are reserved
    feedback_.trajectory_status_msg.cam_table_underflow = (0 != (status & 1 << 9));
    feedback_.trajectory_status_msg.homing_error = (0 != (status & 1 << 11));
    feedback_.trajectory_status_msg.referenced = (0 != (status & 1 << 12));
    feedback_.trajectory_status_msg.homing = (0 != (status & 1 << 13));
    feedback_.trajectory_status_msg.move_aborted = (0 != (status & 1 << 14));
    feedback_.trajectory_status_msg.in_motion = (0 != (status & 1 << 15));
  }
  return succeeded;
}

void HarmonicServo::publishResetGoal(bool goal_reset_requested)
{
  ds_core_msgs::KeyBool reset_goal_msg;
  reset_goal_msg.key = config_.joint_name;
  reset_goal_msg.value = goal_reset_requested;
  servo_reset_goal_pub_.publish(reset_goal_msg);
}

void HarmonicServo::resetGoal()
{
  ds_core_msgs::KeyBool reset_goal_msg;
  ROS_WARN_STREAM("resetGoal called! setting to " << feedback_.position_counts);
  goal_counts_ = feedback_.position_counts;
  commanded_counts_ = feedback_.position_counts;
  reset_goal_msg.key = config_.joint_name;
  reset_goal_msg.value = true;
  servo_reset_goal_pub_.publish(reset_goal_msg);
}

void HarmonicServo::resetSerialNumberCount()
{
  serial_number_count_ = 0;
}

bool HarmonicServo::serialNumberCallback(ds_core_msgs::RawData msg)
{
  auto str = std::string(std::begin(msg.data), std::end(msg.data));
  ROS_INFO_STREAM("serialNumberCallback received: " << str);
  int serial_number;
  bool success = parseResponse(msg.data, &serial_number);
  if (success)
  {
    serial_number_count_ += 1;
  }
  // Require 2x receipt of the serial number to exit the NoDevice state.
  // This is a hacky way of ensuring that a full set of status data has
  // been received before initiating/resuming normal operation.
  if (serial_number_count_ > 1)
  {
    std::lock_guard<std::recursive_mutex> eventLock(event_mutex_);
    sbp_->process_event(DeviceDetected());
  }
  return success;
}

void HarmonicServo::publishFeedback()
{
  // TODO(llindzey): This section should ALSO be robust to not having received
  //     one of those messages yet. Maybe I should initialize the map?
  auto now = ros::Time::now();

  ds_actuator_msgs::ServoState servo_msg;
  servo_msg.header.stamp = now;
  // QUESTION(llindzey): Why didn't sail_servo fill in the frame here?
  servo_msg.ds_header.io_time = now;

  servo_msg.cmd_radians = radiansFromCounts(commanded_counts_);
  servo_msg.actual_counts = feedback_.position_counts;
  servo_msg.cmd_counts = commanded_counts_;
  servo_msg.servo_name = config_.joint_name;
  // TODO(llindzey): Actually implement the enable logic =)
  servo_msg.enable = true;
  servo_msg.actual_radians = feedback_.position_radians;

  if (index_requested_) {
  	if (config_.encoder_center - config_.wrap_margin <= feedback_.position_counts && feedback_.position_counts <= config_.encoder_center + config_.wrap_margin) {
		index_requested_ = false;
		publishIndexResult(true);
  }
	else {
		publishIndexResult(false);
	}
  }
  actuator_state_pub_.publish(servo_msg);

  sensor_msgs::JointState joint_msg;
  joint_msg.header.stamp = now;
  joint_msg.name.push_back(config_.joint_name);
  joint_msg.position.push_back(feedback_.position_radians);
  // QUESTION(llindzey): The previous driver didn't fill in the velocity or
  //    effort fields. Since we have those now, should I go ahead and do so?
  //    (Velocity is easy -- effort is presumably related to motor_current.)

  joint_state_pub_.publish(joint_msg);

  sentry_msgs::HarmonicServoState state_msg;
  state_msg.header.stamp = now;
  state_msg.ds_header.io_time = now;
  state_msg.servo_state = servo_msg;
  state_msg.encoder_counts = feedback_.position_counts;
  state_msg.actual_velocity = feedback_.velocity_radians;
  // Convert to Volts from returned value in 100mV units.
  state_msg.input_voltage = 1.0 * feedback_.input_voltage / 10;
  // Convert to Amps from returned value in 0.01A units.
  state_msg.motor_current = 1.0 * feedback_.motor_current / 100;

  state_msg.halls = feedback_.hall_msg;
  state_msg.event_status = feedback_.event_status_msg;
  state_msg.fault_status = feedback_.fault_status_msg;
  state_msg.trajectory_status = feedback_.trajectory_status_msg;

  state_msg.encoder_status_register = feedback_.encoder_status;
  state_msg.encoder_data_valid = feedback_.encoder_data_valid;

  servo_state_pub_.publish(state_msg);
}

// These operate on an internal representation, and are not expected to perform
// checks for data validity; that happens in the ROS and IoCommand callbacks.
float HarmonicServo::radiansFromCounts(int counts)
{
  return config_.encoder_fin_direction * (counts - config_.encoder_center) / config_.counts_per_radian;
}

int HarmonicServo::countsFromRadians(float radians)
{
  return (int)std::round(config_.encoder_center + config_.encoder_fin_direction * radians * config_.counts_per_radian);
}

// TODO(llindzey): I don't love this name, since the instrument reports values
//   in units of 0.1 counts/sec. Is there a typical short name for "wire units"?
float HarmonicServo::velocityFromCounts(int counts)
{
  return 0.1 * counts / config_.counts_per_radian;
}

}  // namespace harmonic_servo
