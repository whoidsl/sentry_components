/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on 10/7/19.
//
// Utility functions for parsing the data returned by the
// Copley motor controller.

#include "harmonic_servo/servo_parsers.h"

namespace harmonic_servo {

// QUESTION(llindzey): This is ugly, but I don't know a cleaner way to
//   do this in C++. sscanf has nicer syntax, but matches greedily and succeeds
//   in cases that I'd want it to fail (e.g. 'v 123abc\r') and in cases of
//   overflow (e.g. it'll silently wrap given (1UL<<32) + (1<<10), rather than
//   throwing an exception like stoi).
//   Really, the cleanest option in code might have been to use the
//   binary interface to the controller.
  template <typename T>
bool parseResponse(std::vector<uint8_t> raw_data, T *output) {
  std::string data_string = std::string(raw_data.begin(), raw_data.end());

  const std::regex data_regex("([ve]) (-?[0-9]+)\\r");
  std::smatch data_match;
  if(!std::regex_search(data_string, data_match, data_regex)) {
    ROS_ERROR_STREAM("Harmonic Servo driver unable to parse response: " << data_string);
    return false;
  }

  T returned_value;
  try {
    if (boost::is_same<T,int>::value) {
      returned_value = std::stoi(data_match[2]);
    } else if (boost::is_same<T, short>::value) {
      int temp = std::stoi(data_match[2]);
      if (temp < SHRT_MIN || temp > SHRT_MAX) {
	ROS_ERROR_STREAM("harmonic_servo::parseResponse expected a short. got " << temp);
	return false;
      } else {
	// We've already checked that this will fit
	returned_value = temp;
      }
    } else if (boost::is_same<T,unsigned long>::value) {
      // stoul wraps, so create a temp value and check for negative first.
      long long temp = std::stoll(data_match[2]);
      if (temp < 0) {
	ROS_ERROR_STREAM("harmonic_servo::parseResponse expected non-negative input. got " << temp);
	return false;
      } else if (temp >= (long long) 1<<32) {
	ROS_ERROR_STREAM("harmonic_servo::parseResponse expected uint32_t input. got " << temp);
	return false;
      }
      returned_value = std::stoul(data_match[2]);
    } else {
      ROS_ERROR_STREAM("harmonic_servo::parseResponse encountered unsupported template type");
      return false;
    }
  } catch (const std::invalid_argument& ex) {
    // Thrown if no conversion could be performed
    ROS_ERROR_STREAM("Harmonic Servo driver unable to parse response: " << data_string
		     << "Exception is: " << ex.what());
    return false;
  } catch (const std::out_of_range& ex) {
    // Thrown if input is out of int's range
    ROS_ERROR_STREAM("Harmonic Servo driver unable to parse response: " << data_string
		     << "Exception is: " << ex.what());
    return false;
  }

  if (data_match[1] == 'v') {
    *output = returned_value;
    return true;
  }
  else if (data_match[1] == 'e') {
    // try to print more-informative error message
    if (copley_error_codes.count(returned_value) > 0) {
      ROS_ERROR_STREAM("Harmonic Servo reported error: "
		       << copley_error_codes.at(returned_value));
    } else {
      ROS_ERROR_STREAM("Harmonic servo reported error with unrecognized code: "
		       << data_string);
    }
    return false;
  }

  // All other cases get the generic error message again.
  //  ROS_ERROR_STREAM("Could not parse Servo response: " << data_string << "huh?");
  return false;
}

  template bool parseResponse<int>(std::vector<uint8_t> raw_data, int* output);
  template bool parseResponse<short>(std::vector<uint8_t> raw_data, short* output);
  template bool parseResponse<unsigned long>(std::vector<uint8_t> raw_data, unsigned long* output);

}  // namespace harmonic_servo
