# Harmonic Servo Driver (rev2)
Rev2 of the Sentry Harmonic Servo Driver. This iteration includes:
- Indexing parameters set at installation time
- A single boost msm state machine for all servo transitions
- User defined software stops to avoid hitting the mechanical hard stops
- The ability to "remember" the home position in all circumstances

## Installing a new HD Servo
1. Plug the servo in, run the driver, and verify servo wrap is close to one of the Sentry mechanical hard stops. Servo should wrap as close as possible to the hard stop, the driver does not currently handle wraps so they must be outside the software limits. Note that this is much easier to verify with a benchtest fixture, but may be done at sea if necessary.
2. Set the servo to coast either via service call or Sentrysitter GUI and record encoder counts at center
3. Mark the encoder count value down in the "encoder_center" param of the yaml config file.
4. Repeat this process for the "fins all the way up" and "fins all the way down" positions. Record the observed values as described below:  

   Fwd_Servo:  
   - min_goal_counts: fins all the way up (should be close to 0)  
   - max_goal_counts: fins all the way down (should be close to 4096)  
   - encoder_center: fins level  
   
   Aft_Servo:  
   - min_goal_counts: fins all the way down (should be close to 0)  
   - max_goal_counts: fins all the way up (should be close to 4096)  
   - encoder_center: fins level  
   
5. Once these values have been entered, rebuild the workspace using `catkin_make`
6. Re-run the harmonic servo driver and test these new values using the "goto_index" service call.

*Note: You will need to manually publish a 'sentry/abort' message with enable set to True if running the harmonic servo driver standalone*

The harmonic servo is now installed and ready to be used on the vehicle. These soft stop and encoder center params should not need to be changed again unless there is damage to the servo encoder or a new servo is being installed.

## Changes to predive
Since the fins level position is set at installation, the new predive procedure involves "homing" the servos. This can be accomplished in Sentrysitter or with the "goto_index" service call, which should send the servos to the encoder_center position. Success is considered to be a new servo position within 15 counts of the encoder_center position. This definition of success can be modified by changing the "wrap_margin" param in the yaml config file.

## Normal Operation
1. Run the driver using roslaunch or `sentry_config` setup (make sure configs are setup appropriately, see above sections)
   1. Set the servos to coast: `rosservice call /sentry/actuators/servo_(fwd|aft)/coast_cmd "coast: true"`
   2. Set the servos to brake: `rosservice call /sentry/actuators/servo_(fwd|aft)/coast_cmd "coast: false"`
   3. Set the servos to home: `rosservice call /sentry/actuators/servo_(fwd|aft)/index_cmd "{}"`
2. To run the servos: Either start publishing `/sentry/actuators/servo_(fwd|aft)/cmd` messages or play an `actuators` bagfile to replay these messages.
3. *Note that the 'sentry/abort' topic will need to also be publishing an enable=True message*

## Troubleshooting

More info on servo operation and common troubleshooting steps can be found [here](https://docs.google.com/presentation/d/1igtn88SAIjvtBEtXzJgmAgVRr2rrRRTWMIUaQ4XFusY/edit?usp=sharing). 
Longform link: https://docs.google.com/presentation/d/1igtn88SAIjvtBEtXzJgmAgVRr2rrRRTWMIUaQ4XFusY/edit?usp=sharing

* If something fails and kills the driver, it is likely a logic issue with the state machine. The state machine is very specific about servo state transitions and will error out if used in a non-standard way. Most state machine transitions should be covered, but there may still be corner cases out there. Please contact ivandor@whoi.edu if you encounter a segfault.
* If the servo is not moving to the correct positions, it is possible the numbers entered into the config files are incorrect. Check values for min_goal_counts, max_goal_counts and encoder_center. These values get used to calculate encoder_center and counts_per_radians which then get used to translate counts to radians and radians to counts. An error in these params can easily propagate downstream to the servo positions being off.

For any other issues, contact I. Vandor at ivandor@whoi.edu