/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHAjNTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on 8 October 2019
//

#include <gtest/gtest.h>
#include <harmonic_servo/servo_parsers.h>

// These tests test the base utility functions for parsing, without requiring
// any of the ROS machinery.
// (Thus, they can be run as a simple gtest.)
class ServoParsingTest : public ::testing::Test {

protected:
  virtual void setUp() {

  }

};

// Test parsing ints
TEST_F(ServoParsingTest, test_value_good) {
  std::vector<int> values = {-1000, -1, 0, 1, 1000, 1<<30};
  for (const auto& value : values) {
    std::string input = "v " + std::to_string(value) + "\r";
    auto raw_input = std::vector<uint8_t>(input.begin(), input.end());

    int output;
    bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
    EXPECT_TRUE(succeeded);
    EXPECT_EQ(value, output);
  }
}

// Test parsing with invalid inputs
TEST_F(ServoParsingTest, test_value_invalid) {
  std::vector<std::string> bad_input = {"\r", "v \r", "v 123abc\r", "v"};
  for (const auto& input : bad_input) {
    auto raw_input = std::vector<uint8_t>(input.begin(), input.end());
    int output;
    bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
    EXPECT_FALSE(succeeded);
  }
}

// Test parsing error responses
// First two are valid; third is invalid index; rest are improperly constructed
TEST_F(ServoParsingTest, test_value_error) {
  std::vector<std::string> bad_input = {"e 1\r", "e 22\r", "e 49\r", "e a\r", "e", "e \r"};
  for (const auto& input : bad_input) {
    auto raw_input = std::vector<uint8_t>(input.begin(), input.end());
    int output;
    bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
    EXPECT_FALSE(succeeded);
  }
}

// Test templated parsing function with nominal it input
TEST_F(ServoParsingTest, test_templated_1) {
  int output;
  std::string input = "v 100\r";
  auto raw_input = std::vector<uint8_t>(input.begin(), input.end());
  bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
  EXPECT_TRUE(succeeded);
  EXPECT_EQ(100, output);
}

// Unsigned ints should generate an error if input is negative
TEST_F(ServoParsingTest, test_templated_2) {
  unsigned long output;
  std::string input = "v -100\r";
  auto raw_input = std::vector<uint8_t>(input.begin(), input.end());
  bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
  EXPECT_FALSE(succeeded);
  std::cout << "test_templated_2, output: " << output << std::endl;
}

// int32s should error if input is 2**32-1
TEST_F(ServoParsingTest, test_templated_3) {
  int output;
  std::string input = "v 4294967295\r";
  auto raw_input = std::vector<uint8_t>(input.begin(), input.end());
  bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
  EXPECT_FALSE(succeeded);
}

// but uints should be fine with 2**32 - 1
TEST_F(ServoParsingTest, test_templated_4) {
  unsigned long output;
  std::string input = "v 4294967295\r";
  auto raw_input = std::vector<uint8_t>(input.begin(), input.end());
  bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
  EXPECT_TRUE(succeeded);
  EXPECT_EQ(4294967295, output);
}

TEST_F(ServoParsingTest, test_short_good) {
  std::vector<short> values = {-1000, -1, 0, 1, 1000, 1<<14};
  for (const auto& value : values) {
    std::string input = "v " + std::to_string(value) + "\r";
    auto raw_input = std::vector<uint8_t>(input.begin(), input.end());
    short output;
    bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
    EXPECT_TRUE(succeeded);
    EXPECT_EQ(value, output);
  }
}

TEST_F(ServoParsingTest, test_short_out_of_range) {
  std::vector<int> values = {1<<15, 1 - (1<<16)};
  for (const auto& value : values) {
    std::string input = "v " + std::to_string(value) + "\r";
    auto raw_input = std::vector<uint8_t>(input.begin(), input.end());
    short output;
    bool succeeded = harmonic_servo::parseResponse(raw_input, &output);
    EXPECT_FALSE(succeeded);
  }
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
