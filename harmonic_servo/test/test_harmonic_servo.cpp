/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHAjNTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by llindzey on 21 March 2019
//

// Tests that exercise the servo driver's parsing of returned ASCII data
// * Direct access of variables
// * checking published status updates

// TODO: This requires a roscore to be running, and will cause
//   `catkin_make run_tests` to hang if one hasn't been started.
// => this means that it needs to be a rostest, not just a gtest.

#include <gtest/gtest.h>
#include <math.h>

#include <ros/ros.h>
#include <rosgraph_msgs/Log.h>
#include <sensor_msgs/JointState.h>

#include <ds_actuator_msgs/ServoState.h>
#include <ds_core_msgs/RawData.h>
#include <ds_core_msgs/Status.h>
#include <harmonic_servo/harmonic_servo.h>
#include <harmonic_servo/servo_parsers.h>
#include <sentry_msgs/HarmonicServoState.h>


// Expose the parsers of the sentry servo class without requiring the
// hardware to be plugged in.
class HarmonicServoUnderTest : public harmonic_servo::HarmonicServo {
  // Required for the google test infrastructure to access
  // protected members.
  // I think the ugliness of this nicely reflects the ugliness
  // of the white-box testing I'm doing here :-\
  // Some of the conversion functions actually need to be members
  // of the HarmonicServo, since they depend on configuration
  // set at run-time, but the parsing probably shouldn't be,
  // especially now that it has been moved to servo_parsers.
  // TODO: Test all "invalid input" using the parseResponse function;
  //       callback-specific tests should be limited to out-of-range
  //       and the like.
  FRIEND_TEST(HarmonicServoTest, test_radians_from_counts);
  FRIEND_TEST(HarmonicServoTest, test_counts_from_radians);
  FRIEND_TEST(HarmonicServoTest, test_publish_state_msgs);
  FRIEND_TEST(HarmonicServoTest, test_parsing_halls_u);
  FRIEND_TEST(HarmonicServoTest, test_parsing_halls_vw);
  FRIEND_TEST(HarmonicServoTest, test_parsing_halls_input_out_of_range);
  FRIEND_TEST(HarmonicServoTest, test_parsing_velocity_good);
  FRIEND_TEST(HarmonicServoTest, test_parsing_velocity_nonsense);
  FRIEND_TEST(HarmonicServoTest, test_parsing_input_voltage_good);
  FRIEND_TEST(HarmonicServoTest, test_parsing_input_voltage_out_of_range);
  FRIEND_TEST(HarmonicServoTest, test_parsing_position_good);
  FRIEND_TEST(HarmonicServoTest, test_parsing_position_out_of_range);
  FRIEND_TEST(HarmonicServoTest, test_parsing_position_nonsense);
  FRIEND_TEST(HarmonicServoTest, test_parsing_motor_current_good);
  FRIEND_TEST(HarmonicServoTest, test_parsing_motor_current_out_of_range);
  FRIEND_TEST(HarmonicServoTest, test_parsing_event_status);
  FRIEND_TEST(HarmonicServoTest, test_parsing_fault_status);
  FRIEND_TEST(HarmonicServoTest, test_parsing_trajectory_status);
  FRIEND_TEST(HarmonicServoTest, test_parsing_encoder_status_1);
  FRIEND_TEST(HarmonicServoTest, test_parsing_encoder_status_2);
  FRIEND_TEST(HarmonicServoTest, test_parsing_encoder_status_3);

public:
  // DO NOT override init(), since it expects setupParameters et al to be called
  // (Which requires the serial port to be available for the tests to pass)
  // TODO(llindzey): I followed the pattern in other nodes, but there's got
  //    to be a cleaner way that allows pulling params from the config file.
  virtual void init_test() {
    // Set the params here that are normally specified in a launch file.
    config_.encoder_center = 2047;
    config_.encoder_counts = 4096;
    config_.counts_per_radian = config_.encoder_counts / (4*M_PI/3);
    config_.encoder_fin_direction = 1.0;
    iosm_timeout_ = ros::Duration(0.5);

    // Purposely skip setupParameters / setupConnections
    // setupConnections(); // Requires device to exist ...Ugh.
    setupPublishers(); // Needed for status output,
    setupSubscriptions();
    setupServices();
    setupTimers();
  };
};

// Utility class for monitoring the servo's published status
class StatusSubscriber {

public:
  StatusSubscriber() {}

  void callback(const ds_core_msgs::Status& incoming_msg) {

    // Can't filter on descriptive_name because that's set in setupParameters,
    // which isn't called for these tests.
    //    std::cout << "got status message! node_name: " << node_name << ", descriptive_name: " << incoming_msg.descriptive_name << std::endl;
    //    if(incoming_msg.descriptive_name == node_name)
    msg = incoming_msg;

  }

  void setupSubscribers(ros::NodeHandle nh, std::string name){
    node_name = name;
    sub = nh.subscribe(node_name + "/status", 1, &StatusSubscriber::callback, this);

  }
  ros::Subscriber sub;
  ds_core_msgs::Status msg;
  std::string node_name;
};

// Utility class for checking whether the servo has published errors
// to rosout.
class RosoutSubscriber {

public:
  RosoutSubscriber() : error_count(0) {};

  void callback(const rosgraph_msgs::Log& msg) {
    if(msg.level == msg.ERROR && msg.name == node_name) {
      error_count += 1;
    }
  };

  void setupSubscribers(ros::NodeHandle nh, const std::string& name) {
    sub = nh.subscribe("/rosout", 1, &RosoutSubscriber::callback, this);
    node_name = name;
  }

  void reset() {
    error_count = 0;
  }

  ros::Subscriber sub;
  std::string node_name = "/harmonic_servo_test1";
  int error_count;
};

class ServoSubscriber {
public:
  ServoSubscriber() : joint_msg_(), state_msg_(), sentry_servo_msg_() {}

  void jointCallback(const sensor_msgs::JointState& msg) {
    joint_msg_ = msg;
  }

  void stateCallback(const ds_actuator_msgs::ServoState& msg) {
    state_msg_ = msg;
  }

  void servoStateCallback(const sentry_msgs::HarmonicServoState& msg) {
    sentry_servo_msg_ = msg;
  }


  void setupSubscribers(ros::NodeHandle nh, const std::string& topic) {
    joint_sub_ = nh.subscribe(topic + "/joint_states", 1, &ServoSubscriber::jointCallback, this);
    state_sub_ = nh.subscribe(topic + "/state", 1, &ServoSubscriber::stateCallback, this);
    servo_state_sub_ = nh.subscribe(topic + "/sentry_servo_state", 1, &ServoSubscriber::servoStateCallback, this);
  }

  ros::Subscriber joint_sub_;
  ros::Subscriber state_sub_;
  ros::Subscriber servo_state_sub_;

  sensor_msgs::JointState joint_msg_;
  ds_actuator_msgs::ServoState state_msg_;
  sentry_msgs::HarmonicServoState sentry_servo_msg_;
};


class HarmonicServoTest : public ::testing::Test {

protected:
  virtual void SetUp() {
    dut.init_test();
    ros::NodeHandle nh;

    std::string node_name = "/harmonic_servo_test1";
    rs.setupSubscribers(nh, node_name);
    ss.setupSubscribers(nh, node_name);
    status_subscriber.setupSubscribers(nh, node_name);

    abort_pub = nh.advertise<ds_core_msgs::Abort>("/sentry/abort", 1);
  }

  ds_core_msgs::RawData makeMessage(std::string input) {
    ds_core_msgs::RawData msg;
    msg.data = std::vector<uint8_t>(input.begin(), input.end());
    return msg;
  }

  HarmonicServoUnderTest dut;
  ros::Subscriber rosout_sub;
  RosoutSubscriber rs;
  ServoSubscriber ss;
  StatusSubscriber status_subscriber;
  ros::Publisher abort_pub;

};

///////////////////////
// Testing that the servo responds to enable/disable correctly
// TODO: The goals of this test are not well suited to this architecture.
//   It really should be handled by setting up a fake serial connection + interfacing with sentry via messages. I had avoided that becasue it seemed a complicated overkill to try to implement both sides of that and coordinate them for tests, but it may be necessary ...
/**
TEST_F(HarmonicServoTest, test_status_transitions) {
  ds_core_msgs::Status status_msg;
  ds_core_msgs::Abort abort_msg;
  abort_msg.abort = false;
  ros::TimerEvent ee;

  // Send first enable message, and give it time to propagate.
  // TODO: These tests are failing because this message is never received by the servo class. Oh. I bet the issue is that DsProcess doesn't cycle ITS callback queue in response to a SpinOnce. So ... for now, this can directly call handleEnable, but that's way too much mucking about in the internals. Better to just spin it up as a separate node and test this behavior purely externally.
  // This is required so that the publisher is registered and the first message is actually recieved. Otherwise, only the following ones go through. (Based on running `rostopic echo /sentry/abort` before starting the tests)
  ros::Duration(2.0).sleep();

  abort_msg.enable = true;
  abort_msg.stamp = ros::Time::now();
  abort_pub.publish(abort_msg); // Hah. Doesn't actually work.
  dut.handleEnable(abort_msg);

  ros::Duration(0.1).sleep();
  ros::spinOnce();

  // at startup, should be error
  dut.checkProcessStatus(ee);
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  EXPECT_EQ(status_msg.STATUS_ERROR, status_subscriber.msg.status);

  // after first position data received, should be good
  dut.positionCallback(makeMessage("v 4000\r"));
  dut.checkProcessStatus(ee);
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  EXPECT_EQ(status_msg.STATUS_GOOD, status_subscriber.msg.status);

  // after disable, should be back to warn
  dut.positionCallback(makeMessage("v 4000\r"));
  abort_msg.enable = false;
  abort_msg.stamp = ros::Time::now();
  abort_pub.publish(abort_msg);
  dut.handleEnable(abort_msg);
  ros::Duration(0.1).sleep();
  ros::spinOnce();

  dut.checkProcessStatus(ee);
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  EXPECT_EQ(status_msg.STATUS_WARN, status_msg.status);

  // after enable, back to good
  dut.positionCallback(makeMessage("v 4000\r"));
  abort_msg.enable = true;
  abort_msg.stamp = ros::Time::now();
  abort_pub.publish(abort_msg);
  dut.handleEnable(abort_msg);
  ros::Duration(0.1).sleep();
  ros::spinOnce();

  dut.checkProcessStatus(ee);
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  EXPECT_EQ(status_msg.STATUS_WARN, status_msg.status);
}

TEST_F(HarmonicServoTest, test_status_position_timeout) {
  ds_core_msgs::Status status_msg;
  ros::TimerEvent ee;

  // Send first enable message
  ds_core_msgs::Abort abort_msg;
  abort_msg.abort = false;
  abort_msg.enable = true;
  abort_pub.publish(abort_msg);

  // at startup, should be error b/c no feedback received yet
  dut.checkProcessStatus(ee);
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  EXPECT_EQ(status_msg.STATUS_ERROR, status_subscriber.msg.status);

  // after first position data received, should be good
  dut.positionCallback(makeMessage("v 4000\r"));
  dut.checkProcessStatus(ee);
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  EXPECT_EQ(status_msg.STATUS_GOOD, status_subscriber.msg.status);


  // after not having received data from the servo, should be error
  dut.position_timeout_.sleep();
  dut.checkProcessStatus(ee);
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  EXPECT_EQ(status_msg.STATUS_ERROR, status_msg.status);

  // and, after receiving data from servo, back to good
  dut.positionCallback(makeMessage("v 4000\r"));
  dut.checkProcessStatus(ee);
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  EXPECT_EQ(status_msg.STATUS_GOOD, status_msg.status);
}

**/


///////////////////////
// Testing conversions

TEST_F(HarmonicServoTest, test_radians_from_counts) {
  EXPECT_FLOAT_EQ(0, dut.radiansFromCounts(dut.config_.encoder_center));
  EXPECT_FLOAT_EQ(2*M_PI/3, dut.radiansFromCounts(dut.config_.encoder_center + 0.5*dut.config_.encoder_counts));
  EXPECT_FLOAT_EQ(-2*M_PI/3, dut.radiansFromCounts(dut.config_.encoder_center - 0.5*dut.config_.encoder_counts));
}


TEST_F(HarmonicServoTest, test_counts_from_radians) {
  EXPECT_FLOAT_EQ(2047, dut.countsFromRadians(0));
  EXPECT_FLOAT_EQ(dut.config_.encoder_center, dut.countsFromRadians(0));
}


///////////////////////
// Testing that expected publications actually happen
TEST_F(HarmonicServoTest, test_publish_state_msgs) {
  // Call callbacks ... order doesn't really matter, aside from position coming last.

  int voltage_counts = 365;
  dut.inputVoltageCallback(makeMessage("v " + std::to_string(voltage_counts) + "\r"));
  float voltage_volts = voltage_counts / 10.0;

  int current_counts = 321;
  dut.motorCurrentCallback(makeMessage("v " + std::to_string(current_counts) + "\r"));
  float current_amps = current_counts / 100.0;

  dut.hallStateCallback(makeMessage("v 6\r"));

  dut.encoderStatusCallback(makeMessage("v 0\r"));
  dut.faultStatusCallback(makeMessage("v 0\r"));
  dut.eventStatusCallback(makeMessage("v 0\r"));
  dut.trajectoryStatusCallback(makeMessage("v 0\r"));

  int velocity_counts = 2000;
  dut.velocityCallback(makeMessage("v " + std::to_string(velocity_counts) + "\r"));
  float velocity_radians = (4.0/3) * M_PI * 0.1 * velocity_counts / dut.config_.encoder_counts;

  int position_counts = 3000;
  dut.positionCallback(makeMessage("v " + std::to_string(position_counts) + "\r"));
  float position_radians = (4.0/3) * M_PI*(position_counts - dut.config_.encoder_center) / dut.config_.encoder_counts;

  // Time for the callbacks to process
  ros::Duration(0.1).sleep();
  ros::spinOnce();
  // NB: This will segfault rather than fail if the joint_msg_
  //     hasn't been received yet.
  EXPECT_FLOAT_EQ(position_radians, ss.joint_msg_.position[0]);
  EXPECT_FLOAT_EQ(position_radians, ss.state_msg_.actual_radians);
  EXPECT_FLOAT_EQ(velocity_radians, ss.sentry_servo_msg_.actual_velocity);
  EXPECT_FLOAT_EQ(voltage_volts, ss.sentry_servo_msg_.input_voltage);
  EXPECT_FLOAT_EQ(current_amps, ss.sentry_servo_msg_.motor_current);
  EXPECT_FALSE(ss.sentry_servo_msg_.halls.u);
  EXPECT_TRUE(ss.sentry_servo_msg_.halls.v);
  EXPECT_TRUE(ss.sentry_servo_msg_.halls.w);
}


/////////////////
// Test that the callbacks generate the correct messages (status and error)

TEST_F(HarmonicServoTest, test_parsing_halls_u) {
  // assumes that the bits are u,v,w LSB->MSB.
  auto msg = makeMessage("v 1\r");
  dut.hallStateCallback(msg);
  EXPECT_EQ(1, dut.feedback_.hall_states);
  EXPECT_TRUE(dut.feedback_.hall_msg.u);
  EXPECT_FALSE(dut.feedback_.hall_msg.v);
  EXPECT_FALSE(dut.feedback_.hall_msg.w);
}

TEST_F(HarmonicServoTest, test_parsing_halls_vw) {
  // assumes that the bits are u,v,w LSB->MSB.
  auto msg = makeMessage("v 6\r");
  dut.hallStateCallback(msg);
  EXPECT_EQ(6, dut.feedback_.hall_states);
  EXPECT_FALSE(dut.feedback_.hall_msg.u);
  EXPECT_TRUE(dut.feedback_.hall_msg.v);
  EXPECT_TRUE(dut.feedback_.hall_msg.w);
}

TEST_F(HarmonicServoTest, test_parsing_halls_input_out_of_range) {
  std::vector<std::string> bad_input = {{"v 8\r"}, {"v -1\r"}, {"v 5000000000\r"}};
  for(const auto& cmd : bad_input) {
    auto msg = makeMessage(cmd);
    dut.hallStateCallback(msg);
    ros::Duration(0.1).sleep();
    ros::spinOnce();
    EXPECT_FALSE(dut.feedback_.hall_msg.u);
    EXPECT_FALSE(dut.feedback_.hall_msg.v);
    EXPECT_FALSE(dut.feedback_.hall_msg.w);
    EXPECT_EQ(1, rs.error_count);
    rs.reset();
  }
}

TEST_F(HarmonicServoTest, test_parsing_velocity_good) {
  std::vector<int> input = {-100, 0, 100};
  for(const auto& value : input) {
    std::string cmd = "v " + std::to_string(value) + "\r";
    auto msg = makeMessage(cmd);
    dut.velocityCallback(msg);
    EXPECT_EQ(value, dut.feedback_.velocity_counts);
  }
}

TEST_F(HarmonicServoTest, test_parsing_velocity_nonsense) {
  std::vector<std::string> bad_input = {"\r", "e 1\r", "v \r", "v 123abc\r", "e a\r"};
  for(const auto& cmd : bad_input) {
    auto msg = makeMessage(cmd);
    dut.velocityCallback(msg);
    ros::Duration(0.1).sleep();
    ros::spinOnce();
    EXPECT_EQ(1, rs.error_count);
    rs.reset();
  }
}

TEST_F(HarmonicServoTest, test_parsing_input_voltage_good) {
  std::vector<int> input = {200, 432, 480, 600};
  for(const auto& value : input) {
    std::string cmd = "v " + std::to_string(value) + "\r";
    auto msg = makeMessage(cmd);
    dut.inputVoltageCallback(msg);
    EXPECT_EQ(value, dut.feedback_.input_voltage);
  }
}

TEST_F(HarmonicServoTest, test_parsing_input_voltage_out_of_range) {
  std::vector<int> input = {-200, 0, 1000};
  for(const auto& value : input) {
    std::string cmd = "v " + std::to_string(value) + "\r";
    auto msg = makeMessage(cmd);
    dut.inputVoltageCallback(msg);
    ros::Duration(0.1).sleep();
    ros::spinOnce();
    EXPECT_EQ(1, rs.error_count);
    rs.reset();
  }
}

TEST_F(HarmonicServoTest, test_parsing_position_good) {
  std::vector<int> input = {4095, 2047, 0};
  for(const auto& value : input) {
    std::string cmd = "v " + std::to_string(value) + "\r";
    auto msg = makeMessage(cmd);
    dut.positionCallback(msg);
    EXPECT_EQ(value, dut.feedback_.position_counts);;
  }
}

TEST_F(HarmonicServoTest, test_parsing_position_out_of_range) {
  // Test how it handles a value larger than standard int
  // NOTE: See comment in code about how it does NOT handle
  //       input so long it overflows an int32.
  std::vector<long long int> input = {-1, 4096, 5000000000};
  int init_pos = dut.feedback_.position_counts;
  for(const auto& value : input) {
    std::string cmd = "v " + std::to_string(value) + "\r";
    auto msg = makeMessage(cmd);
    dut.positionCallback(msg);
    ros::Duration(0.1).sleep();
    ros::spinOnce();
    EXPECT_EQ(1, rs.error_count);
    EXPECT_EQ(init_pos, dut.feedback_.position_counts);
    rs.reset();
  }
}

TEST_F(HarmonicServoTest, test_parsing_position_nonsense) {
  int init_pos = dut.feedback_.position_counts;
  std::vector<std::string> bad_input = {"v \r", "", "asdf", "v asdf\r", "v", "v 123abc\r"};
  for(const auto& cmd : bad_input) {
    auto msg = makeMessage(cmd);
    dut.positionCallback(msg);
    ros::Duration(0.1).sleep();
    ros::spinOnce();
    EXPECT_EQ(init_pos, dut.feedback_.position_counts);
    EXPECT_EQ(1, rs.error_count);
    rs.reset();
  }
}

TEST_F(HarmonicServoTest, test_parsing_motor_current_good) {
  std::vector<int> input = {0, 100, 1000}; // units of 0.01 A
  for(const auto& value : input) {
    std::string cmd = "v " + std::to_string(value) + "\r";
    auto msg = makeMessage(cmd);
    dut.motorCurrentCallback(msg);
    EXPECT_EQ(value, dut.feedback_.motor_current);
  }
}

/**
// Removing this test because hardware testing showed that reported
// current can vary significantly from commanded, and I haven't characterized
// it well enough to usefully detect bad data (indicating need for re-sync).
TEST_F(HarmonicServoTest, test_parsing_motor_current_out_of_range) {
  std::vector<int> input = {-200, 10000};
  for(const auto& value : input) {
    std::string cmd = "v " + std::to_string(value) + "\r";
    auto msg = makeMessage(cmd);
    dut.motorCurrentCallback(msg);
    ros::Duration(0.1).sleep();
    ros::spinOnce();
    EXPECT_EQ(1, rs.error_count);
    rs.reset();
  }
}
**/

TEST_F(HarmonicServoTest, test_parsing_event_status) {

  int status = 0;
  status += 1<<11;  // enable_not_active
  status += 1<<25;  // tracking window

  std::string cmd = "v " + std::to_string(status) + "\r";
  auto msg = makeMessage(cmd);
  dut.eventStatusCallback(msg);
  EXPECT_EQ(status, dut.feedback_.event_status);
  EXPECT_TRUE(dut.feedback_.event_status_msg.enable_not_active);
  EXPECT_TRUE(dut.feedback_.event_status_msg.tracking_window);
  EXPECT_FALSE(dut.feedback_.event_status_msg.software_disable);
  EXPECT_FALSE(dut.feedback_.event_status_msg.acceleration_limit);
}

TEST_F(HarmonicServoTest, test_parsing_fault_status) {

  int status = 0;
  status += 1<<7;  // feedback fault
  status += 1<<9;  // tracking error

  std::string cmd = "v " + std::to_string(status) + "\r";
  auto msg = makeMessage(cmd);
  dut.faultStatusCallback(msg);
  EXPECT_EQ(status, dut.feedback_.fault_status);
  EXPECT_TRUE(dut.feedback_.fault_status_msg.feedback_fault);
  EXPECT_TRUE(dut.feedback_.fault_status_msg.tracking_error);
  EXPECT_FALSE(dut.feedback_.fault_status_msg.phasing_error);
  EXPECT_FALSE(dut.feedback_.fault_status_msg.current_limited);
}

TEST_F(HarmonicServoTest, test_parsing_trajectory_status) {

  int status = 0;
  status += 1<<12; // referenced
  status += 1<<15; // in motion

  short sstatus = status; // implementation defined, but appears to truncate
  std::string cmd = "v " + std::to_string(sstatus) + "\r";
  auto msg = makeMessage(cmd);
  dut.trajectoryStatusCallback(msg);
  EXPECT_EQ((uint16_t) sstatus, dut.feedback_.trajectory_status);
  EXPECT_TRUE(dut.feedback_.trajectory_status_msg.referenced);
  EXPECT_TRUE(dut.feedback_.trajectory_status_msg.in_motion);
  EXPECT_FALSE(dut.feedback_.trajectory_status_msg.move_aborted);
  EXPECT_FALSE(dut.feedback_.trajectory_status_msg.homing);
  EXPECT_FALSE(dut.feedback_.trajectory_status_msg.homing_error);
}

TEST_F(HarmonicServoTest, test_parsing_encoder_status_1) {
  int status = 0;
  status += 1<<15; // invalid!
  std::string cmd = "v " + std::to_string(status) + "\r";
  auto msg = makeMessage(cmd);
  dut.encoderStatusCallback(msg);
  EXPECT_EQ(status, dut.feedback_.encoder_status);
  EXPECT_FALSE(dut.feedback_.encoder_data_valid);
}

TEST_F(HarmonicServoTest, test_parsing_encoder_status_2) {
  int status = 0;
  std::string cmd = "v " + std::to_string(status) + "\r";
  auto msg = makeMessage(cmd);
  dut.encoderStatusCallback(msg);
  EXPECT_EQ(status, dut.feedback_.encoder_status);
  EXPECT_TRUE(dut.feedback_.encoder_data_valid);
}

// Test that out-of-range value is detected
TEST_F(HarmonicServoTest, test_parsing_encoder_status_3) {
  long long status = 8589934592;
  std::string cmd = "v " + std::to_string(status) + "\r";
  auto msg = makeMessage(cmd);
  dut.encoderStatusCallback(msg);
  ros::Duration(0.2).sleep();
  ros::spinOnce();
  EXPECT_EQ(1, rs.error_count);
  rs.reset();
  EXPECT_FALSE(dut.feedback_.encoder_data_valid);
}


int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "test_harmonic_servo");
  ros::NodeHandle nh;
  return RUN_ALL_TESTS();
}
