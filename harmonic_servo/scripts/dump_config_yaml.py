#! /usr/bin/env python

from __future__ import print_function

import argparse
import numpy as np
import serial
import string
import time

def dump_config(port, baud, filename):
    with open(filename, 'w') as fp, serial.Serial(port=port, baudrate=baud, parity='N', stopbits=1, bytesize=8) as ser:

        for addr in range(0x1b0):
            cmd = 'g f {}\r'.format(hex(addr))
            print(cmd)
            ser.write(cmd)
            resp = ser.read_until('\r')
            # When querying a string parameter, the controller will
            # return the null-terminated string, so strip that off
            # along with the line termination character.
            resp = resp.strip('\0\r')
            if resp.startswith('e'):
                print("Query {} -> {}".format(cmd.replace('\r', ''), resp))
            elif resp.startswith('v'):
                val = resp.split(' ', 1)[1]
                if not all(ch in string.printable for ch in val):
                    print("Not all values are printable: {}".format(val))
                    print([ord(ch) for ch in val])
                else:
                    # Check that it is a settable parameter. Otherwise, we don't
                    # want it in these parameter dumps.
                    set_str = "s f {} {}\r".format(hex(addr), val)
                    ser.write(set_str)
                    set_resp = ser.read_until('\r')
                    if set_resp.startswith('ok'):
                        fp.write('{}: {}\n'.format(hex(addr), val))
                    else:
                        print("Set {} -> {}".format(set_str.replace('\r', ''), set_resp))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('port', help="serial port the servo is connected to")
    args = parser.parse_args()
    dumpfile = "param_dump_{}.yaml".format(time.time())
    dump_config(args.port, 9600, dumpfile)
