#! /usr/bin/env python

import serial
import time

def main(port):
    ser = serial.Serial(port, baudrate=9600)
    status_cmds = ["0x12f", "0xc9", "0xa4", "0x17"]

    states = [3, 2, 0, 1, 0]
    #delays = [5, 5, 5, 5, 5] # we know this worked
    delays = [1, 2, 2, 1, 5] # starting guess...

    ser.write("s r 0x24 0\r")
    ser.read_until("\r")

    for state, delay in zip(states, delays):
        t0 = time.time()
        print "====================="
        print "Sending: {}".format(state)
        ser.write("s r 0xab {}\r".format(state))
        ser.read_until("\r")

        while delay > time.time() - t0:
            for cmd in status_cmds:
                ser.write("g r {}\r".format(cmd))
                resp = ser.read_until("\r")
                dt = time.time() - t0
                print "{:2f}: {} -> {}".format(dt, cmd, resp)
            print ""

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("port")
    args = parser.parse_args()
    main(args.port)


# After sending 3:
# * 0x12f was 32768 once
# * 0xc9 always 4097
# * 0x17 mostly 0, once 4095
# * rest were 0

# After sending 2:
# * 0x12f was 32768 ONCE
# * 0xc9 always 4097
# * 0x17 0, 6, 5, 11, 16, 27, 33 ....stopped increasing at 47

# After sending 0
# * -x12f always 0
# * 0xc9 always 4097
# * 0x17 always 47/48

# After sending 1
# * 0x12f 32768 ONCE
# * 0x17 always 4095

# After sending 0
# * -x12f 32768 ONCE
# * 0x17 steadily decreasing
