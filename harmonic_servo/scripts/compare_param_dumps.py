#! /usr/bin/env python

from __future__ import print_function

import argparse
import yaml

def compare(filename1, filename2):
    with open(filename1, 'r') as fp1, open(filename2, 'r') as fp2:
        config1 = yaml.safe_load(fp1)
        config2 = yaml.safe_load(fp2)

        print(config1)
        print(config2)

        if config1 == config2:
            print("Configurations are the same")
        else:
            # Try to print out where they differ...
            print("Configurations differ:")
            keys = set(config1.keys()).union(set(config2.keys()))
            for key in keys:
                if key not in config1:
                    print("{} in config2, but not in config1".format(hex(key)))
                elif key not in config2:
                    print("{} in config1, but not in config2".format(hex(key)))
                elif config1[key] != config2[key]:
                    print("{}: {} (config1), {} (config2)".format(hex(key), config1[key], config2[key]))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('config1', help="First config file for comparison")
    parser.add_argument('config2', help="Second config file ...")
    args = parser.parse_args()
    compare(args.config1, args.config2)
