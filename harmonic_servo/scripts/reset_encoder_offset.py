#! /usr/bin/env python

import serial
import time

def main(port):
    ser = serial.Serial(port, baudrate=9600, timeout=0.5)
    # First, set current offset to zero
    ser.write("s f 0xc6 0\r")
    _ = ser.read_until("\r")
    ser.write("r\r")

    # Give it time to reboot....
    print "waiting for controller to reboot..."
    resp = None
    while resp != "v 15\r":
        print "sleeping..."
        time.sleep(1)
        ser.write("g r 0xc2\r")
        resp = ser.read_until("\r")
    print "got response! {}".format(resp.strip("\r"))

    # Find current position
    ser.write("g r 0x17\r")
    resp = ser.read_until("\r")
    pos = resp.split()[1].strip("\r")
    print "At position: {} ... resetting zero".format(pos)

    # Set current position to be controller 0
    cmd = "s f 0xc6 -{}\r".format(pos)
    print "sending: {}".format(cmd.strip("\r"))
    ser.write(cmd)
    _ = ser.read_until("\r")
    ser.write("r\r")

    # Give it time to reboot....
    print "waiting for controller to reboot..."
    resp = None
    while resp != "v 15\r":
        print "sleeping..."
        time.sleep(1)
        ser.write("g r 0xc2\r")
        resp = ser.read_until("\r")
    print "got response! {}".format(resp.strip("\r"))

    print "Waiting for position data"
    ser.write("g r 0x17\r")
    resp = ser.read_until("\r")
    pos = resp.split()[1].strip("\r")
    print "At position: {}".format(pos)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("port")
    args = parser.parse_args()
    main(args.port)



    
    
