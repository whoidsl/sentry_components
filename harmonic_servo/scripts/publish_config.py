#! /usr/bin/env python2.7

import argparse
import yaml

import rospy
import sentry_msgs.msg
import sentry_msgs.srv


def publish_config(servo, filename):
    if servo not in ['aft', 'fwd']:
        print "Invalid choice of servo: {}. Must be aft/fwd".format(servo)
        return

    with open(filename, 'r') as fp:
        config = yaml.safe_load(fp)

    req = sentry_msgs.srv.ServoConfigureCmdRequest()
    for key, val in config.iteritems():
        # There's probably a better yaml-y way to do this, but this works as a
        # quick-and-dirty way to force all values to be of type string.
        msg = sentry_msgs.msg.ServoParameter("{}".format(hex(key)), "{}".format(val))
        req.parameters.append(msg)

    service_name = '/sentry/actuators/servo_{}/config_cmd'.format(servo)
    srv = rospy.ServiceProxy(service_name, sentry_msgs.srv.ServoConfigureCmd)
    resp = srv(req)
    print resp


if __name__ == "__main__":
    rospy.init_node('publish_config')
    parser = argparse.ArgumentParser()
    parser.add_argument('servo', help="which servo to configure. 'aft'|'fwd'")
    parser.add_argument('filename', help="yaml file containing configuration")
    args = parser.parse_args()
    publish_config(args.servo, args.filename)
