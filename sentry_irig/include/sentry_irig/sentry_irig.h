/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 3/25/18.
//

#ifndef SENTRY_IRIG_SENTRY_IRIG_H
#define SENTRY_IRIG_SENTRY_IRIG_H

#include <ds_base/sensor_base.h>
#include <sentry_msgs/IrigState.h>

namespace sentry_irig
{
class IrigBoard : public ds_base::SensorBase
{
public:
  explicit IrigBoard();
  IrigBoard(int argc, char* argv[], const std::string& name);
  ~IrigBoard() override;
  DS_DISABLE_COPY(IrigBoard);

protected:
  void setupParameters() override;
  void setupPublishers() override;

  void checkProcessStatus(const ros::TimerEvent& event) override;

  // TODO: Add service to resync via IRIG

  void parseReceivedBytes(const ds_core_msgs::RawData& bytes);

private:
  ros::Publisher nmea_zda_pub_;
  ros::Publisher state_pub_;

  bool ignore_date_;
  double offset_ms_;
  double expected_offset_ms_;
  double tolerance_warn_;
  double tolerance_error_;
};
}
#endif  // SENTRY_IRIG_SENTRY_IRIG_H
