/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 3/25/18.
//

#include "sentry_irig/sentry_irig.h"

#include <sentry_msgs/IrigState.h>
#include <ds_nmea_msgs/Zda.h>
#include <ds_nmea_parsers/Zda.h>
#include <stdexcept>

namespace sentry_irig
{
IrigBoard::IrigBoard() : ds_base::SensorBase()
{
  tolerance_error_ = 20;
  tolerance_warn_ = 10;
  offset_ms_ = 0;
  ignore_date_ = false;
  expected_offset_ms_ = 0;
}

IrigBoard::IrigBoard(int argc, char* argv[], const std::string& name) : ds_base::SensorBase(argc, argv, name)
{
  tolerance_error_ = 20;
  tolerance_warn_ = 10;
  offset_ms_ = 0;
  ignore_date_ = false;
  expected_offset_ms_ = 0;
}

IrigBoard::~IrigBoard() = default;

void IrigBoard::setupParameters()
{
  SensorBase::setupParameters();

  tolerance_warn_ = ros::param::param<double>("~tolerance_warn_ms", 10.0);
  tolerance_error_ = ros::param::param<double>("~tolerance_error_ms", 20.0);
  ignore_date_ = ros::param::param<bool>("~ignore_date", false);
  expected_offset_ms_ = ros::param::param<double>("~expected_offset_ms", 0.0);

  if (tolerance_warn_ <= 0)
  {
    ROS_WARN_STREAM("IRIG warning tolerance set to " << tolerance_warn_ << "ms, which seems too low!");
  }
  if (tolerance_error_ <= 0)
  {
    ROS_WARN_STREAM("IRIG error tolerance set to " << tolerance_error_ << "ms, which seems too low!");
  }
  if (tolerance_error_ >= tolerance_warn_)
  {
    ROS_WARN_STREAM("IRIG error tolerance (" << tolerance_error_ << " ms) is looser than the warning tolderance ("
                                             << tolerance_warn_ << " ms).  This seems wrong...");
  }
  if (ignore_date_)
  {
    ROS_INFO_STREAM("Ignore date as requested...");
  }
}

void IrigBoard::setupPublishers()
{
  SensorBase::setupPublishers();

  auto nh = nodeHandle();
  nmea_zda_pub_ = nh.advertise<ds_nmea_msgs::Zda>(ros::this_node::getName() + "/nmea_zda", 10, false);
  state_pub_ = nh.advertise<sentry_msgs::IrigState>(ros::this_node::getName() + "/state", 10, false);
}

void IrigBoard::checkProcessStatus(const ros::TimerEvent& event)
{
  auto msg = statusMessage();   // marks status as good
  checkMessageTimestamps(msg);  // standard I/O checks

  // non-standard "are we in bounds" check
  if (abs(offset_ms_ - expected_offset_ms_) >= tolerance_error_)
  {
    msg.status = ds_core_msgs::Status::STATUS_ERROR;
    ROS_ERROR_STREAM("IRIG Board reports differs from CPU clock by " << offset_ms_ << "ms!");
  }
  else if (abs(offset_ms_ - expected_offset_ms_) >= tolerance_warn_)
  {
    msg.status = ds_core_msgs::Status::STATUS_WARN;
    ROS_WARN_STREAM("IRIG Board reports differs from CPU clock by " << offset_ms_ << "ms!");
  }

  publishStatus(msg);
}

void IrigBoard::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  auto ok = false;
  auto msg = ds_nmea_msgs::Zda{};

  auto stream = std::stringstream(std::string{ std::begin(bytes.data), std::end(bytes.data) });
  auto nmea_msg = std::string{};

  while (std::getline(stream, nmea_msg))
  {
    try
    {
      // Find the start of the NMEA message, triming leading characters as required
      auto begin_it = nmea_msg.find('$');
      if (begin_it == std::string::npos)
      {
        continue;
      }
      if (begin_it != 0)
      {
        nmea_msg = nmea_msg.substr(begin_it);
      }

      // Look for a ZDA string
      if (nmea_msg.find("ZDA") != std::string::npos)
      {
        auto msg = ds_nmea_msgs::Zda{};
        if (!ds_nmea_msgs::from_string(msg, nmea_msg))
        {
          ROS_WARN_STREAM("Unable to parse $--ZDA from string: " << nmea_msg);
          continue;
        }

        nmea_zda_pub_.publish(msg);
        updateTimestamp("nmea_zda");

        // update the state message
        ros::Duration diff = msg.utc_time - bytes.ds_header.io_time;
        if (ignore_date_)
        {
          offset_ms_ = fmod(diff.toSec(), 3600 * 24) * 1000.0;
        }
        else
        {
          offset_ms_ = diff.toSec() * 1000.0;
        }

        sentry_msgs::IrigState state_msg;
        state_msg.header = bytes.header;
        state_msg.ds_header = bytes.ds_header;
        state_msg.header.stamp = msg.utc_time;
        state_msg.zda_minus_rostime_ms = offset_ms_;
        state_msg.expected_offset_ms = expected_offset_ms_;

        state_pub_.publish(state_msg);

        // If we have an error, go ahead and report it immediately.
        if (abs(offset_ms_ - expected_offset_ms_) >= tolerance_warn_)
        {
          checkProcessStatus(ros::TimerEvent());
        }
      }  // if ZDA found
    }
    catch (const std::exception& err)
    {
      ROS_ERROR_STREAM("Unable to parse datetime because: " << err.what());
      offset_ms_ = -9999;
      msg.utc_time = ros::Time();
    }
  }  // while getline
}

}  // namespace sentry_irig
