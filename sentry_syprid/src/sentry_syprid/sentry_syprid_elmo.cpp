/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_syprid/sentry_syprid_elmo.h"
#include "sentry_syprid/ElmoState.h"
#include "ds_core_msgs/RawData.h"

namespace sentry_syprid
{

SypridElmo::SypridElmo()
  : ElmoMotorController()
{
}

SypridElmo::SypridElmo(int argc, char** argv, const std::string& name)
  : ElmoMotorController(argc, argv, name)
{
}

SypridElmo::~SypridElmo() = default;

//void SypridElmo::setupCommands()
//{
////  DS_D(ElmoMotorController);
//
//  // Always disable motor output and echo.  Put these at the absolute beginning of the list
//  d->init_commands_.emplace_back("MO=0\r", 0.2, false);
//  d->init_commands_.front().setDelayAfter(ros::Duration(0.2));
//  d->init_commands_.emplace_back("EO=0\r", 0.2, false);
//  d->init_commands_.front().setDelayAfter(ros::Duration(0.2));
//
//  for (auto& cmd : ros::param::param<std::vector<std::string>>("~init_commands", {}))
//  {
//
//    d->init_commands_.emplace_back(cmd, 0.2, false);
//    d->init_commands_.back().setDelayAfter(ros::Duration(0.2));
//  }
//
//  for (auto& cmd : ros::param::param<std::vector<std::string>>("~poll_commands", {}))
//  {
//    const auto io_command = ds_asio::IoCommand{
//      cmd, 0.2, false,
//    };
//    d->poll_commands_.emplace_back(std::make_pair(0, std::move(io_command)));
//    d->poll_commands_.back().second.setDelayAfter(ros::Duration(0.2));
//  }
//}
//
//void SypridElmo::setup()
//{
//  DsProcess::setup();
//  ds_elmo::ElmoMotorController::setupCommands();

//  DS_D(ElmoMotorController);
//
//  // Now add the init commands as preempt commands
//  for (auto& io_command : d->init_commands_)
//  {
//    ROS_ERROR_STREAM("Command read from params: " << io_command.getCommand());
//    d->elmo_sm_->addPreemptCommand(io_command);
//    ROS_ERROR_STREAM("Added init command: " << io_command.getCommand());
//  }
//
//  // And the polled commands as regular commands.  Save the command
//  // id after adding.
//  for (auto& poll_command : d->poll_commands_)
//  {
//    poll_command.second = d->wrap_command_for_logging(std::move(poll_command.second));
//    poll_command.first = d->elmo_sm_->addRegularCommand(poll_command.second);
//    ROS_ERROR_STREAM("Added poll command: " << poll_command.second.getCommand() << " (id=" << poll_command.first << ")");
//  }
//
//  d->has_started_ = true;
//}

void SypridElmo::setupPublishers()
{
  ds_elmo::ElmoMotorController::setupPublishers();

  const auto state_topic = ros::param::param<std::string>("~state_topic", "elmo_state");
  m_pub = nodeHandle().advertise<ElmoState>(state_topic, 10);
}

void SypridElmo::setupServices()
{
  ds_elmo::ElmoMotorController::setupSubscriptions();
  const auto cmd_service = ros::param::param<std::string>("~command_service", "torque_cmd");
  m_srv = nodeHandle().advertiseService<TorqueCmd::Request, TorqueCmd::Response>(
      cmd_service, boost::bind(&SypridElmo::setCommandedTorque, this, _1, _2));

  auto heartbeat_cmd = ros::param::param<std::string>("~heartbeat_elmo_cmd", "heartbeat_cmd");
  m_heartbeat_srv = nodeHandle().serviceClient<sentry_syprid::TorqueCmd>(heartbeat_cmd);
}

void SypridElmo::setupSubscriptions()
{
  ds_elmo::ElmoMotorController::setupSubscriptions();
  const auto heartbeat_topic = ros::param::param<std::string>("~heartbeat_topic", "syprid_heartbeat");
  m_heartbeat_sub = nodeHandle().subscribe<ds_core_msgs::Status>(heartbeat_topic, 10, &SypridElmo::setHeartbeat, this);
}

void SypridElmo::setupTimers()
{
  ds_elmo::ElmoMotorController::setupTimers();

  const auto period = ros::param::param<float>("~state_update_period", 0.5);
  const auto heartbeat_period = ros::param::param<float>("~heartbeat_check_period", 5.0);
  m_timer = nodeHandle().createTimer(
      ros::Duration(period), boost::bind(&SypridElmo::sendElmoStatusMessage, this, _1));
  m_heartbeat_timer = nodeHandle().createTimer(
      ros::Duration(heartbeat_period), boost::bind(&SypridElmo::checkHeartbeat, this, _1));
}

void SypridElmo::checkHeartbeat(const ros::TimerEvent&)
{
  std::vector<std::string> node_list;
  ros::master::getNodes(node_list);
  auto str = ros::names::resolve("/sentry/pkz/syprid", std::string{ "heartbeat_topic" });

/*   for (size_t i=0;i<node_list.size();i++) {
    ROS_WARN_STREAM("Node "<<i<<" is "<<node_list[i]);
  } */
  auto it = std::find(std::begin(node_list), std::end(node_list), "/sentry/pkz/syprid");
  if (it != std::end(node_list))
  {
    //DO NOTHING SINCE HEARTBEAT EXISTS
    //ROS_WARN_STREAM("Found syprid node, we're good");
  }
  else {
    ROS_ERROR_STREAM("NO SYPRID NODE!");
    m_enabled = false;
    auto elmo_cmd = sentry_syprid::TorqueCmd{};
    elmo_cmd.request.torque = 0.0;
    m_heartbeat_srv.call(elmo_cmd);
    ROS_FATAL_STREAM("No heartbeat from syprid node, killing thrusters!");
    ROS_BREAK();
  }
  node_list.clear();
/*   for (size_t i =0;i<node_list.size();i++) {
    ROS_WARN_STREAM("Node "<<i<<" is now"<<node_list[i]);
  } */
}

//Subscriber callback to set torque enabled flag based on status flag in heartbeat msg from main syprid node
void SypridElmo::setHeartbeat(const ds_core_msgs::Status status_msg) {
  if (status_msg.STATUS_GOOD) {
    m_enabled == true;
  }
  else {
    m_enabled == false;
  }
}

void SypridElmo::sendElmoStatusMessage(const ros::TimerEvent&)
{
  auto state = ElmoState{};
  state.header.stamp = ros::Time::now();
  state.ds_header.io_time = state.header.stamp;
  const auto l_uuid = uuid();
  std::copy(std::begin(l_uuid.data), std::end(l_uuid.data), std::begin(state.ds_header.source_uuid));

  const auto l_parameters = parameters();

  try
  {
    state.current_reactive = l_parameters.at("ID").second;
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.current_active = l_parameters.at("IQ").second;
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.torque = l_parameters.at("TC").second;
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.line_voltage = l_parameters.at("AN[6]").second;
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.main_position = static_cast<int>(l_parameters.at("PX").second);
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.main_feedback_vel = static_cast<int>(l_parameters.at("VX").second);
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.status = static_cast<uint32_t>(l_parameters.at("SR").second);
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.enabled = static_cast<unsigned char>(l_parameters.at("MO").second == 1);
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.mode = static_cast<uint8_t>(l_parameters.at("UM").second);
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.failure = static_cast<uint32_t>(l_parameters.at("MF").second);
  } catch (std::out_of_range&) {
//    return;
  }
  try
  {
    state.temperature = static_cast<int>(l_parameters.at("TI[1]").second);
  } catch (std::out_of_range&) {
//    return;
  }

  m_pub.publish(state);
}

bool SypridElmo::setCommandedTorque(const sentry_syprid::TorqueCmd::Request& req,
                                    sentry_syprid::TorqueCmd::Response& resp)
{
  // if we're disabled, send 0
/*   if (m_enabled == false)
  {
    m_torque = 0.00;
    ROS_ERROR_STREAM("DISABLED, torque value is "<<m_torque);
  } else {
    m_torque = req.torque;
    ROS_ERROR_STREAM("ENABLED, torque value is "<<m_torque);
  } */
  m_torque = req.torque;

  const auto command = std::string{ "UF[1]=" + std::to_string(m_torque) + "\r" };
  //ROS_ERROR_STREAM("New torque commanded... " << command);
  auto io_command = ds_asio::IoCommand{ command, 0.2 };
#ifdef INSERT_ARTIFICIAL_DELAY
  io_command.setDelayAfter(ros::Duration(ARTIFICIAL_DELAY));
#endif
  if (m_cmd_id == 0){
    // Add these commands in order
    auto mo_cmd = ds_asio::IoCommand{"MO=1", 0.2};
    addRegularCommand(mo_cmd);

    auto mo_param = ds_asio::IoCommand{"MO", 0.2};
    addRegularCommand(mo_param);

    m_cmd_id = addRegularCommand(io_command);

    auto xq_cmd = ds_asio::IoCommand{"XQ##DM", 0.2};
    addRegularCommand(xq_cmd);
    ROS_INFO_STREAM("New torque commanded... " << command);
  } else {
//    io_command.setId(m_cmd_id);
    if (replaceRegularCommand(m_cmd_id, io_command)){
      ROS_INFO_STREAM("REPLACED with " << command);
    } else {
      m_cmd_id = addRegularCommand(io_command);
      ROS_ERROR_STREAM("REPLACE FAILURE use new command instead " << command);
    }
  }
}

} //namespace
