/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by jvaccaro on 12/19/19.
//

#include "sentry_syprid/sentry_syprid.h"
#include "sentry_syprid/TorqueCmd.h"


namespace sentry_syprid {

Syprid::Syprid()
    : ds_base::DsProcess()
{
}

Syprid::Syprid(char argc, char* argv[], const std::string& name)
    : ds_base::DsProcess(argc, argv, name)
{
}

Syprid::~Syprid()
{
}

void Syprid::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();
  auto nh = nodeHandle();
  auto state_topic = ros::param::param<std::string>("~state_topic", "syprid_state");
  m_state_pub = nh.advertise<sentry_msgs::SypridState>(state_topic, 10, false);
  m_current_state = sentry_msgs::SypridState{};
  m_current_state.elmo_port_rpm.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_stbd_rpm.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_port_amps_active.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_stbd_amps_active.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_port_amps_reactive.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_stbd_amps_reactive.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_port_volts.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_stbd_volts.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_port_temp.resize(m_current_state.NUM_READS_IN_HISTORY);
  m_current_state.elmo_stbd_temp.resize(m_current_state.NUM_READS_IN_HISTORY);
//  auto elmo_port_cmd = ros::param::param<std::string>("~elmo_port_cmd", "elmo_port_cmd");
//  m_elmo_port_pub = nh.advertise<ds_actuator_msgs::ThrusterCmd>(elmo_port_cmd, 10, false);
//  auto elmo_stbd_cmd = ros::param::param<std::string>("~elmo_stbd_cmd", "elmo_stbd_cmd");
//  m_elmo_stbd_pub = nh.advertise<ds_actuator_msgs::ThrusterCmd>(elmo_stbd_cmd, 10, false);
  auto heartbeat_topic = ros::param::param<std::string>("~heartbeat_topic", "syprid_heartbeat");
  m_heartbeat_pub = nh.advertise<ds_core_msgs::Status>(heartbeat_topic, 10, false);
}

void Syprid::setupSubscriptions()
{
  ds_base::DsProcess::setupSubscriptions();
  auto nh = nodeHandle();

  auto dcam_topic = ros::param::param<std::string>("~dcam_sub", "dcam_topic");
  m_dcam_sub = nh.subscribe<sentry_msgs::SypridDcamState>
      (dcam_topic, 1, boost::bind(&Syprid::onDcamState, this, _1));

  auto elmo_port_topic = ros::param::param<std::string>("~elmo_port_sub", "elmo_port_topic");
  m_port_sub = nh.subscribe<ElmoState>
      (elmo_port_topic, 1, boost::bind(&Syprid::onPortElmoState, this, _1));

  auto elmo_stbd_topic = ros::param::param<std::string>("~elmo_stbd_sub", "elmo_stbd_topic");
  m_stbd_sub = nh.subscribe<ElmoState>
      (elmo_stbd_topic, 1, boost::bind(&Syprid::onStbdElmoState, this, _1));
}

void Syprid::setupServices()
{
  ds_base::DsProcess::setupServices();
  auto command_service = ros::param::param<std::string>("~command_service", "syprid_cmd");
  m_command_srv = nodeHandle().advertiseService<sentry_msgs::SypridCmd::Request, sentry_msgs::SypridCmd::Response>(
      command_service, boost::bind(&Syprid::onCommand, this, _1, _2));

  auto dcam_cmd = ros::param::param<std::string>("~dcam_cmd", "dcam_cmd");
  m_dcam_srv = nodeHandle().serviceClient<sentry_msgs::SypridDcamCmd>(dcam_cmd);

  auto port_cmd = ros::param::param<std::string>("~port_elmo_cmd", "port_cmd");
  m_port_srv = nodeHandle().serviceClient<sentry_syprid::TorqueCmd>(port_cmd);

  auto stbd_cmd = ros::param::param<std::string>("~stbd_elmo_cmd", "stbd_cmd");
  m_stbd_srv = nodeHandle().serviceClient<sentry_syprid::TorqueCmd>(stbd_cmd);
}

void Syprid::setupTimers()
{
  ds_base::DsProcess::setupTimers();

  m_timeout = ros::param::param<float>("~state_timeout_sec", 1.0);
  m_state_timer = nodeHandle().createTimer(
      ros::Duration(m_timeout), boost::bind(&Syprid::stateTimeout, this, _1));

  auto elmo_timeout = ros::param::param<float>("~elmo_timeout_sec", 0.0);
  if (elmo_timeout>0){
    m_elmo_timer = nodeHandle().createTimer(
        ros::Duration(elmo_timeout), boost::bind(&Syprid::elmosTimeout, this, _1));
  }
  auto dcam_timeout = ros::param::param<float>("~dcam_timeout_sec", 0.0);
  if (dcam_timeout>0){
    m_dcam_timer = nodeHandle().createTimer(
        ros::Duration(dcam_timeout), boost::bind(&Syprid::dcamsTimeout, this, _1));
  }

  heartbeat_timeout = ros::param::param<float>("~heartbeat_timeout_sec", 10.0);
  m_heartbeat_timer = nodeHandle().createTimer(
      ros::Duration(heartbeat_timeout), boost::bind(&Syprid::heartbeatTimeout, this, _1));
}

void Syprid::stateTimeout(const ros::TimerEvent&)
{
  m_state_pub.publish(m_current_state);

//  m_current_state.dcam_port_conn = sentry_msgs::SypridState::PKZ_DISCONNECTED;
//  m_current_state.dcam_stbd_conn = sentry_msgs::SypridState::PKZ_DISCONNECTED;
//  m_current_state.elmo_port_conn = sentry_msgs::SypridState::PKZ_DISCONNECTED;
//  m_current_state.elmo_stbd_conn = sentry_msgs::SypridState::PKZ_DISCONNECTED;
//
//  // Reinforce previously commanded states for timeouts, etc
//  setDcam(sentry_msgs::SypridDcamCmd::Request::DCAM_NO_CHANGE);
//  setTorque(m_current_state.elmo_port_torque_desired, true);
//  setTorque(m_current_state.elmo_stbd_torque_desired, false);
}

void Syprid::elmosTimeout(const ros::TimerEvent&)
{
  setTorque(m_current_state.elmo_port_torque_desired, true);
  setTorque(m_current_state.elmo_stbd_torque_desired, false);
}

void Syprid::dcamsTimeout(const ros::TimerEvent&)
{
  setDcam(sentry_msgs::SypridDcamCmd::Request::DCAM_NO_CHANGE);
}

//Publish a ds_core_msgs::Status msg every x sec w/ STATUS_GOOD if alive
void Syprid::heartbeatTimeout(const ros::TimerEvent&) {
  ds_core_msgs::Status status;
  status.descriptive_name = "syprid_node";
  status.status = status.STATUS_GOOD;
  m_heartbeat_pub.publish(status);
  //ROS_WARN_STREAM("Published heartbeat from syprid node!");
}
void Syprid::onPortElmoState(const ElmoState::ConstPtr msg)
{
  m_current_state.elmo_port_conn = sentry_msgs::SypridState::PKZ_CONNECTED;

  m_current_state.elmo_port_rpm_latest = msg->main_feedback_vel * 60.0 / 36.0;
  m_current_state.elmo_port_rpm_avg = bufferAverage
	  (m_current_state.elmo_port_rpm_latest,
	   m_current_state.elmo_port_rpm,
           m_current_state.NUM_READS_IN_HISTORY);

  m_current_state.elmo_port_amps_active_latest = msg->current_active;
  m_current_state.elmo_port_amps_active_avg = bufferAverage
      (m_current_state.elmo_port_amps_active_latest,
       m_current_state.elmo_port_amps_active,
       m_current_state.NUM_READS_IN_HISTORY);

  m_current_state.elmo_port_amps_reactive_latest = msg->current_reactive;
  m_current_state.elmo_port_amps_reactive_avg = bufferAverage
      (m_current_state.elmo_port_amps_reactive_latest,
       m_current_state.elmo_port_amps_reactive,
       m_current_state.NUM_READS_IN_HISTORY);

  m_current_state.elmo_port_volts_latest = msg->line_voltage;
  m_current_state.elmo_port_volts_avg = bufferAverage
      (m_current_state.elmo_port_volts_latest,
       m_current_state.elmo_port_volts,
       m_current_state.NUM_READS_IN_HISTORY);

  m_current_state.elmo_port_temp_latest = msg->temperature;
  m_current_state.elmo_port_temp_avg = bufferAverage
      (m_current_state.elmo_port_temp_latest,
       m_current_state.elmo_port_temp,
       m_current_state.NUM_READS_IN_HISTORY);
}

void Syprid::onStbdElmoState(const ElmoState::ConstPtr msg)
{
  m_current_state.elmo_stbd_conn = sentry_msgs::SypridState::PKZ_CONNECTED;

  m_current_state.elmo_stbd_rpm_latest = msg->main_feedback_vel* 60.0 / 36.0;
  m_current_state.elmo_stbd_rpm_avg = bufferAverage
      (m_current_state.elmo_stbd_rpm_latest,
       m_current_state.elmo_stbd_rpm,
       m_current_state.NUM_READS_IN_HISTORY);

  m_current_state.elmo_stbd_amps_active_latest = msg->current_active;
  m_current_state.elmo_stbd_amps_active_avg = bufferAverage
      (m_current_state.elmo_stbd_amps_active_latest,
       m_current_state.elmo_stbd_amps_active,
       m_current_state.NUM_READS_IN_HISTORY);

  m_current_state.elmo_stbd_amps_reactive_latest = msg->current_reactive;
  m_current_state.elmo_stbd_amps_reactive_avg = bufferAverage
      (m_current_state.elmo_stbd_amps_reactive_latest,
       m_current_state.elmo_stbd_amps_reactive,
       m_current_state.NUM_READS_IN_HISTORY);

  m_current_state.elmo_stbd_volts_latest = msg->line_voltage;
  m_current_state.elmo_stbd_volts_avg = bufferAverage
      (m_current_state.elmo_stbd_volts_latest,
       m_current_state.elmo_stbd_volts,
       m_current_state.NUM_READS_IN_HISTORY);

  m_current_state.elmo_stbd_temp_latest = msg->temperature;
  m_current_state.elmo_stbd_temp_avg = bufferAverage
      (m_current_state.elmo_stbd_temp_latest,
       m_current_state.elmo_stbd_temp,
       m_current_state.NUM_READS_IN_HISTORY);
}

void Syprid::onDcamState(const sentry_msgs::SypridDcamState::ConstPtr msg)
{
  m_current_state.dcam_port_conn = sentry_msgs::SypridState::PKZ_CONNECTED;
  m_current_state.dcam_stbd_conn = sentry_msgs::SypridState::PKZ_CONNECTED;
  m_current_state.dcam_port_state = msg->dcam_port_state;
  m_current_state.dcam_stbd_state = msg->dcam_stbd_state;
  m_current_state.dcam_persistence_state = msg->dcam_persistence_state;
  m_current_state.flow_running_avg = msg->flow_sensor_running_avg;
  m_current_state.flow_total_counts = msg->flow_sensor_total_counts;
}

float
Syprid::bufferAverage(float new_val, std::vector<float> average_array, int max_length)
{
  double sum = 0;
  for (int i=0; i<max_length - 1; i++){
    average_array[i] = average_array[i+1];
    sum += average_array[i];
  }
  average_array[max_length-1] = new_val;
  sum += new_val;
  return new_val / max_length;
}

std::string
Syprid::nestCommand(uint8_t syprid_cmd, double t1, double t2)
{
  // get PKZ_START_ALL 10 3
  auto sub_cmd = sentry_msgs::SypridCmd{};
  sub_cmd.request.pkz_cmd = syprid_cmd;
  sub_cmd.request.torque_1 = t1;
  sub_cmd.request.torque_2 = t2;
  if (onCommand(sub_cmd.request, sub_cmd.response))
    return sub_cmd.response.action + "\n";
  return "NO ACTION TAKEN\n";
}

std::string
Syprid::setTorque(double torque, bool port)
{
//  auto elmo_cmd = ds_actuator_msgs::ThrusterCmd{};
  //ROS_WARN_STREAM("Received torque request on "<<port<<" for torque val: "<<torque);
  std::string cmdstr = "Setting torque on ";
  auto elmo_cmd = sentry_syprid::TorqueCmd{};
  std::vector<double> torque_req;
  torque_req.resize(torque);
  if (torque == 0) {
    elmo_cmd.request.torque = 0;
    if (port){
      m_port_srv.call(elmo_cmd);
      m_current_state.elmo_port_torque_desired = torque;
      cmdstr+="port to ";
    }
    else {
      m_stbd_srv.call(elmo_cmd);
      m_current_state.elmo_stbd_torque_desired = torque;
      cmdstr+="stbd to ";
    }
    cmdstr+=elmo_cmd.request.torque;
  }
  else {
    std::iota(torque_req.begin(), torque_req.end(),1);
    for (size_t i=0;i<torque_req.size();i++) {
      elmo_cmd.request.torque = torque_req[i];
      ROS_WARN_STREAM_ONCE("elmo cmd request torque: "<<torque);
      if (port){
        m_port_srv.call(elmo_cmd);
        m_current_state.elmo_port_torque_desired = torque_req[i];
        cmdstr+="port to ";
        //ROS_WARN_STREAM("Elmo port to: "<<torque_req[i]);
      } else {
        m_stbd_srv.call(elmo_cmd);
        m_current_state.elmo_stbd_torque_desired = torque_req[i];
        cmdstr+="stbd to ";
        //ROS_WARN_STREAM("Elmo stbd to: "<<torque_req[i]);
      }
    }
    cmdstr+=elmo_cmd.request.torque;
  }
  return cmdstr;
}

void
Syprid::setDcam(uint8_t dcam_cmd)
{
  auto cmd = sentry_msgs::SypridDcamCmd{};
  cmd.request.dcam_cmd = dcam_cmd;
  m_dcam_srv.call(cmd.request, cmd.response);
}

bool
Syprid::onCommand(const sentry_msgs::SypridCmd::Request req,
                  sentry_msgs::SypridCmd::Response resp)
{
//# MC                    SypridCmd vals
//#       - Sub-actions
//#
//# PKZ START ALL $P $S   pkz_cmd=PKZ_START_ALL torque_1=$P torque_2=$S
//#       - Opens port dcam
//#       - Opens stbd dcam
//#       - Sets port torque to $P
//#       - Sets stbd torque to $S
//#
//# PKZ STOP ALL          pkz_cmd=PKZ_STOP_ALL
//#       - Zeros port torque
//#       - Zeros stbd torque
//#       - Closes port dcam
//#       - Closes stbd dcam
//#
//# PKZ START PORT $P     pkz_cmd=PKZ_START_PORT torque_1=$P
//#       - Opens port dcam
//#       - Sets port torque to $P
//# PKZ START STBD $S     pkz_cmd=PKZ_START_STBD torque_1=$S
//#       - Opens stbd dcam
//#       - Sets stbd torque to $S
//# PKZ STOP PORT         pkz_cmd=PKZ_STOP_PORT
//#       - Zeros port torque
//#       - Closes port dcam
//# PKZ STOP STBD         pkz_cmd=PKZ_STOP_STBD
//#       - Zeros stbd torque
//#       - Closes stbd dcam
//#
//# PKZ OPEN PORT         pkz_cmd=PKZ_OPEN_DCAM_PORT
//# PKZ OPEN STBD         pkz_cmd=PKZ_OPEN_DCAM_STBD
//# PKZ CLOSE PORT        pkz_cmd=PKZ_CLOSE_DCAM_PORT
//# PKZ CLOSE STBD        pkz_cmd=PKZ_CLOSE_DCAM_STBD
//# PKZ ENABLE PERSISTENCE pkz_cmd=PKZ_ENABLE_DCAM_PERSISTENCE
//# PKZ DISABLE PERSISTENCE pkz_cmd=PKZ_DISABLE_DCAM_PERSISTENCE
//# PKZ TORQUE PORT $P    pkz_cmd=PKZ_TORQUE_PORT torque_1=$P
//# PKZ TORQUE STBD $S    pkz_cmd=PKZ_TORQUE_STBD torque_1=$S

  switch (req.pkz_cmd) {
    case sentry_msgs::SypridCmd::Request::PKZ_START_ALL :
      resp.action += "Start port attempted... "
          + nestCommand(req.PKZ_START_PORT, req.torque_1);
      resp.action += "Start stbd attempted... "
          + nestCommand(req.PKZ_START_STBD, req.torque_2);
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_STOP_ALL :
      resp.action += "Stop port attempted... "
          + nestCommand(req.PKZ_STOP_PORT);
      resp.action += "Stop stbd attempted... "
          + nestCommand(req.PKZ_STOP_STBD);
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_START_PORT :
      resp.action += "Open port dcam attempted... "
          + nestCommand(req.PKZ_OPEN_DCAM_PORT);
      resp.action += "Set port torque attempted... "
          + nestCommand(req.PKZ_TORQUE_PORT, req.torque_1);
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_START_STBD :
      resp.action += "Open port dcam attempted... "
          + nestCommand(req.PKZ_OPEN_DCAM_STBD);
      resp.action += "Set port torque attempted... "
          + nestCommand(req.PKZ_TORQUE_STBD, req.torque_1);
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_STOP_PORT :
      resp.action += "Set port torque attempted... "
          + nestCommand(req.PKZ_TORQUE_PORT);
      resp.action += "Close port dcam attempted... "
          + nestCommand(req.PKZ_CLOSE_DCAM_PORT);
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_STOP_STBD :
      resp.action += "Set port torque attempted... "
          + nestCommand(req.PKZ_TORQUE_STBD);
      resp.action += "Close port dcam attempted... "
          + nestCommand(req.PKZ_CLOSE_DCAM_STBD);
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_OPEN_DCAM_PORT :
      m_current_state.dcam_port_state_desired = sentry_msgs::SypridState::PKZ_DCAM_OPEN;
      setDcam(sentry_msgs::SypridDcamCmd::Request::DCAM_OPEN_PORT);
      resp.action += "Commanded port dcam open";
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_OPEN_DCAM_STBD :
      m_current_state.dcam_stbd_state_desired = sentry_msgs::SypridState::PKZ_DCAM_OPEN;
      setDcam(sentry_msgs::SypridDcamCmd::Request::DCAM_OPEN_STBD);
      resp.action += "Commanded stbd dcam open";
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_CLOSE_DCAM_PORT :
      m_current_state.dcam_port_state_desired = sentry_msgs::SypridState::PKZ_DCAM_CLOSED;
      setDcam(sentry_msgs::SypridDcamCmd::Request::DCAM_CLOSE_PORT);
      resp.action += "Commanded port dcam closed";
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_CLOSE_DCAM_STBD :
      m_current_state.dcam_stbd_state_desired = sentry_msgs::SypridState::PKZ_DCAM_CLOSED;
      setDcam(sentry_msgs::SypridDcamCmd::Request::DCAM_CLOSE_STBD);
      resp.action += "Commanded stbd dcam closed";
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_ENABLE_DCAM_PERSISTENCE :
      m_current_state.dcam_persistence_state_desired = sentry_msgs::SypridState::PKZ_DCAM_ENABLE_PERSISTENCE;
      setDcam(sentry_msgs::SypridDcamCmd::Request::DCAM_ENABLE_PERSISTENCE);
      resp.action += "Commanded dcam persistence enabled";
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_DISABLE_DCAM_PERSISTENCE :
      m_current_state.dcam_persistence_state_desired = sentry_msgs::SypridState::PKZ_DCAM_DISABLE_PERSISTENCE;
      setDcam(sentry_msgs::SypridDcamCmd::Request::DCAM_DISABLE_PERSISTENCE);
      resp.action += "Commanded dcam persistence disabled";
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_REQUEST_FLOW_SENSOR_DATA :
      m_current_state.flow_sensor_state_desired = sentry_msgs::SypridState::PKZ_FLOW_SENSOR_ENABLE_DATA;
      setDcam(sentry_msgs::SypridDcamCmd::Request::FLOW_SENSOR_ENABLE_DATA);
      resp.action += "Commanded flow sensor data request";
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_TORQUE_PORT :
      setTorque(req.torque_1, true);
      resp.action += "Commanded port torque to " + std::to_string(req.torque_1);
      break;
    case sentry_msgs::SypridCmd::Request::PKZ_TORQUE_STBD :
      setTorque(req.torque_1, false);
      resp.action += "Commanded stbd torque to " + std::to_string(req.torque_1);
      break;
    default :
      resp.action += "NO ACTION TAKEN";
  }
  return true;
}

} //namespace
