/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by jvaccaro on 12/19/19.
//

#include "sentry_syprid/sentry_syprid_dcams.h"

namespace sentry_syprid {

SypridDcams::SypridDcams()
    : ds_base::DsProcess()
{
}

SypridDcams::SypridDcams(int argc, char* argv[], const std::string& name)
    : ds_base::DsProcess(argc, argv, name)
{
}

SypridDcams::~SypridDcams() = default;


void
SypridDcams::setupConnections()
{
  ds_base::DsProcess::setupConnections();
//  m_conn = addConnection("instrument", boost::bind(&SypridDcams::onStatus, this, _1));
  m_iosm = addIoSM("dcam_sm", "instrument", boost::bind(&SypridDcams::onStatus, this, _1));
//  std::string port_str =
//  const auto port_command = ds_asio::IoCommand{
//      port_str, 0.2, boost::bind(&SypridDcams::onBytes, _1),
//  };
//  m_iosm->addRegularCommand(poll_command);
}

void
SypridDcams::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();
  auto state_topic = ros::param::param<std::string>("~state_topic", "dcam_state");
  m_state_pub = nodeHandle().advertise<sentry_msgs::SypridDcamState>(state_topic, 10, false);
}

void
SypridDcams::setupServices()
{
  ds_base::DsProcess::setupServices();
  auto command_service = ros::param::param<std::string>("~command_service", "dcam_cmd");
  m_cmd_srv = nodeHandle().advertiseService<sentry_msgs::SypridDcamCmd::Request, sentry_msgs::SypridDcamCmd::Response>
      (command_service, boost::bind(&SypridDcams::onCommand, this, _1, _2));

}

bool
SypridDcams::onCommand(sentry_msgs::SypridDcamCmd::Request req,
                        sentry_msgs::SypridDcamCmd::Response res)
{
  std::string cmd_str = "";
  switch (req.dcam_cmd){
    case req.DCAM_OPEN_PORT :
      m_desired_state.dcam_port_state = sentry_msgs::SypridDcamState::DCAM_OPEN;
      cmd_str = "#HBP " + dcamStateToCmd(sentry_msgs::SypridDcamState::DCAM_OPEN);
      break;
    case req.DCAM_OPEN_STBD :
      m_desired_state.dcam_stbd_state = sentry_msgs::SypridDcamState::DCAM_OPEN;
      cmd_str = "#HBS " + dcamStateToCmd(sentry_msgs::SypridDcamState::DCAM_OPEN);
      break;
    case req.DCAM_CLOSE_PORT :
      m_desired_state.dcam_port_state = sentry_msgs::SypridDcamState::DCAM_CLOSED;
      cmd_str = "#HBP " + dcamStateToCmd(sentry_msgs::SypridDcamState::DCAM_CLOSED);
      break;
    case req.DCAM_CLOSE_STBD :
      m_desired_state.dcam_stbd_state = sentry_msgs::SypridDcamState::DCAM_CLOSED;
      cmd_str = "#HBS " + dcamStateToCmd(sentry_msgs::SypridDcamState::DCAM_CLOSED);
      break;
    case req.DCAM_ENABLE_PERSISTENCE :
      m_desired_state.dcam_persistence_state = sentry_msgs::SypridDcamState::DCAM_PERSISTENCE_ENABLED;
      cmd_str = "#PER " + dcamStateToCmd(sentry_msgs::SypridDcamState::DCAM_OPEN);
      break;
    case req.DCAM_DISABLE_PERSISTENCE :
      m_desired_state.dcam_persistence_state = sentry_msgs::SypridDcamState::DCAM_PERSISTENCE_DISABLED;
      cmd_str = "#PER " + dcamStateToCmd(sentry_msgs::SypridDcamState::DCAM_CLOSED);
      break;
    case req.FLOW_SENSOR_ENABLE_DATA :
      //ROS_WARN_STREAM("Received request for flow sensor data");
      m_desired_state.flow_sensor_state = sentry_msgs::SypridDcamState::FLOW_SENSOR_REQUEST_ENABLED;
      cmd_str = "#FLW\n";
      break;
    default:
      return false;
  }
  const auto cmd = ds_asio::IoCommand{cmd_str, 1.0, false};
  //ROS_WARN_STREAM("SENDING DCAM CMD: "<<cmd_str);
  m_iosm->addPreemptCommand(std::move(cmd));
//  sendUpdateToDcams();
  return true;
}

bool
SypridDcams::onStatus(ds_core_msgs::RawData bytes)
{
  const auto in_data = reinterpret_cast<char*>(bytes.data.data());
  //ROS_WARN_STREAM("RECEIVED DATA IN: "<<in_data);
  int reported_state = -1;
  int reported_avg = 0;
  int reported_counts = 0;
  int args = 0;
  args = sscanf(in_data,"!HBP: %d", &reported_state);
  if (args==1)
  {
    m_current_state.dcam_port_state = cmdToDcamState(reported_state);
  }
  args = sscanf(in_data,"!HBS: %d", &reported_state);
  if (args==1)
  {
    m_current_state.dcam_stbd_state = cmdToDcamState(reported_state);
  }
  args = sscanf(in_data, "!PER: %d", &reported_state);
  if (args==1)
  {
    m_current_state.dcam_persistence_state = cmdToPersistenceState(reported_state);
  }
  args = sscanf(in_data, "!FLW,%d,%d", &reported_avg, &reported_counts);
  if (args==2)
  {
    m_current_state.flow_sensor_state = cmdToFlowState(0);
    m_current_state.flow_sensor_running_avg = reported_avg;
    m_current_state.flow_sensor_total_counts = reported_counts;
  }
  m_state_pub.publish(m_current_state);
//  sendUpdateToDcams();
  return true;
}


std::string
SypridDcams::dcamStateToCmd(uint8_t dcam_state)
{
  if (dcam_state == sentry_msgs::SypridDcamState::DCAM_OPEN){
    return "1\n";
  } else if (dcam_state == sentry_msgs::SypridDcamState::DCAM_CLOSED){
    return "0\n";
  }
  return "";
}

uint8_t
SypridDcams::cmdToDcamState(int8_t cmd)
{
  if (cmd==1){
    return sentry_msgs::SypridDcamState::DCAM_OPEN;
  } else if (cmd==0){
    return sentry_msgs::SypridDcamState::DCAM_CLOSED;
  }
  return 0;
}

uint8_t
SypridDcams::cmdToPersistenceState(int8_t cmd)
{
  if (cmd==1){
    return sentry_msgs::SypridDcamState::DCAM_PERSISTENCE_ENABLED;
  } else if (cmd==0){
    return sentry_msgs::SypridDcamState::DCAM_PERSISTENCE_DISABLED;
  }
  return 0;
}

uint8_t
SypridDcams::cmdToFlowState(int8_t cmd)
{
  if (cmd==1){
    return sentry_msgs::SypridDcamState::FLOW_SENSOR_REQUEST_ENABLED;
  } else if (cmd==0){
    return sentry_msgs::SypridDcamState::FLOW_SENSOR_REQUEST_DISABLED;
  }
  return 0;
}

void
SypridDcams::sendUpdateToDcams()
{
//  // If no desired state has been set yet then assume the current state is just fine
  if (m_desired_state.dcam_port_state == 0){
    m_desired_state.dcam_port_state = m_current_state.dcam_port_state;
  }
  if (m_desired_state.dcam_stbd_state == 0){
    m_desired_state.dcam_stbd_state = m_current_state.dcam_stbd_state;
  }
  // Now compare for differences between desired state and current state,
  // and send commands to fix them
  if (m_current_state.dcam_port_state != m_desired_state.dcam_port_state){
    std::string cmd_str = "#HBP " + dcamStateToCmd(m_desired_state.dcam_port_state);
    const auto cmd = ds_asio::IoCommand{cmd_str, 0.5, false};
    m_iosm->addPreemptCommand(std::move(cmd));

    ROS_ERROR_STREAM("CHANGE PORT" << cmd_str);
//    m_conn->send("#HBP " + dcamStateToCmd(m_desired_state.dcam_port_state));
  }
  if (m_current_state.dcam_stbd_state != m_desired_state.dcam_stbd_state){
    std::string cmd_str = "#HBS " + dcamStateToCmd(m_desired_state.dcam_port_state);
    const auto cmd = ds_asio::IoCommand{cmd_str, 0.5, false};
    m_iosm->addPreemptCommand(std::move(cmd));

    ROS_ERROR_STREAM("CHANGE STBD " << cmd_str);
    m_iosm->addPreemptCommand(std::move(cmd));
//    m_conn->send("#HBS " + dcamStateToCmd(m_desired_state.dcam_stbd_state));
  }
  if (m_current_state.dcam_persistence_state != m_desired_state.dcam_persistence_state){
    std::string cmd_str = "#PER " + dcamStateToCmd(m_desired_state.dcam_persistence_state);
    const auto cmd = ds_asio::IoCommand{cmd_str, 0.5, false};
    m_iosm->addPreemptCommand(std::move(cmd));
  }
}

}
