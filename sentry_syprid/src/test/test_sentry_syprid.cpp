//
// Created by jvaccaro on 12/19/19.
//
#include "sentry_syprid/sentry_syprid.h"

#include <list>
#include <gtest/gtest.h>

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class SypridTest : public ::testing::Test
{
public:
    // This method runs ONCE before a text fixture is run (not once-per-test-case)
    static void SetUpTestCase()
    {
        ros::Time::init();
    }
};

TEST_F(SypridTest, sypridTorqueTest)
{
// Simple function to create a sentry_msgs::SypridCmd from arguments
auto torque_cmd = [](uint pkz_cmd, float t1, float t2 ) {
    auto msg = sentry_msgs::SypridCmd{};
    msg.request.pkz_cmd = pkz_cmd;
    msg.request.torque_1 = t1;
    msg.request.torque_2 = t2;
    return msg;
};

sentry_syprid::Syprid this_syprid;
//auto torque_msg = torque_cmd(1,4,4);
uint8_t pkz_cmd = 1;
double t1 = 4;
double t2 = 4;
auto received_str = this_syprid.setTorque(t1, true);
std::string expected_str = "Setting torque on port to \x4";
auto parsed_msg = sentry_msgs::SypridCmd{};
EXPECT_EQ(expected_str, received_str);
}

TEST_F(SypridTest, WrongValueTest) {
    sentry_syprid::Syprid this_syprid;
    uint8_t pkz_cmd = 3;
    double t1 = 1;
    double t2 = 3;
    auto received_str = this_syprid.setTorque(t1, false);
    std::string expected_str = "Setting torque on stbd to \x3";
    EXPECT_EQ(expected_str, received_str);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}