/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by jvaccaro on 12/19/19.
//

#ifndef SENTRY_WS_SENTRY_SYPRID_DCAMS_H
#define SENTRY_WS_SENTRY_SYPRID_DCAMS_H

#include "ds_base/ds_process.h"
#include "sentry_msgs/SypridDcamState.h"
#include "sentry_msgs/SypridDcamCmd.h"

namespace sentry_syprid {

class SypridDcams : public ds_base::DsProcess {
  DS_DISABLE_COPY(SypridDcams)
 public:
  SypridDcams();
  SypridDcams(int argc, char* argv[], const std::string& name);
  ~SypridDcams() override;
  static std::string dcamStateToCmd(uint8_t dcam_state);
  static uint8_t cmdToDcamState(int8_t cmd);
  static uint8_t cmdToPersistenceState(int8_t cmd);
  static uint8_t cmdToFlowState(int8_t cmd);

 protected:
  void setupPublishers() override;
  void setupConnections() override;
  void setupServices() override;
  bool onCommand(sentry_msgs::SypridDcamCmd::Request req,
                 sentry_msgs::SypridDcamCmd::Response res);
  bool onStatus(ds_core_msgs::RawData bytes);

 private:
  void sendUpdateToDcams();
  sentry_msgs::SypridDcamState m_current_state;
  sentry_msgs::SypridDcamState m_desired_state;
//  boost::shared_ptr<ds_asio::DsConnection> dcam_;      //!< The DCAM I/O connection.
  boost::shared_ptr<ds_asio::IoSM> m_iosm;           //!< The DCAM state machine.
//  boost::shared_ptr<ds_asio::DsConnection> m_conn;
  ros::Publisher m_state_pub;
  ros::ServiceServer m_cmd_srv;
};

}

#endif //SENTRY_WS_SENTRY_SYPRID_DCAMS_H
