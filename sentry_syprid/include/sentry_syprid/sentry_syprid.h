/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by jvaccaro on 12/19/19.
//

#ifndef SENTRY_WS_SENTRY_SYPRID_H
#define SENTRY_WS_SENTRY_SYPRID_H

#include "ds_base/ds_process.h"
#include "sentry_msgs/SypridState.h"
#include "sentry_msgs/SypridCmd.h"
#include "sentry_msgs/SypridDcamState.h"
#include "sentry_msgs/SypridDcamCmd.h"
#include "sentry_syprid/ElmoState.h"
#include "ds_actuator_msgs/ThrusterCmd.h"
#include "ds_core_msgs/Status.h"

namespace sentry_syprid{

/// This node controls and stores the status of the Syprid system on AUV Sentry.
/// Syprid (or plankzooka) is a plankton sampling system that consists of
/// two samplers, one on the port, and one on the starboard side.
///
/// Each sampler has a dcam to open/close, and a motor (elmo) to suck in water/plankton.
///
/// In order to start sampling, you need to open the dcams and command a torque.
///
///

class Syprid : public ds_base::DsProcess{
  DS_DISABLE_COPY(Syprid)
 public:
  Syprid();
  Syprid(char argc, char* argv[], const std::string& name);
  ~Syprid();

  static float bufferAverage(float new_val, std::vector<float> average_array, int max_length);

 protected:
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupServices() override;
  void setupTimers() override;

public:
  /// @brief process and pack the Elmo State into the SypridState
  void onPortElmoState(const ElmoState::ConstPtr msg);
  void onStbdElmoState(const ElmoState::ConstPtr msg);

  /// @brief process and pack the dcam state into the SypridState
  void onDcamState(const sentry_msgs::SypridDcamState::ConstPtr msg);

  /// @brief At the timeout, the SypridState gets published.
  void stateTimeout(const ros::TimerEvent&);

  /// @brief At the timeout, if there is a current commanded elmo state,
  /// it gets re-commanded
  void elmosTimeout(const ros::TimerEvent&);

  /// @brief At the timeout, if there is a current commanded dcams state,
  /// it gets re-commanded
  void dcamsTimeout(const ros::TimerEvent&);

  /// @brief At the timeout, publishes a heartbeat msg
  void heartbeatTimeout(const ros::TimerEvent&);

  /// @brief commands elmo state to a single elmo via a service
  std::string setTorque(double torque, bool port);

  /// @brief commands dcam state via a service
  void setDcam(uint8_t dcam_cmd);


  /// @brief at the command, actions are attempted and logged
  bool onCommand(sentry_msgs::SypridCmd::Request req,
                 sentry_msgs::SypridCmd::Response resp);

  /// @brief For multi-step commands, this function wraps the recursive steps
  std::string nestCommand(uint8_t syprid_cmd, double t1=0.00, double t2=0.00);

  ros::Publisher m_state_pub;
  ros::Publisher m_heartbeat_pub;

//  ros::Publisher m_elmo_port_pub;
//  ros::Publisher m_elmo_stbd_pub;
  ros::ServiceServer m_command_srv;

  ros::Subscriber m_port_sub;
  ros::Subscriber m_stbd_sub;
  ros::Subscriber m_dcam_sub;
  ros::ServiceClient m_port_srv;
  ros::ServiceClient m_stbd_srv;
  ros::ServiceClient m_dcam_srv;

  sentry_msgs::SypridState m_current_state;
//  sentry_msgs::SypridState m_desired_state;
  ros::Timer m_state_timer;
  ros::Timer m_elmo_timer;
  ros::Timer m_dcam_timer;
  ros::Timer m_heartbeat_timer;
  double m_timeout, heartbeat_timeout;
};

}

#endif //SENTRY_WS_SENTRY_SYPRID_H
