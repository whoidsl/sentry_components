/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_SYPRID_ELMO_H
#define SENTRY_SYPRID_ELMO_H

#include <ds_elmo/elmomotorcontroller.h>
#include "sentry_syprid/TorqueCmd.h"
#include "ds_core_msgs/Status.h"

namespace sentry_syprid
{

/// @brief Syprid-specific Elmo motor controllers
///
/// # Parameters
///
/// In addition to the parameters used by `DsProcess`, the underlying
/// `ElmoMotorController` looks for the following:
///
///   - `~init_commands`:  A list of commands to send to the controller at startup
///   - `~poll_commands`:  A list of commands to regularly poll the controller with
///
/// # Connections
///
/// The elmo controller expects connection information under a private `~elmo` parameter
/// namespace.
class SypridElmo : public ds_elmo::ElmoMotorController
{
//  DS_DECLARE_PRIVATE(SypridElmo)

public:
  SypridElmo();
  SypridElmo(int argc, char* argv[], const std::string& name);
  ~SypridElmo() override;
  DS_DISABLE_COPY(SypridElmo);

 protected:
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupServices() override;
  void setupTimers() override;

  bool elmos_enabled;

 private:
  bool setCommandedTorque(const TorqueCmd::Request& req, TorqueCmd::Response& resp);
  void sendElmoStatusMessage(const ros::TimerEvent&);
  void checkHeartbeat(const ros::TimerEvent&);
  void setHeartbeat(const ds_core_msgs::Status status_msg);
  bool m_enabled;
  boost::shared_ptr<ds_core_msgs::Status const> statusMsgReceived;
  float m_torque;
  ros::Timer m_timer;
  ros::Timer m_heartbeat_timer;
  ros::Publisher m_pub;
  ros::ServiceServer m_srv;
  ros::ServiceClient m_heartbeat_srv;
  ros::Subscriber m_heartbeat_sub;
  ros::Duration timeout;
  uint64_t m_cmd_id = 0;
};
}
#endif  // SENTRY_SYPRID_ELMO_H
