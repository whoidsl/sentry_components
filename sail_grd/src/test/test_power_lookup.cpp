/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 5/2/18.
//

#include "../../include/sail_grd/pwr_lookup.h"
#include "../../include/sail_grd/pwr.h"

#include <ds_base/ds_bus.h>
#include <gtest/gtest.h>
#include <ros/param.h>

// Test fixture for parsing, needed because we want to call ros::Time::init() before our tests.
class PowerLookupTest : public ::testing::Test
{
public:
  int repeats = 5;
  // This method runs ONCE before a text fixture is run (not once-per-test-case)
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

TEST_F(PowerLookupTest, powerup_actual)
{
  ROS_ERROR_STREAM("Actual mission powerup");
  ROS_ERROR_STREAM("+++++++++++++++++++++++++++++++++++++++++++");
  for (int i = 0; i < repeats; i++)
  {
    ROS_ERROR_STREAM("powerup:     " << grd_util::get_powerup_string());
  }
  ROS_ERROR_STREAM(" ");
}

TEST_F(PowerLookupTest, powerdown_actual)
{
  ROS_ERROR_STREAM("Actual mission powerdown");
  ROS_ERROR_STREAM("+++++++++++++++++++++++++++++++++++++++++++");
  for (int i = 0; i < repeats; i++)
  {
    ROS_ERROR_STREAM("powerdown:   " << grd_util::get_powerdown_string());
  }
  ROS_ERROR_STREAM(" ");
}

TEST_F(PowerLookupTest, powerup_known)
{
  ROS_ERROR_STREAM("Known mission powerup");
  ROS_ERROR_STREAM("Expected:    0000FFFF0000FFFF");
  ROS_ERROR_STREAM("+++++++++++++++++++++++++++++++++++++++++++");
  for (int i = 0; i < repeats; i++)
  {
    ROS_ERROR_STREAM("powerup:     " << grd_util::get_powerup_string("/foo/power_config"));
  }
  ROS_ERROR_STREAM(" ");
}

TEST_F(PowerLookupTest, powerdown_known)
{
  ROS_ERROR_STREAM("Known mission powerdown");
  ROS_ERROR_STREAM("Expected:    FFFF00000000FFFF");
  ROS_ERROR_STREAM("+++++++++++++++++++++++++++++++++++++++++++");
  for (int i = 0; i < repeats; i++)
  {
    ROS_ERROR_STREAM("powerdown:   " << grd_util::get_powerdown_string("/foo/power_config"));
  }
  ROS_ERROR_STREAM(" ");
}

TEST_F(PowerLookupTest, powerup_none)
{
  ROS_ERROR_STREAM("No values given");
  ROS_ERROR_STREAM("Expected:    0000000000000000");
  ROS_ERROR_STREAM("+++++++++++++++++++++++++++++++++++++++++++");
  for (int i = 0; i < repeats; i++)
  {
    ROS_ERROR_STREAM("powerup:     " << grd_util::get_powerup_string("/none"));
  }
  ROS_ERROR_STREAM(" ");
}

TEST_F(PowerLookupTest, powerdown_none)
{
  ROS_ERROR_STREAM("No values given");
  ROS_ERROR_STREAM("Expected:    0000000000000000");
  ROS_ERROR_STREAM("+++++++++++++++++++++++++++++++++++++++++++");
  for (int i = 0; i < repeats; i++)
  {
    ROS_ERROR_STREAM("powerdown:   " << grd_util::get_powerdown_string("/none"));
  }
  ROS_ERROR_STREAM(" ");
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "test_powerlookup");
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();
  return ret;
}
