/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/6/18.
//

#ifndef PROJECT_PWR_LOOKUP_H
#define PROJECT_PWR_LOOKUP_H

#include <ds_util/int_to_hex.h>
#include <ros/param.h>

// Utilities for looking up pwr grid parameters from the address/name input.
// Use the default param string unless you know what you're doing.

namespace grd_util
{
//    std::string param_str = "/sentry/config/power_config";
// if not found, return -1
uint16_t get_address(std::string name, std::string _param_str = "/sentry/config/power_config");

// if not found, return ""
std::string get_name(uint16_t address, std::string _param_str = "/sentry/config/power_config");

// if not found, return false
bool get_confirm_on(uint16_t address, std::string _param_str = "/sentry/config/power_config");

// if not found, return false
bool get_confirm_off(uint16_t address, std::string _param_str = "/sentry/config/power_config");

// if not found, return ""
std::string get_group(uint16_t address, std::string _param_str = "/sentry/config/power_config");

// if not found, return false
bool get_mission_poweron(uint16_t address, std::string _param_str = "/sentry/config/power_config");

// if not found, return false
bool get_mission_poweroff(uint16_t address, std::string _param_str = "/sentry/config/power_config");

std::vector<uint16_t> get_powerup_addresses(std::string name, std::string _param_str);

std::vector<uint16_t> get_powerdown_addresses(std::string name, std::string _param_str);

std::string get_powerup_string(std::string _param_str = "/sentry/config/power_config");

std::string get_powerdown_string(std::string _param_str = "/sentry/config/power_config");
}

#endif  // PROJECT_PWR_LOOKUP_H
