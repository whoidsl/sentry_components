/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/1/18.
//

#include <sentry_msgs/BatteryCmd.h>
#include <ds_hotel_msgs/PowerSupplyCommand.h>
#include "../../include/sail_bat/batman.h"

using namespace sail_bat;

BatMan::BatMan() : DsProcess()
{
  ChargeConfig config();
}

BatMan::BatMan(int argc, char** argv, const std::string& name) : DsProcess(argc, argv, name)
{
  ChargeConfig config();
}

BatMan::~BatMan()
{
  safeBats();
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void BatMan::startCharge()
{
  stopCharge();

  ROS_INFO_STREAM("CHARGE STARTED");
  multi = std::unique_ptr<MultichargeSequence>(new MultichargeSequence(bats, chgs, multi_timer));
}

void BatMan::stopCharge()
{
  ROS_INFO_STREAM("CHARGE STOPPED");
  if (multi)
  {
    multi_timer.stop();
    multi.reset();
  }

  for (int i = 0; i < bats.size(); i++)
  {
    // ZB 2023-04-25: Don't need to explicitly zero shunts.  Follow the trail in the supervisor firmware
    //                supervisor.c: starting in line 1981
    //bats[i]->zeroShunt();
    bats[i]->setOff();
  }

  for (int i = 0; i < chgs.size(); i++)
  {
    chgs[i]->setOff();
  }
}

void BatMan::batsOn()
{
  ROS_INFO_STREAM("BATS ON");
  for (int i = 0; i < bats.size(); i++)
  {
    bats[i]->setOn();
  }
}

void BatMan::batsOff()
{
  ROS_INFO_STREAM("BATS OFF");
  for (int i = 0; i < bats.size(); i++)
  {
    bats[i]->setOff();
  }
}

void BatMan::shoreOn()
{
  ROS_INFO_STREAM("SHORE ON");
  shore->setVC(61, 8.0);
}

void BatMan::shoreOff()
{
  ROS_INFO_STREAM("SHORE OFF");
  shore->setOff();
}

void BatMan::safeBats()
{
  ROS_INFO_STREAM("SAFE BATS");
  stopCharge();
}

void BatMan::pubLatest(const ros::TimerEvent&)
{
  auto now = ds_hotel_msgs::BatMan{};

  // Add timestamp.
  now.header.stamp = ros::Time::now();
  now.ds_header.io_time = now.header.stamp;

  now.moduleVolt.resize(num_bats);
  now.moduleAh.resize(num_bats);
  now.modulePercent.resize(num_bats);
  now.num_bats = 0;
  now.maxModuleVolt = 0;
  now.minModuleVolt = 100;
  now.maxCellVolt = 0;
  now.minCellVolt = 100;
  now.maxSwitchTemp = 0;
  now.minSwitchTemp = 100;
  now.maxCellTemp = 0;
  now.minCellTemp = 100;

  for (int i = 0; i < num_bats; i++)
  {
    auto bat = bats[i]->get_latest();
    now.moduleVolt[i] = bat.totalVoltage;
    now.moduleAh[i] = bat.chargeAh;
    now.modulePercent[i] = bat.percentFull;

    if (bat.idnum == 0)
    {
      // Battery hasn't published yet, so skip to the next battery
      continue;
    }
    now.num_bats += 1;
    if (bat.switchTemp < now.minSwitchTemp)
    {
      now.minSwitchTemp = bat.switchTemp;
    }
    if (bat.switchTemp > now.maxSwitchTemp)
    {
      now.maxSwitchTemp = bat.switchTemp;
    }
    if (bat.minCellTemp < now.minCellTemp)
    {
      now.minCellTemp = bat.minCellTemp;
    }
    if (bat.maxCellTemp > now.maxCellTemp)
    {
      now.maxCellTemp = bat.maxCellTemp;
    }
    if (bat.totalVoltage > now.maxModuleVolt)
    {
      now.maxModuleVolt = bat.totalVoltage;
    }
    if (bat.totalVoltage < now.minModuleVolt)
    {
      now.minModuleVolt = bat.totalVoltage;
    }
    if (bat.maxCellVoltage > now.maxCellVolt)
    {
      now.maxCellVolt = bat.maxCellVoltage;
    }
    if (bat.minCellVoltage < now.minCellVolt)
    {
      now.minCellVolt = bat.minCellVoltage;
    }
    if (bat.percentFull > -10 && bat.percentFull < 110)
    {
      if (bat.discharging)
      {
        now.capacityCoulombs += bat.capacityCoulombs;
        now.capacityAh += bat.capacityAh;
        now.chargeCoulombs += bat.chargeCoulombs;
        now.chargeAh += bat.chargeAh;
      }
    }
    else
    {
      ROS_ERROR_STREAM("Bat " << bat.idnum << " has an out-of-range percent");
    }
  }
  now.percentFull = 100.0 * now.chargeAh / now.capacityAh;
  if(multi)
  {
    now.charger_ids = multi->getChargerPairs();
  }
  latest = now;
  batman_pub_.publish(latest);
}

void BatMan::stop()
{
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void BatMan::setupParameters()
{
  num_bats = 6;
  num_chgs = 5;

  auto batman_prefix = ros::this_node::getNamespace() + "/";
  auto bat_prefix = ros::param::param<std::string>("~bat_path", batman_prefix);
  bats.resize(num_bats);
  for (int i = 0; i < num_bats; i++)
  {
    bats[i] = new Bat(bat_prefix + "bat" + std::to_string(i + 1), i + 1);
  }

  auto chg_prefix = ros::param::param<std::string>("~chg_path", batman_prefix);
  //    auto chg_prefix = ros::this_node::getNamespace();
  chgs.resize(num_chgs);
  for (int i = 0; i < num_chgs; i++)
  {
    chgs[i] = new Pwr(chg_prefix + "chg" + std::to_string(i + 1), i + 1);
  }

  auto shore_addr = ros::param::param<std::string>("~shore_address", batman_prefix + "shore");
  shore = new Pwr(shore_addr, 0);
}

void BatMan::setupConnections()
{
  bat_clients_.resize(num_bats);
  bat_subs_.resize(num_bats);
  chg_clients_.resize(num_chgs);
  chg_subs_.resize(num_chgs);

  auto nh = nodeHandle();
  for (int i = 0; i < num_bats; i++)
  {
//    ROS_INFO_STREAM(bats[i]->get_addr());
    bat_clients_[i] = nh.serviceClient<sentry_msgs::BatteryCmd>(bats[i]->get_addr() + "/cmd");
    bat_subs_[i] = nh.subscribe(bats[i]->get_addr() + "/state", 1, &Bat::updateLatest, bats[i]);
    bats[i]->set_client(bat_clients_[i]);
    if (i < num_chgs)
    {
      chg_clients_[i] = nh.serviceClient<ds_hotel_msgs::PowerSupplyCommand>(chgs[i]->get_addr() + "/cmd");
      chg_subs_[i] = nh.subscribe(chgs[i]->get_addr() + "/dcpwr", 1, &Pwr::updateLatest, chgs[i]);
      chgs[i]->set_client(chg_clients_[i]);
    }
  }

  shore_client_ = nh.serviceClient<ds_hotel_msgs::PowerSupplyCommand>(shore->get_addr() + "/cmd");
  shore_sub_ = nh.subscribe(shore->get_addr() + "/dcpwr", 1, &Pwr::updateLatest, shore);
  shore->set_client(shore_client_);

  chg_cmd_ = nh.advertiseService<sentry_msgs::ChargeCmd::Request, sentry_msgs::ChargeCmd::Response>(
      ros::this_node::getName() + "/chg_cmd", boost::bind(&BatMan::_chg_cmd, this, _1, _2));

  bat_pwr_cmd_ = nh.advertiseService<sentry_msgs::PowerCmd::Request, sentry_msgs::PowerCmd::Response>(
      ros::this_node::getName() + "/bat_pwr_cmd", boost::bind(&BatMan::_bat_pwr_cmd, this, _1, _2));

  shore_pwr_cmd_ = nh.advertiseService<sentry_msgs::PowerCmd::Request, sentry_msgs::PowerCmd::Response>(
      ros::this_node::getName() + "/shore_pwr_cmd", boost::bind(&BatMan::_shore_pwr_cmd, this, _1, _2));

  batman_pub_ = nh.advertise<ds_hotel_msgs::BatMan>(ros::this_node::getName() + "/state", 10);

  multi_timer = nh.createTimer(ros::Duration(1), &BatMan::tick, this);
  multi_timer.stop();

  pub_timer = nh.createTimer(ros::Duration(3), &BatMan::pubLatest, this);
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

bool BatMan::_chg_cmd(const sentry_msgs::ChargeCmd::Request& req, const sentry_msgs::ChargeCmd::Response& resp)
{
  switch (req.command)
  {
    case sentry_msgs::ChargeCmd::Request::CHARGE_CMD_OFF:
      stopCharge();
      break;
    case sentry_msgs::ChargeCmd::Request::CHARGE_CMD_CHARGE:
      startCharge();
      break;
    default:
      return false;
  }
  return true;
}

bool BatMan::_bat_pwr_cmd(const sentry_msgs::PowerCmd::Request& req, const sentry_msgs::PowerCmd::Response& resp)
{
  switch (req.command)
  {
    case sentry_msgs::PowerCmd::Request::POWER_CMD_OFF:
      batsOff();
      break;
    case sentry_msgs::PowerCmd::Request::POWER_CMD_ON:
      batsOn();
      break;
    default:
      return false;
  }
  return true;
}

bool BatMan::_shore_pwr_cmd(const sentry_msgs::PowerCmd::Request& req, const sentry_msgs::PowerCmd::Response& resp)
{
  switch (req.command)
  {
    case sentry_msgs::PowerCmd::Request::POWER_CMD_OFF:
      shoreOff();
      break;
    case sentry_msgs::PowerCmd::Request::POWER_CMD_ON:
      shoreOn();
      break;
    default:
      return false;
  }
  return true;
}

void BatMan::tick(const ros::TimerEvent&)
{
  if (multi)
  {
    multi->tick();
  }
}

///////////////////////////////////////////////////////////////////////////////
// MultichargeSequence
///////////////////////////////////////////////////////////////////////////////

MultichargeSequence::MultichargeSequence(std::vector<Bat*> _batteries, std::vector<Pwr*> _chargers, ros::Timer _timer)
  : batteries(_batteries), chargers(_chargers), timer(_timer), config()
{
  num_bats = batteries.size();
  charger_pairs_.resize(num_bats, -1);
  starting = false;
  timer.setPeriod(ros::Duration(30));
  timer.start();

  startCharge();
}

MultichargeSequence::~MultichargeSequence()
{
  stopCharge();
}

std::vector<std::int8_t> MultichargeSequence::getChargerPairs() const
{
  return charger_pairs_;
}

void MultichargeSequence::startCharge()
{
  starting = true;
  batteryId = 0;
  chargerId = 0;

  // Turn all the batteries off
  for (int i = 0; i < num_bats; i++)
  {
    batteries[i]->setOff();
  }

  //     Turn on all the chargers REALLY LOW
  for (auto chg = chargers.begin(); chg != chargers.end(); chg++)
  {
    (*chg)->setVC(config.getTARGET_V(), config.getINITIAL_C());
  }

  // we're going to start turning the batteries on slowly one by one
  // checking to see if current is being drawn.  That way we don't
  // have to worry about batteries / chargers getting cross-wired,
  // which would be a major safety issue!

  batteries[batteryId]->setCharge();
}

void MultichargeSequence::startupTick()
{
  bool found = false;

  if (batteries[batteryId]->get_is_charging())
  {
    for (auto chg = chargers.begin(); chg != chargers.end();)
    {
      if ((*chg)->get_meas_amps() > config.getINITIAL_C() / 2.0)
      {
        // We found the connected charger!
        ROS_INFO_STREAM("Starting charge sequence " << (*chg)->get_id() << " --> " << batteries[batteryId]->get_id());
        ROS_INFO_STREAM("          Max Charge Current:  " << config.getMAX_C());
        ROS_INFO_STREAM("       Ramp Charge Increment:  " << config.getRAMP_INCR_C());
        ROS_INFO_STREAM("Min Charge Current Before CV:  " << config.getMIN_C());
        ROS_INFO_STREAM("       Target Module Voltage:  " << config.getTARGET_V());
        ROS_INFO_STREAM("         Target Cell Voltage:  " << config.getTARGET_CELL_V());
        ROS_INFO_STREAM("            Max Cell Voltage:  " << config.getMAX_CELL_V());

	charger_pairs_[batteryId] = static_cast<std::int8_t>((*chg)->get_id());
        chargeSeqs.push_back(new ChargeSequence(config, (*chg), batteries[batteryId]));
        found = true;
        auto eraseMe = chg;
        chg++;
        chargers.erase(eraseMe);
        break;
      }
      chg++;
    }
    if (!found)
    {
      ROS_ERROR_STREAM("Could not find charger for battery " << batteries[batteryId]->get_id());
      batteries[batteryId]->setOff();
    }
  }

  batteryId++;
  if (batteryId >= num_bats || chargers.empty())
  {
    // we're out of batteries or chargers
    starting = false;
    // turn off any remaining chargers
    for (auto chg = chargers.begin(); chg != chargers.end(); chg++)
    {
      (*chg)->setOff();
    }
    chargers.clear();
    ROS_INFO_STREAM("Charging " << chargeSeqs.size() << " battery/charger pairs!");
    return;
  }

  // turn on the next battery
  std::stringstream batname;
  batname << "B" << batteryId;

  if (batteries.size() < batteryId)
  {
    ROS_ERROR_STREAM("No battery " << batteryId << " exists");
  }
  else
  {
    batteries[batteryId]->setCharge();
  }
}

void MultichargeSequence::tick()
{
  timer.setPeriod(ros::Duration(40));

  if (starting)
  {
    startupTick();
  }
  for (auto chg_sq = chargeSeqs.begin(); chg_sq != chargeSeqs.end();)
  {
    (*chg_sq)->tick();
    if ((*chg_sq)->isDone())
    {
      // erasing makes the iterator invalid, so create a temporary
      // This also makes the loop iterator ugly.  But ugly beats segfault.
      auto eraseMe = chg_sq;
      chg_sq++;
      chargeSeqs.erase(eraseMe);
    }
    else
    {
      chg_sq++;
    }
  }
}

void MultichargeSequence::stopCharge()
{
  ROS_INFO_STREAM("MULTI stop");
  chargeSeqs.resize(0);  // will call destructors
}

///////////////////////////////////////////////////////////////////////////////
// ChargeSequence
///////////////////////////////////////////////////////////////////////////////
ChargeSequence::ChargeSequence(const ChargeConfig& _c, Pwr* _charger, Bat* _battery)
  : config(_c), battery(_battery), charger(_charger)
{
  // Do nothing (initializer list did it all)
  batteryCommsTimeout = config.getBatteryTimeout();
  chargerCommsTimeout = config.getChargerTimeout();
  holdDuration = ros::Duration(60 * config.getHoldDurationMinutes());

  current = config.getINITIAL_C();
  voltage = config.getTARGET_V();
  terminalVmax = config.getN_CELLS() * config.getTARGET_CELL_V() + config.getDIODE_V();

  state = ChargeState::RAMP;

  balancerState = BALANCER_NOT_STARTED;
  startCharge();
}

ChargeSequence::~ChargeSequence()
{
  stopCharge();
}

void ChargeSequence::tick()
{
  if (charge_notOk())
  {
    ROS_ERROR_STREAM("Battery " << battery->get_id() << " is not OK, aborting");
    stopCharge();
    return;
  }

  battery->setCharge();

  switch (state)
  {
    case DONE:
      // do nothing
      break;

    case RAMP:
      tick_ramp();
      break;

    case CHARGE:
      tick_charge();
      break;

    case BALANCE:
      tick_balance();
      break;

    default:
      ROS_ERROR_STREAM("Battery " << battery->get_id() << " charge sequence has an unknown state, aborting charge");
      stopCharge();
  }
}

void ChargeSequence::tick_ramp()
{

  if (charger->get_is_const_voltage())
  {
    ROS_INFO_STREAM("Charger " << charger->get_id() << " --> Battery " << battery->get_id()
                                       << " reached constant-voltage during ramp.  Proceeding directly to charge");
    voltage = terminalVmax + current * config.getCABLE_R();
    state = ChargeState::CHARGE;
    charger->setVC(voltage, current);
    return;
  }

  if(battery->get_max_cell_voltage() > config.getMAX_CELL_V())
  {
    ROS_INFO_STREAM("Charger " << charger->get_id() << " --> Battery " << battery->get_id()
                                       << " reached cell max volt limit (" << battery->get_max_cell_voltage() 
				       << " > " << config.getMAX_CELL_V() << ").");
    ROS_INFO_STREAM("Reducing current one step and proceeding directly to charge.");
    current -= config.getINCR_C();
    if (current < 0)
    {
      ROS_ERROR_STREAM("Attempted to set charger: " << charger->get_id() << " current negative: (" << current << "). Setting current to 0 and halting charge. ");
      stopCharge();
      return;
    }
    voltage = terminalVmax + current * config.getCABLE_R();
    state = ChargeState::CHARGE;
    charger->setVC(voltage, current);
    return;
  }
  
  // Min cell voltage exceeds balance target, stop ramp here
  if(battery->get_min_cell_voltage() > config.getTARGET_CELL_V())
  {
    ROS_INFO_STREAM("Charger " << charger->get_id() << " --> Battery " << battery->get_id()
                                       << " reached target cell volt limit (" << battery->get_max_cell_voltage() 
				       << " > " << config.getTARGET_CELL_V() << ").");
    ROS_INFO_STREAM("proceeding directly to charge.");
    state = ChargeState::CHARGE;
    return;
  }

  // The normal case (constant current!)
  if (current + config.getRAMP_INCR_C() < config.getMAX_C())
  {
    current += config.getRAMP_INCR_C();
    voltage = terminalVmax + current * config.getCABLE_R();
    ROS_INFO_STREAM("Charger " << charger->get_id() << " --> Battery " << battery->get_id()
                                       << " INCR RAMP V=" << voltage << "V C=" << current << "A");
    charger->setVC(voltage, current);
    return;
  }

  // Finished ramp
  voltage = terminalVmax + current * config.getCABLE_R();
  current = config.getMAX_C();
  state = ChargeState::CHARGE;

  ROS_INFO_STREAM("Charger " << charger->get_id() << " --> Battery " << battery->get_id()
                                     << " has finished RAMP V=" << voltage << "V C=" << current << "A");
  charger->setVC(voltage, current);
}

void ChargeSequence::tick_charge()
{
  // Should only be called when we're in the charge state
  if (ChargeState::CHARGE != state) {
    ROS_WARN_STREAM("ChargeSequence::tick_charge() called when state != ChargeState::CHARGE");
    return;
  }

  // don't let the next current go negative.
  const auto next_current = std::max(current - config.getINCR_C(), 0.0);
  const auto voltage_at_next_stepdown = terminalVmax + next_current * config.getCABLE_R();
  auto reduce_current = false;

  if(battery->get_max_cell_voltage() > config.getMAX_CELL_V())
  {
    ROS_INFO_STREAM("Charger " << charger->get_id() << ", Battery " << battery->get_id() << " has reached individual max cell voltage limit (" << battery->get_max_cell_voltage() << " > " << config.getMAX_CELL_V() <<")");
    reduce_current = true;
  }

  if (charger->get_meas_volts() > voltage_at_next_stepdown)
  {
    ROS_INFO_STREAM("Charger " << charger->get_id() << " has reached next step-down voltage limit: " << voltage_at_next_stepdown);
    reduce_current = true;
  }

  // Not reducing current this step, nothing left to do
  if (!reduce_current)
  {
    return;
  }

  // Next current less than the minimum?  Time to enter CV mode
  if (next_current < config.getMIN_C())
  {
    ROS_INFO_STREAM("Charger " << charger->get_id() << " --> Battery " << battery->get_id()
                                       << " has finished charging, trickle-charging until balance complete");
    voltage = terminalVmax;
    current = config.getMIN_C();
    state = ChargeState::BALANCE;
    charger->setVC(voltage, current);
    return;
  }

  current = next_current;
  voltage = voltage_at_next_stepdown;
  ROS_INFO_STREAM("Stepping down charger: " << charger->get_id() << " c: " << current << " v: " << voltage);
  charger->setVC(voltage, current);
}

void ChargeSequence::tick_balance()
{
  // Should only be called when we're in the balance state
  if (ChargeState::BALANCE != state) {
    ROS_WARN_STREAM("ChargeSequence::tick_balance() called when state != ChargeState::BALANCE");
    return;
  }

  //if (battery->get_max_cell_voltage() > config.getMAX_CELL_V())
  //{
  //    ROS_INFO_STREAM("B" << battery->get_id() << " exceeded max cell voltage while trickle charging (" << battery->get_max_cell_voltage() << " > " << config.getMAX_CELL_V() << "). Stopping charge.");
  //    holdInProgress = false;
  //    stopCharge();
  //    return;
  //}


  if ((charger->get_meas_amps() <= 0.100) && (battery->get_min_cell_voltage() >= 4.090) )
  {
    if (!holdInProgress) {
      holdStartTime = ros::WallTime::now();
    }
    holdInProgress = true;
  }
  else
  {
    if (holdInProgress)
    {
       ROS_INFO_STREAM("B"<< battery->get_id() << " charge current >= 0.1A or min cell voltage < 4.090V.  Trickle charge timer will reset.");
    }
    holdInProgress = false;
  }

  if (!holdInProgress)
  {
    return;
  }

  const auto elapsed_hold_time = (ros::WallTime::now() - holdStartTime).toSec();
  const auto hold_time_remaining = holdDuration.toSec() - elapsed_hold_time;
  if (hold_time_remaining > 0)
  {
    ROS_INFO_STREAM("B" << battery->get_id() << " trickle charge time remaining: " << static_cast<int>(hold_time_remaining / 60) << "m");
    return;
  }

  ROS_INFO_STREAM("B" << battery->get_id() << " finished trickle charge.");
  ROS_INFO_STREAM("B" << battery->get_id() << " setting full level.");
  battery->setFull();
  holdInProgress = false; // start over with the hold time and zero again in 10 minutes.
  ROS_INFO_STREAM("B" << battery->get_id() << " stopping charge.");
  stopCharge();
}

bool ChargeSequence::charge_notOk()
{
  // Check if battery is OK
  if (battery->get_errors())
  {
    ROS_ERROR_STREAM("Battery " << battery->get_id() << " is not OK, aborting");
    return true;
  }
  // Check if we heard from the battery recently enough
  if (battery->get_time_since() > batteryCommsTimeout)
  {
    ROS_ERROR_STREAM("Battery " << battery->get_id() << " last comm " << battery->get_time_since()
                                << " seconds ago, aborting charge");
    return true;
  }

  // Check if power supply is OK
  if (charger->get_errors())
  {
    ROS_ERROR_STREAM("Power supply " << charger->get_id() << " not OK, aborting charge");
    return true;
  }

  // Check if we heard from power supply recently enough
  if (charger->get_time_since() > chargerCommsTimeout)
  {
    ROS_ERROR_STREAM("Power supply " << charger->get_id() << " lost comm " << charger->get_time_since()
                                     << " seconds ago, aborting charge");
    return true;
  }

  return false;
}

void ChargeSequence::stopCharge()
{
  current = 0;
  battery->setOff();
  charger->setOff();
  state = DONE;
}

void ChargeSequence::startCharge()
{
  battery->setCharge();
  charger->setVC(voltage, current);
}
