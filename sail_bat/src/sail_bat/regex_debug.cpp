/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 5/12/18.
//

#include "../../include/sail_bat/regex_debug.h"

namespace debug
{
RegexDebug::RegexDebug()
{
  start_regex = boost::regex{ "(!B1 )?B1(?= \\d)" };
  match_regex = boost::regex{ "(?<= )([0-9A-Fa-f]{2})\\s?\n\r\x03" };
}

bool RegexDebug::attempt_to_match(std::string msg)
{
  boost::smatch start_results;
  if (!boost::regex_search(msg, start_results, start_regex))
  {
    return false;
  }

  boost::smatch end_results;
  // These two lines are good
  std::string sub_string = msg.substr(start_results.position());
  ROS_INFO_STREAM("Substring: \n " << sub_string);
  if (!boost::regex_search(sub_string, end_results, match_regex))
  {
    return false;
  }

  std::string str1 = end_results[1];
  std::string str2 = msg.substr(start_results.position() + end_results.position(), 2);
  ROS_INFO_STREAM("str1 = " << str1 << " str2 = " << str2);
  int int1 = strtoul(str1.c_str(), NULL, 16);
  int int2 = strtoul(str2.c_str(), NULL, 16);
  ROS_INFO_STREAM("int1 = " << int1 << " int2 = " << int2);
  if (int1 != int2)
  {
    ROS_ERROR_STREAM("Weird behavior!");
    return false;
  }
  return true;
}

bool RegexDebug::attempt_to_match_bad(std::string msg)
{
  boost::smatch start_results;
  if (!boost::regex_search(msg, start_results, start_regex))
  {
    return false;
  }

  boost::smatch end_results;
  // This line is bad
  if (!boost::regex_search(msg.substr(start_results.position()), end_results, match_regex))
  {
    return false;
  }

  std::string str1 = end_results[1];
  std::string str2 = msg.substr(start_results.position() + end_results.position(), 2);
  ROS_INFO_STREAM("str1 = " << str1 << " str2 = " << str2);
  int int1 = strtoul(str1.c_str(), NULL, 16);
  int int2 = strtoul(str2.c_str(), NULL, 16);
  ROS_INFO_STREAM("int1 = " << int1 << " int2 = " << int2);
  if (int1 != int2)
  {
    ROS_ERROR_STREAM("Weird behavior!");
    std::string msgFilt(msg.substr(start_results.position(), end_results.position()));
    ROS_INFO_STREAM("Msg: \n" << msgFilt);
    return false;
  }
  return true;
}
}