Author: J Vaccaro
Updated: 2/27/18

Battery : DsBusDevice

** include/sail_bat/battery.h
** src/sail_bat/battery.cpp

Using the BAT sail bus, Battery drivers communicate with a single Battery Module.
They regularly query and publish the latest status information on the topic ~node/state.

Launch requires
    - ds_bus

Launch prefers
    - batman, ds_lambda_ps

Service servers
    - BatteryCmd.srv : Battery action command
      ~nodename/cmd

      uint8 BAT_CMD_OFF=1
      uint8 BAT_CMD_CHARGE=2
      uint8 BAT_CMD_DISCHARGE=3
      uint8 BAT_CMD_RESET_FLAGS=4
      uint8 BAT_CMD_REBOOT=5
      uint8 BAT_CMD_RECORD_FULL=6
      uint8 BAT_CMD_RECORD_EMPTY=7
      uint8 BAT_CMD_ZERO_SHUNTS=8
      uint8 BAT_CMD_ENABLE_BALANCE=9
      uint8 BAT_CMD_DISABLE_BALANCE=10

Subscriptions
    -BAT_SAIL/instrument/raw

Publishers
    - Battery.msg
      ~nodename/state


