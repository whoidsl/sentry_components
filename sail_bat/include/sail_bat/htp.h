/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/23/18.
//

#ifndef PROJECT_BATSAIL_HTP_H
#define PROJECT_BATSAIL_HTP_H

#include "ds_base/ds_bus_device.h"
#include "ds_hotel_msgs/HTP.h"
#include "ds_core_msgs/IoSMcommand.h"
#include <string>
#include <memory>
#include <ds_base/util.h>

namespace sail_bat
{
class HTP : public ds_base::DsBusDevice
{
public:
  explicit HTP();
  HTP(int argc, char* argv[], const std::string& name);
  ~HTP() override;
  DS_DISABLE_COPY(HTP)

private:
  ros::Publisher htp_pub_;
  ds_hotel_msgs::HTP handled;

  /// \brief unique address of HTP sensor from parameters
  std::string address_;

  /// \brief internal Regexs calculated from address_
  boost::regex match_regex;
  boost::regex parse_regex;
  void update_regex();

  /// @brief I/O state machine
  boost::shared_ptr<ds_asio::IoSM> iosm;

protected:
  /// \brief overwritten functions from DsBusDevice
  void setupParameters() override;
  void setupIoSM() override;
  void setupConnections() override;
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;

  /// \brief from counts into proper units
  double computeHumidity(unsigned long raw);
  double computeTemperature(unsigned long raw);
  double computePressure(unsigned long raw);

public:
  /// \brief parsing functions: public for unit testing purposes
  bool acceptsMessage(const std::string& msg);
  bool handleMessage(const std::string& msg);

  /// \brief getters for verification of parsing success
  double getHumidity();
  double getTemperature();
  double getPressure();
};
}

#endif  // PROJECT_HTP_H
