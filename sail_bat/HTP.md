Author: J Vaccaro
Updated: 2/21/18

HTP : DsBusDevice

** include/sail_grd/htp.h
** src/sail_grd/htp.cpp

Using the GRD sail bus, HTP drivers gather and publish humidity, temperature, and pressure
information from a specified sail sensor address.

Launch requires
    - ds_bus

Service servers

Subscriptions

Publishers
    - HTP.msg
      ~nodename/htp