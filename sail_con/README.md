# Connections provided by this node

```
Publications: 
 [nodename]/state [ds_actuator_msgs/ServoState]
 [nodename]/joint_states [sensor_msgs/JointState]
 [nodename]/status [ds_core_msgs/Status]

Subscriptions: 
 * [nodename] [ds_actuator_msgs/ServoCmd]

Services: 
 * [nodename]/ctrl_cmd [sentry_msgs/SailServoCmd]
```

